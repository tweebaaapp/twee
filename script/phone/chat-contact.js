//进入聊天页面
function chatTo(userId, username, userPortrait, conversationType, isTop) {
     console.log("userId:"+ userId+ " username:"+username+' userPortrait:'+userPortrait+' conversationType:'+conversationType+ ' isTop:'+ isTop);
    localStorage.setItem('openFrame', 'chatPage');
    api.openWin({
        name: 'chatPage',
        url: '../chat/chat.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            userId: userId,
            userName: username,
            userPortrait: userPortrait,
            conversationType: conversationType,
            isTop: isTop
        }
    });
}

//获取未读消息总数
function getTotalUnreadCount(n) {
    var rong = api.require('rongCloud2');
    rong.getTotalUnreadCount(function (ret, err) {
        //console.log("未读消息总数:" + JSON.stringify(ret));
        var count = ret.result;
        if (count !== 0) {
            api.sendEvent({
                name: 'showUnreadTips',
                extra: {
                    count: count
                }
            });
            $('.new-info').show();
        } else {
            api.sendEvent({
                name: 'showUnreadTips',
                extra: {
                    count: 0
                }
            });
            $('.new-info').hide();
        }
        //console.log('未读消息'+JSON.stringify(ret));
    })
}

function postNotification() {
    api.notification({
        vibrate: [500, 500],
        sound: 'default',
        notify: {
            title: 'Tweebaa',
            content: $.i18n.prop('havenewMessage') //'you have new message'
        }

    });
}


//获取请求列表
//getApplcationList();
function getApplcationList() {
    $('#appFriendsList').html('');
    var list = JSON.parse(localStorage.getItem('friendList'));
    //console.log(localStorage.getItem('friendList'));
    if (list !== null) {
        var len = list.length;
        if (len !== 0) {
            api.sendEvent({
                name: 'showFriendRequestTips',
                extra: {
                    active: 'show'
                }
            });
        } else {
            api.sendEvent({
                name: 'showFriendRequestTips',
                extra: {
                    active: 'hide'
                }
            });
        }
        for (i = 0; i < len; i++) {
            // var c = list[i].sendUserId.split('@')[1];
            var t = '<li class="contact-list-received box-p2 mb-5">\n' +
                '        <i class="icon iconfont icon-like_fill" style="color: #fa8585"></i> <span class="text-blue small">' + list[i].username + '</span> <span class="small"> ' + $.i18n.prop('wantaddfriend') + '</span>\n' +
                '        <div class="ui-row">\n' +
                '          <div class="x-small ui-col ui-col-50">&nbsp;</div>\n' +
                '          <div class="ui-col ui-col-25 pr-2">\n' +
                '            <button class="ui-btn btn-block btn-half-h ui-btn-primary" onclick="ignore(\'' + i + '\')">' + $.i18n.prop('ignore') + '</button>\n' +
                '          </div>\n' +
                '          <div class="ui-col ui-col-25">\n' +
                '            <button class="ui-btn btn-block btn-half-h ui-btn-success" onclick="agree(\'' + list[i].sendUserId + '\',\'' + list[i].username + '\',\'' + i + '\')" >' + $.i18n.prop('agree') + '</button>\n' +
                '          </div>\n' +
                '        </div>\n' +
                '      </li>';
            $('#appFriendsList').prepend(t);
        }
    }
}

//获取会话列表
function getConversationList() {
    $('.no-item').hide();
    var rong = api.require('rongCloud2');
    rong.getConversationList(function (ret, err) {
        //console.log("会话列表:" +JSON.stringify(ret));
        //结束下拉刷新状态
        api.refreshHeaderLoadDone();
        var r = ret.result;
        if (!r || !r.length || r.length === 0) {
            $('.no-item').show();
            return false
        }
        var len = r.length;
        $('#conversationList').empty();
        for (i = 0; i < len; i++) {
            var conversationType = r[i].conversationType;
            var lastMessage;
            var unreadMessageCount = '';
            var chatId;
            var chatName;
            var displayName = '';
            var name;
            var portrait;
            var portraitUrl;
            var receivePortrait;
            if (r[i].latestMessageId === -1) {
                continue;
            }
            if (r[i].objectName == 'RC:DizNtf') {
                name = {
                    sendName: ''
                };
                lastMessage = $.i18n.prop('groupMessage');  /// 退群
            } else if (r[i].objectName == 'RC:TxtMsg') {
                name = JSON.parse(r[i].latestMessage.extra);
                lastMessage = r[i].latestMessage.text;
            } else if (r[i].objectName == 'RC:ImgMsg') {
                name = JSON.parse(r[i].latestMessage.extra);
                lastMessage = $.i18n.prop('image');
            } else if (r[i].objectName == 'RC:VcMsg') {
                name = JSON.parse(r[i].latestMessage.extra);
                lastMessage = $.i18n.prop('voiceMessage');
            }
            if (r[i].unreadMessageCount !== 0) {
                unreadMessageCount = '<span class="unread-tips">' + r[i].unreadMessageCount + '</span>';
            }
            if (conversationType === 'PRIVATE') {
                if (r[i].senderUserId === localStorage.getItem('chatID')) {
                    chatId = r[i].targetId;
                    chatName = name.receiveName;
                    displayName = name.receiveName;
                    receivePortrait = name.receivePortrait !== '' ? name.receivePortrait : 'https://api.tweebucks.com/tweebaa/downloadFile/default.jpg';
                    portrait = '<img src="' + receivePortrait + '" class="contact-list-friends-header">';
                    portraitUrl = name.receivePortrait;
                } else {
                    chatId = r[i].senderUserId;
                    chatName = name.sendName;
                    displayName = name.sendName;
                    portrait = '<img src="' + name.sendPortrait + '" class="contact-list-friends-header">';
                    portraitUrl = name.sendPortrait;
                }
            } else if (conversationType === 'DISCUSSION') {
                chatId = r[i].targetId;
                chatName = r[i].conversationTitle;
                portrait = '<i class="icon iconfont icon-qunliao contact-list-friends-header" style="font-size: 3rem"></i>';
                portraitUrl = '';
            }
            var d = moment.utc(ret.result[i].sentTime).local().format('MM-DD HH:mm');
            var template = '<li class="contact-list-friends box-p2 mb-4 ui-row  ' + r[i].isTop + '" onclick="chatTo(\'' + chatId + '\',\'' + chatName + '\',\'' + portraitUrl + '\',\'' + conversationType + '\',\'' + r[i].isTop + '\',\'messages\')" id="' + chatId + '">\n' +
                '        <span class="chat-time x-small text-light-gray">' + d + '</span>' +
                portrait +
                '        <div class="conversation-list-friends-info">\n' +
                '        <mark class="' + conversationType + '"></mark>\n' +
                '          <p class="ui-nowrap mb-3"> <span class="chat-name">' + chatName + '</span>' + unreadMessageCount + '</p>\n' +
                '          <p class="text-dark-gray small ui-nowrap">' + name.sendName + ' : ' + lastMessage + '</p>\n' +
                '        </div>\n' +
                '        <input type="hidden" value="' + chatName + '" data-id="' + chatId + '"> \n' +
                '</li>';
            $('#conversationList').append(template);
        }

        getTotalUnreadCount();
        $('.no-item').hide();
        $('#conversationList').find('input').each(function () {  //如果没有获得群名称
            if ($(this).val() === '') {
                var that = $(this);
                var id = $(this).attr('data-id');
                var rong = api.require('rongCloud2');
                rong.getDiscussion({
                    discussionId: id
                }, function (ret, err) {
                    that.closest('.contact-list-friends').find('span.chat-name').text(ret.result.name);
                })
            }
        });

    });

}

//长按删除聊天
$('#conversationList').on('touchstart', '.contact-list-friends', function (event) {
    var timeout = undefined;
    var that = $(this);
    var $this = this;
    var id = that.attr('id'); //消息ID
    var type = that.find('mark').attr('class');
    var name = that.find('.chat-name').text();
    var pic = that.find('.contact-list-friends-header').attr('src');
    that.addClass('opacity-5');
    timeout = setTimeout(fn, 800);
    function fn() {
        $('.ui-dialog-operate').addClass('show');
        $('#userChatPortrait').attr('src',pic);
        $('#userChatName').text(name);
    }
    $this.addEventListener('touchmove', function (event) {
        clearTimeout(timeout);
        that.removeClass('opacity-5');
    }, false);
    $this.addEventListener('touchend', function (event) {
        clearTimeout(timeout);
        that.removeClass('opacity-5');
    }, false);
    $('#removeConversation').on('click',function () {
        removeConversation(id, type);
        hidedialog();
    });
    $('#deleteConversation').on('click',function () {
        var rong = api.require('rongCloud2');
        rong.addToBlacklist({//拉黑
            userId: id
        }, function (ret, err) {
            removeConversation(id, type);
            hidedialog();
        })
    });
    $('.ui-dialog-close').on('click',function () {
        hidedialog();
    })

});

function hidedialog() {
    $('.ui-dialog-operate').removeClass('show');
}

//删除会话
function removeConversation(chatId, type) {
    //console.log(chatId);
    var rong = api.require('rongCloud2');
    rong.removeConversation({
        conversationType: type,
        targetId: chatId
    }, function (ret, err) {
        //console.log(JSON.stringify(ret));
        api.sendEvent({
            name: 'conversationListReload'
        });
    })
}

//获取朋友列表
function getFriendList(page, userID) {
    loading('show');
    $('.no-item').hide();
    var name = localStorage.getItem('chatID');
    var paramStr = {
        "itemStr": name,
        "start": 0,
        "pageSize": 500
    };
    var url = "https://api.tweebucks.com/tweebaa/getUserContactList";
    var token = "IM";
    var method = "POST";
    var contentTypeStr = "application/json";
    $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        //console.log(JSON.stringify(data));
        //结束下拉刷新状态
        api.refreshHeaderLoadDone();
        localStorage.setItem('contacts', JSON.stringify(data));
        loading('hide');
        $('#friendsList').html('');
        if (data.length !== 0) {
            var len = data.length;
            for (var i = 0; i < len; i++) {
                var userId = data[i].userId.split('@')[0];
                var template;
                if (page === undefined) {
                    // template = '<li class="contact-list-friends waves box-p2 mb-4" id="'+ data[i].userId +'" tapmode onclick="chatTo(\'' + data[i].userId + '\',\'' + data[i].name + '\',\'' + data[i].portraitUri + '\',\'PRIVATE\')">\n' +
                    template = '<li class="contact-list-friends waves box-p2 mb-4" id="' + data[i].userId + '" onclick="openFriendProfile(\'' + data[i].userId + '\',\'' + data[i].name + '\',\'' + data[i].portraitUri + '\',\'PRIVATE\')">\n' +
                        '        <img src="' + data[i].portraitUri + '" class="contact-list-friends-header">\n' +
                        '        <div class="contact-list-friends-info">\n' +
                        '          <p class="show-chat-name"> ' + data[i].name + ' </p>\n' +
                        '          <p class="text-dark-gray small"> ' + $.i18n.prop('username') + ' :(' + userId + ') </p>\n' +
                        '        </div>\n' +
                        '      </li>';
                    $('#friendsList').prepend(template);
                } else {
                    var checked = '';
                    var notouch = '';
                    if (userID.indexOf(data[i].userId) !== -1) {
                        checked = ' checked';
                        notouch = ' no-touch'
                    }
                    template = '<li class="contact-list-friends waves box-p2 mb-4 ' + checked + notouch + '" data-friends=\'' + JSON.stringify(data[i]) + '\'>\n' +
                        '      <img src="' + data[i].portraitUri + '" class="contact-list-friends-header">\n' +
                        '      <div class="contact-list-friends-info">\n' +
                        '        <p>  ' + data[i].name + ' </p>\n' +
                        '        <p class="text-blue small"> User Name :(' + userId + ') </p>\n' +
                        '      </div>\n' +
                        '      <div class="check-box">&radic;</div>\n' +
                        '    </li>';
                    $('#groupUserList').prepend(template);
                }
            }

        } else {
            $('.no-item').show();
        }
    });
}

//进入好友profile 页面
function openFriendProfile(userId, username, userPortrait, conversationType) {
    localStorage.setItem('openFrame', 'friendProfile');
    api.openFrame({
        name: 'friendProfile',
        url: '../users/friend-profile.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            userId: userId,
            userName: username,
            userPortrait: userPortrait,
            conversationType: conversationType
        }
    });
}

//删除好友
function deleteFriend(id, name) {
    loading('show');
    var u = localStorage.getItem('chatID');
    var param = {"itemStr": u, "userContactId": id, 'userContactName': name};
    var met = "POST";
    var urL = 'https://api.tweebucks.com/tweebaa/removeContact';
    var key = 'IM';
    var contentTypeStr = "application/json";
    $.when(postApi(urL, met, param, key, contentTypeStr)).done(function (data) {
        removeConversation(id, 'PRIVATE');
        api.sendEvent({
            name: 'contactListReload'
        });
        api.sendEvent({
            name: 'conversationListReload'
        });
        getFriendList();

        var rong = api.require('rongCloud2');
        rong.sendCommandMessage({
            conversationType: 'PRIVATE',
            targetId: id,
            name: 'delFriend',
            data: ''
        }, function (ret, err) {
            rong.addToBlacklist({//拉黑
                userId: id
            }, function (ret, err) {
                //console.log(JSON.stringify(ret));
                loading('hide');
            })
        });
    });
}

//获取发出的申请
function getRecord() {
    $('#recordFriendsList').html('');
    var list = JSON.parse(localStorage.getItem('record'));
    //console.log(localStorage.getItem('friendList'));
    if (list !== null) {
        var len = list.length;
        for (i = 0; i < len; i++) {
            var userId = list[i].userId;
            var t = '<li class="contact-list-send box-p2 mb-5" id="' + userId + '">\n' +
                '        <i class="icon iconfont icon-like" style="color: #fa8585"></i> <span class="small">' + $.i18n.prop('waitingFor') + '</span> <span class="text-blue small"> ' + list[i].username + ' </span> <span class="small">' + $.i18n.prop('toAgree') + '</span>\n' +
                '      </li>';
            $('#recordFriendsList').append(t);
        }
    }
}


//同意加好友
function agree(sendUserId, username, i) {
    loading('show');
    var name = localStorage.getItem('chatID');
    var displayName = localStorage.getItem('storename');
    var rong = api.require('rongCloud2');
    rong.getBlacklistStatus({
        userId: sendUserId
    }, function (ret, err) {
        if (ret.result == 0) {
            rong.removeFromBlacklist({
                userId: sendUserId
            }, function (ret, err) {
            })
        }
    });
    var paramStr = {
        "itemStr": name,
        "nameStr": displayName,
        "userContactId": sendUserId,
        "userContactName": username
    };
    //console.log(JSON.stringify(paramStr));
    var url = "https://api.tweebucks.com/tweebaa/addContacts";
    var token = "IM";
    var method = "POST";
    var contentTypeStr = "application/json";
    $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        if (data == true) {
            getFriendList();
            ignore(i);

            //向好友发一条信息
            //var rong = api.require('rongCloud2');
            var extra = {
                sendName: localStorage.getItem('storename'),
                sendPortrait: localStorage.getItem('chatPortrait'),
                receiveName: username,
                receivePortrait: '',
                Source: 'Friend',
                agree: true
            };
            rong.sendTextMessage({
                conversationType: 'PRIVATE',
                targetId: sendUserId,
                text: $.i18n.prop('friendsNow'),
                extra: JSON.stringify(extra)
            }, function (ret, err) {
                loading('hide');
                if (ret.status == 'success') {
                    //console.log("发送成功:" + JSON.stringify(ret));
                    api.sendEvent({
                        name: 'contactListReload'
                    });
                    api.sendEvent({
                        name: 'conversationListReload'
                    });
                }

            });
        }

    });
}

//在申请列表中删除
function ignore(i) {
    //console.log(i);
    var list = JSON.parse(localStorage.getItem('friendList'));
    list.splice(i, 1);
    var newList = JSON.stringify(list);
    localStorage.setItem('friendList', newList);
    // console.log(localStorage.getItem('friendList'));
    getApplcationList();
}

//在发送申请列表中删除
function deleteRecordList(userId) {
    var list = JSON.parse(localStorage.getItem('record'));
    if (list) {
        var len = list.length;
        if (len !== 0) {
            for (i = 0; i < len; i++) {
                var userid = list[i].userId;
                if (userid == userId) {
                    list.splice(i, 1);
                }
            }
        }
        var newList = JSON.stringify(list);
        localStorage.setItem('record', newList);
        //getRecord();
        getFriendList();
    }
}
function openPortrait(userPortrait) {
    var imageBrowser = api.require('imageBrowser');
    imageBrowser.openImages({
        imageUrls: [userPortrait],
        showList: false,
        tapClose: true
    });
}
//打开用户个人信息页面
function openUserProfile(id, name, userPortrait, groupId, leaderId) {
    if (id === localStorage.getItem('chatID')) {
        return false;
    }
    // localStorage.setItem('openFrame', 'userProfile');
    localStorage.setItem('openFrame', 'friendProfile');
    api.openFrame({
        // name: 'userProfile',
        name: 'friendProfile',
        // url: '../users/user-profile.html',
        url: '../users/friend-profile.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            userId: id,
            userName: name,
            userPortrait: userPortrait,
            conversationType: api.pageParam.conversationType,
            groupId: groupId,
            leaderId: leaderId,
            page: 'chat'
        }
    });
}
