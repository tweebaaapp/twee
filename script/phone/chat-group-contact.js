//选中列表中的好友
$('#groupUserList').on('click','.contact-list-friends',function (e) {
  $(this).toggleClass('checked');
  getFriendsLength();
});
function getFriendsLength() {
  var len;
  if(api.pageParam.conversationType === 'PRIVATE'){
    len = $('.contact-list-friends.checked').length;
    if(len >1){
      $('#createGroupBtn').removeClass('d-none');
    }else{
      $('#createGroupBtn').addClass('d-none');
    }
  }else{
    len = $('.contact-list-friends.checked').not('.no-touch').length;
    if(len >0){
      $('#createGroupBtn').removeClass('d-none');
    }else{
      $('#createGroupBtn').addClass('d-none');
    }
  }

}
//区分按钮
function groupFunction() {
  if(api.pageParam.conversationType === 'PRIVATE'){
    createGroup();
  }else if(api.pageParam.conversationType === 'DISCUSSION'){
    joinGroup();
  }
}
//加入群
function joinGroup() {
  var id = api.pageParam.userId;
  var IdList = [];
  loading('show');
  $('.contact-list-friends.checked').not('.no-touch').each(function () {
    IdList.push($(this).data('friends').userId);
  });
  var rong = api.require('rongCloud2');
  rong.addMemberToDiscussion({
    discussionId: id,
    userIdList: IdList
  }, function(ret, err) {
    if (ret.status == 'success'){
      //console.log(JSON.stringify(ret));
      var paramStr ={
        "groupId" : id,
        "memberId" : IdList
      };
      var url = "https://api.tweebucks.com/tweebaa/addmember";
      var token = "IM";
      var method = "POST";
      var contentTypeStr = "application/json";
      $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        loading('hide');
        api.sendEvent({
          name: 'getGroupList'
        });
        localStorage.setItem('openFrame', 'chatProfile');
        api.closeFrame({
          name:'groupUserList'
        });
      });
    } else{
      loading('hide');
      messages($.i18n.prop('systemError'), 'error', 'top', 2);
    }

  })
}

//创建群
function createGroup() {
  confirmBox($.i18n.prop('wantGroupChat'),$.i18n.prop('cancel'),$.i18n.prop('sure'),'success'); //Your product has been created!
  $('.dialog-box').find('button').on('click',function (e) {
    var index = $(this).attr('id');
    $('.dialog-box').remove();
    if (index === 'btn-1') {
      return false;
    } else {
      loading('show');
      var leaderId = localStorage.getItem('chatID');
      var leaderName = localStorage.getItem('chatName');
      var IdList = [leaderId];
      var nameList=[leaderName];
      $('.contact-list-friends.checked').each(function () {
        nameList.push($(this).data('friends').name);
        IdList.push($(this).data('friends').userId);
      });
      var names = (nameList.slice(0,3)).join('、')+'...';
      //console.log(names);
      var rong = api.require('rongCloud2');
      rong.createDiscussion({
        name: names,
        userIdList: IdList
      }, function(ret, err) {
        if (ret.status == 'success'){
          var paramStr ={
            "groupId" : ret.result.discussionId,
            "name":names,
            "leaderId":leaderId,
            "createDate" : '',
            "members" : IdList
          };
          var url = "https://api.tweebucks.com/tweebaa/creategroup";
          var token = "IM";
          var method = "POST";
          var contentTypeStr = "application/json";
          $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {

            //发送一条群消息
            var rong = api.require('rongCloud2');
            var extra = {
              sendName: localStorage.getItem('storename'),
              sendPortrait: localStorage.getItem('chatPortrait'),
              receiveName: '',
              receivePortrait: localStorage.getItem('chatPortrait')
            };
            rong.sendTextMessage({
              conversationType: 'DISCUSSION',
              targetId: ret.result.discussionId,
              text: $.i18n.prop('joinTheGroup'),
              extra: JSON.stringify(extra)
            }, function (ret, err) {

            });

            api.closeFrame({
              name:'chatPage'
            });
            api.closeFrame({
              name:'chatProfile'
            });
            localStorage.setItem('openFrame', 'chatPage');
            setTimeout(function () {
              loading('hide');
              api.openFrame({
                name: 'chatPage',
                url: '../chat/chat.html',
                bounces:false,
                animation:{
                  type:"movein",
                  subType:"from_right"
                },
                pageParam: {
                  userId:ret.result.discussionId,
                  userName: nameList,
                  conversationType:'DISCUSSION'
                }
              });
              api.closeFrame({
                name:'groupUserList'
              });
            },500);

          });

        }
        else{
          console.log(JSON.stringify(err));
        }
      });

    }

  });


}