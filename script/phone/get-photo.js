var id = 0;
var liWith = $('#width').width() + 5;
var moment = $('#momentpage').val();
// //使用getPicture模块获取照片
// $('#getPic').on('click',function () {
//   api.actionSheet({
//     cancelTitle: $.i18n.prop('cancel'),
//     buttons: [$.i18n.prop('photo'), $.i18n.prop('library')]
//   }, function(ret, err) {
//     var index = ret.buttonIndex;
//     var type;
//     if(index === 1){
//       type = 'camera';
//     }else if(index === 2){
//       type = 'library';
//     }else{
//       return false;
//     }
//     loading('show');
//     api.getPicture({
//       sourceType: type,
//       encodingType: 'jpg',
//       mediaValue: 'pic',
//       destinationType: 'base64',
//       allowEdit: false,
//       quality: 60,
//       targetWidth: 800,
//       saveToPhotoAlbum: false
//     }, function(ret, err) {
//       if (ret) {
//         var img = '<img class="img-fluid" id ="'+ id +'" src='+ ret.base64Data + ' alt="image">';
//         $('.swiper-wrapper').prepend('<div class="swiper-slide">'+ img +'</div>');
//         $('#uploadImgBox').prepend('<li class="ui-col ui-col-20 img-li" style="height: '+ liWith +'px"><i class="icon iconfont icon-delete1" class="text-red"></i><div class="media-box">'+ img +'</div></li>');
//         id++;
//         loading('hide');
//       } else {
//         loading('hide');
//         // alert(JSON.stringify(err));
//       }
//     });
//   });
//
// });
//
// //使用input获取照片
var picArray = [];
// $('#getPhoto').change(function() {
//   var file = this.files; //选择上传的文件
//   if(!file){
//     return false;
//   }
//   //console.log(JSON.stringify(file));
//   var len = file.length;
//   for (i = 0; i<len ; i++){
//     var r = new FileReader();
//     var canvas = document.createElement('canvas');
//     var context = canvas.getContext('2d');
//     $(r).load(function(e) {
//       var image = new Image();
//       image.src = e.target.result;
//       image.onload = function() {
//         if(moment){
//
//           var originWidth = this.width;
//           var originHeight = this.height;
//           // 最大尺寸限制
//           var maxWidth = 800, maxHeight = 800;
//           // 目标尺寸
//           var targetWidth = originWidth, targetHeight = originHeight;
//
//           if (originWidth > maxWidth || originHeight > maxHeight) {
//             if (originWidth / originHeight > maxWidth / maxHeight) {
//               // 更宽，按照宽度限定尺寸
//               targetWidth = maxWidth;
//               targetHeight = Math.round(maxWidth * (originHeight / originWidth));
//             } else {
//               targetHeight = maxHeight;
//               targetWidth = Math.round(maxHeight * (originWidth / originHeight));
//             }
//           }
//           canvas.width = targetWidth;
//           canvas.height = targetHeight;
//           // 清除画布
//           context.clearRect(0, 0, targetWidth, targetHeight);
//           // 图片压缩
//           context.drawImage(image, 0, 0, targetWidth, targetHeight);
//         }else{
//           var clipX = image.width > image.height ? (image.width - image.height) / 2 : 0;
//           var clipY = image.height > image.width ? (image.height - image.width) / 2 : 0;
//           var clipWidth = image.width > image.height ? image.height : image.width;
//           var clipHeight = clipWidth;
//           canvas.width = clipWidth;
//           canvas.height = canvas.width;
//           context.clearRect(0, 0, canvas.width, canvas.height);
//           // context.drawImage(image, 0, 0, targetWidth, targetHeight);
//           context.drawImage(image, clipX, clipY, clipWidth, clipHeight, 0, 0, canvas.width, canvas.height);
//         }
//         var processedProfilePhoto = canvas.toDataURL('image/jpeg', .7);
//         // console.log(processedProfilePhoto);
//         var img = '<img class="img-fluid" id ="'+ id +'" src='+ processedProfilePhoto + ' alt="image">';
//         $('.swiper-wrapper').prepend('<div class="swiper-slide">'+ img +'</div>');
//         $('#uploadImgBox').prepend('<li class="ui-col ui-col-20 img-li" style="height: '+ liWith +'px"><i class="icon iconfont icon-delete1 text-red" ></i><div class="media-box">'+ img +'</div></li>');
//         id++;
//           var pic = processedProfilePhoto.split(",")[1];
//           var picJson = {'PictureBinaryBase64': pic, 'FileName': 'picture.jpg','MediaType': 'Picture'};
//           picArray.push(picJson);
//       };
//         console.log(picArray);
//     });
//     r.readAsDataURL(file[i]); //Base64
//   }
//     // Sortable.create(document.getElementById('uploadImgBox'),
//     //     {
//     //         animation: 150, onEnd: function (e) {
//     //             picArray = [];
//     //             $("#uploadImgBox").find('img').each(function () {
//     //                 var pic = $(this).attr('src').split(",")[1];
//     //                 var picJson = {'PictureBinaryBase64': pic, 'FileName': 'picture.jpg','MediaType': 'Picture'};
//     //                 picArray.push(picJson);
//     //             });
//     //             console.log(picArray);
//     //         }
//     //     });
// });
//使用新选择器获取图片
$('#getPhotosNew').on('click', function () {
    api.actionSheet({
        cancelTitle: $.i18n.prop('cancel'),
        buttons: [$.i18n.prop('photo'), $.i18n.prop('library')]
    }, function (ret, err) {
        var index = ret.buttonIndex;
        if (index === 1) {
            useCamera()
        } else if (index === 2) {
            selectImagesFromAlbum()
        } else {
            return false;
        }
    });

});

//打开相机照相
function useCamera() {
    api.getPicture({
        sourceType: 'camera',
        encodingType: 'jpg',
        mediaValue: 'pic',
        destinationType: 'base64',
        allowEdit: false,
        quality: 60,
        targetWidth: 800,
        saveToPhotoAlbum: true
    }, function (ret, err) {
        if (ret) {
            var img = '<img class="img-fluid" id ="' + id + '" src=' + ret.base64Data + ' alt="image">';
            $('.swiper-wrapper').prepend('<div class="swiper-slide">' + img + '</div>');
            $('#uploadImgBox').prepend('<li class="ui-col ui-col-20 img-li" style="height: ' + liWith + 'px"><i class="icon iconfont icon-delete1" class="text-red"></i><div class="media-box">' + img + '</div></li>');
            id++;
            var pic = ret.base64Data.split(",")[1];
            var picJson = {
                'PictureBinaryBase64': pic,
                'FileName': 'picture.jpg',
                'SeoFileName': 'picture.jpg',
                'MediaType': 'Picture'
            };
            picArray.push(picJson);
            loading('hide');
            Sortable.create(document.getElementById('uploadImgBox'),
                {
                    animation: 150, onEnd: function (e) {
                        picArray = [];
                        $("#uploadImgBox").find('img').each(function () {
                            var pic = $(this).attr('src').split(",")[1];
                            var picJson = {
                                'PictureBinaryBase64': pic,
                                'FileName': 'picture.jpg',
                                'SeoFileName': 'picture.jpg',
                                'MediaType': 'Picture'
                            };
                            picArray.push(picJson);
                        });
                        // console.log(picArray);
                    }
                });
        } else {
            loading('hide');
            // alert(JSON.stringify(err));
        }
    });
}

//在相册选取
function selectImagesFromAlbum() {
    var UIAlbumBrowser = api.require('UIAlbumBrowser');
    UIAlbumBrowser.imagePicker({
        max: 9,
        styles: {
            bg: '#FFFFFF',
            cameraImg: 'widget://res/cameraImg.png',
            mark: {
                icon: '',
                position: 'bottom_left',
                size: 20
            },
            nav: {
                bg: '#252525',
                cancelColor: '#fff',
                cancelSize: 16,
                nextStepColor: '#fff',
                nextStepSize: 16,
                cancelText: $.i18n.prop('cancel'),             //（可选项）字符串类型；取消按钮文字内容；默认：'取消'
                finishText: $.i18n.prop('ok'),             //（可选项）字符串类型；完成按钮文字内容；默认：'下一步'
            }
        },
        animation: true,
    }, function (ret) {
        if (ret.eventType === 'nextStep') {
            var path = ret.list;
            var len = path.length;
            UIAlbumBrowser.closePicker();
            if (api.systemType == "ios" || api.systemType == "IOS") {
                //console.log(JSON.stringify(path));
                for (i = 0; i < len; i++) {
                    UIAlbumBrowser.transPath({
                        quality: 'highest', // 视频质量（android此参数为图片的quality，不支持视频）
                        scale: 1, // 图片质量  取值范围：0~1.0
                        path: path[i].path // 要转换的图片路径（在相册库的绝对路径）
                    }, function (ret, err) {
                        setImages(ret.path)
                    });
                }
            } else {
                for (J = 0; J < len; J++) {
                    setImages(path[J].path);
                }
            }
        }
    });
}

function setImages(path) {
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    var image = new Image();
    image.src = path;
    //console.log(image.src);
    image.onload = function () {
        EXIF.getData(image, function () {
            // alert(EXIF.pretty(this));
            EXIF.getAllTags(this);
            var Orientation = EXIF.getTag(this, 'Orientation');

            if (moment) {
                var originWidth = this.width;
                var originHeight = this.height;
                // 最大尺寸限制
                var maxWidth = 800, maxHeight = 800;
                // 目标尺寸
                var targetWidth = originWidth, targetHeight = originHeight;

                if (originWidth > maxWidth || originHeight > maxHeight) {
                    if (originWidth / originHeight > maxWidth / maxHeight) {
                        // 更宽，按照宽度限定尺寸
                        targetWidth = maxWidth;
                        targetHeight = Math.round(maxWidth * (originHeight / originWidth));
                    } else {
                        targetHeight = maxHeight;
                        targetWidth = Math.round(maxHeight * (originWidth / originHeight));
                    }
                }
                canvas.width = targetWidth;
                canvas.height = targetHeight;
                // 清除画布
                context.clearRect(0, 0, targetWidth, targetHeight);
                // 图片压缩
                context.drawImage(image, 0, 0, targetWidth, targetHeight);
            } else {
                var clipX = image.width > image.height ? (image.width - image.height) / 2 : 0;
                var clipY = image.height > image.width ? (image.height - image.width) / 2 : 0;
                var clipWidth = image.width > image.height ? image.height : image.width;
                var clipHeight = clipWidth;
                canvas.width = clipWidth;
                canvas.height = canvas.width;
                context.clearRect(0, 0, canvas.width, canvas.height);
                // context.drawImage(image, 0, 0, targetWidth, targetHeight);
                context.drawImage(image, clipX, clipY, clipWidth, clipHeight, 0, 0, canvas.width, canvas.height);
            }

            if (Orientation !== "" && Orientation !== 1) {
                //console.log('旋转处理');
                switch (Orientation) {
                    case 6://需要顺时针（向左）90度旋转
                        console.log('需要顺时针（向左）90度旋转');
                        rotateImg(this, 'left', canvas);
                        break;
                    case 8://需要逆时针（向右）90度旋转
                        console.log('需要逆时针（向右）90度旋转');
                        rotateImg(this, 'right', canvas);
                        break;
                    case 3://需要180度旋转
                        console.log('需要180度旋转');
                        rotateImg(this, 'right', canvas);//转两次
                        rotateImg(this, 'right', canvas);
                        break;
                }
            }
            var processedProfilePhoto = canvas.toDataURL('image/jpeg', .7);
            // console.log(processedProfilePhoto);
            var img = '<img class="img-fluid" id ="' + id + '" src=' + processedProfilePhoto + ' alt="image">';
            $('.swiper-wrapper').prepend('<div class="swiper-slide">' + img + '</div>');
            $('#uploadImgBox').prepend('<li class="ui-col ui-col-20 img-li" style="height: ' + liWith + 'px"><i class="icon iconfont icon-delete1 text-red" ></i><div class="media-box">' + img + '</div></li>');
            id++;
            var pic = processedProfilePhoto.split(",")[1];
            var picJson = {
                'PictureBinaryBase64': pic,
                'FileName': 'picture.jpg',
                'SeoFileName': 'picture.jpg',
                'MediaType': 'Picture'
            };
            picArray.push(picJson);
        });

    };
    Sortable.create(document.getElementById('uploadImgBox'),
        {
            animation: 150, onEnd: function (e) {
                picArray = [];
                $("#uploadImgBox").find('img').each(function () {
                    var pic = $(this).attr('src').split(",")[1];
                    var picJson = {
                        'PictureBinaryBase64': pic,
                        'FileName': 'picture.jpg',
                        'SeoFileName': 'picture.jpg',
                        'MediaType': 'Picture'
                    };
                    picArray.push(picJson);
                });
                // console.log(picArray);
            }
        });
}

function rotateImg(img, direction, canvas) {
    //alert(img);
    //最小与最大旋转方向，图片旋转4次后回到原方向
    var min_step = 0;
    var max_step = 3;
    //var img = document.getElementById(pid);
    if (img == null) return;
    //img的高度和宽度不能在img元素隐藏后获取，否则会出错
    var height = img.height;
    var width = img.width;
    //var step = img.getAttribute('step');
    var step = 2;
    if (step == null) {
        step = min_step;
    }
    if (direction == 'right') {
        step++;
        //旋转到原位置，即超过最大值
        step > max_step && (step = min_step);
    } else {
        step--;
        step < min_step && (step = max_step);
    }
    //旋转角度以弧度值为参数
    var degree = step * 90 * Math.PI / 180;
    var ctx = canvas.getContext('2d');
    switch (step) {
        case 0:
            canvas.width = width;
            canvas.height = height;
            ctx.drawImage(img, 0, 0);
            break;
        case 1:
            canvas.width = height;
            canvas.height = width;
            ctx.rotate(degree);
            ctx.drawImage(img, 0, -height);
            break;
        case 2:
            canvas.width = width;
            canvas.height = height;
            ctx.rotate(degree);
            ctx.drawImage(img, -width, -height);
            break;
        case 3:
            canvas.width = height;
            canvas.height = width;
            ctx.rotate(degree);
            ctx.drawImage(img, -width, 0);
            break;
    }
}

$("#getVideo").on('change', function () {
    var fileName = $(this).val();
    if (fileName == "") {
        return false;
    }
    var objUrl = showVideo(this.files[0]);
    var extName = (fileName.substring(fileName.lastIndexOf('.') + 1)).toLowerCase();
    // console.log(extName);
    //console.log(fileName);
    var videos = ['mp4','mpg','mov','MOV','MP4','wmv','WMV'];
    var pics = ['jpg','jpeg','raw','png','JPG','PNG'];
    var audios =['amr','wav','mp3'];
    if (videos.indexOf(extName) !== -1) {
        //console.log(objUrl);
        if (objUrl) {
            $("#ShowVideo").show().attr("src", objUrl);
            $("#ShowVideo")[0].addEventListener("loadedmetadata", function () {
                var tol = Math.floor(this.duration); //获取总时长
                $("#ShowVideo")[0].currentTime = 1;
                if (!moment) {
                    $('.slider-input').jRange({
                        from: 0,
                        to: tol,
                        step: 1,
                        format: function (value) {
                            var theTime = parseInt(value);// 需要转换的时间秒
                            var theTime1 = 0;// 分
                            var theTime2 = 0;// 小时
                            if (theTime > 60) {
                                theTime1 = parseInt(theTime / 60);
                                theTime = parseInt(theTime % 60);
                                if (theTime1 > 60) {
                                    theTime2 = parseInt(theTime1 / 60);
                                    theTime1 = parseInt(theTime1 % 60);
                                }
                            }
                            var result = '';
                            if (theTime > 0) {
                                result = ":" + parseInt(theTime);
                            }
                            if (theTime1 > 0) {
                                result = ":" + parseInt(theTime1) + result;
                            }
                            if (theTime2 > 0) {
                                result = "" + parseInt(theTime2) + result;
                            }
                            return "0" + result;
                        },
                        showLabels: true,
                        showScale: false,
                        isRange: true,
                        onstatechange: function () {
                            var time = Math.floor($('.slider-input').val().split(',')[1]);
                            if (time) {
                                $("#ShowVideo")[0].currentTime = time;
                            }
                        }
                    });
                }
            });
        }
    } else if (pics.indexOf(extName) !== -1) {
        $('#imgProduct').attr('src', objUrl).show();
    } else if (audios.indexOf(extName) !== -1) {
        // alert(objUrl);
        $('#showAudio').attr('src', objUrl).css('display', 'block');
    } else {
        $('#otherProduct').attr('href', objUrl).text(fileName).show();
    }
    $('#uploadVideoBox').hide();
    $('.delete-video-btn').show();
});

$("#cutVideoCheck").on('click', function () {
    if ($(this).prop("checked")) {
        $('#cutVideoTips').remove();
        $('#slider-input').removeClass('d-none');
    } else {
        $('#slider-input').addClass('d-none');
    }
});


function showVideo(file) {
    var url = null;
    if (window.createObjectURL != undefined) { // basic
        url = window.createObjectURL(file);
    } else if (window.URL != undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file);
    } else if (window.webkitURL != undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file);
    }
    return url;
}

//删除视频
$('#delVideo').on('click', function () {
    $(".show-video-box,#imgProduct,#showAudio,#otherProduct").attr("src", '').hide();
    $('#uploadVideoBox').show();
    $('#cutVideoCheckBox,#slider-input').addClass('d-none');
    $('.delete-video-btn').hide();
    $('.slider-input').val('');
});

//删除图片
$('#uploadImgBox').on('click', '.img-li i', function (event) {
    var li = $(this).closest('.img-li');
    var id = $(this).next().find('.img-fluid').attr('id'); //消息ID
    li.remove();
    console.log(id);
    $('.swiper-wrapper').find('img#' + id).closest('.swiper-slide').remove();
});

//图片展示
$('#uploadImgBox').on('click', '.img-li img', function (e) {
    var i = $(this).closest('li').index();
    console.log(i);
    $('.swiper-container').fadeIn();
    var mySwiper = new Swiper('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        initialSlide: i,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction'
        }
    });

});

$('.close-swiper').on('click', function (e) {
    // console.log(e);
    if (e.target.tagName !== "IMG") {
        $('.swiper-container').fadeOut();
    }
});


//上传视频
function uploadVideo(productId, userid, type, cut, PreviewStartTime, PreviewDuration) {
    loading('hide');
    var token = localStorage.getItem('access_token');
    var file = $('#uploadVideoForm')[0];
    var formData = new FormData(file);
    api.setKeepScreenOn({
        keepOn: true
    });

    if (type === 'moment') {
        var url = getBaseUrl() + "api/users/" + userid + "/moments/" + productId + '/files?mediaType=video';
        // console.log('moment视频');
    } else {
        var url = getBaseUrl() + "api/users/" + userid + "/products/" + productId + '/files';
        // console.log('product视频');
    }
    var arcProgress = api.require('arcProgress');
    arcProgress.open({
        type: 'annular',
        centerX: api.frameWidth / 6 * (2 * 1 + 1),
        centerY: api.frameHeight / 2,
        radius: api.frameWidth / 6,
        bgColor: '#87c0ed',
        pgColor: '#325570',
        loopWidth: 3,
        fixedOn: api.frameName
    }, function (ret, err) {
    });
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        async: true,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        headers: {"Authorization": "Bearer " + token},
        xhr: function () {
            myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', function (e) {
                    if (e.lengthComputable) {
                        var percent = Math.floor(e.loaded / e.total * 100);
                        if (percent <= 100) {
                            arcProgress.setValue({
                                id: 1,
                                value: percent
                            });
                            $(".percentage").show();
                            $(".percentage p").text(percent + '%');
                        }
                        if (percent >= 100) {
                            arcProgress.close({
                                id: 1
                            });
                            $(".percentage p").text('');
                            $(".percentage").hide();
                            loading('show');
                        }
                    }
                }, false);
            }
            return myXhr;
        },
        success: function (response) {
            //console.log(JSON.stringify(response));
            if (cut) {
                cutVideo(productId, userid, PreviewStartTime, PreviewDuration)
            }
            loading('hide');
            //console.log(JSON.stringify(type));
            if (type === 'moment') {
                localStorage.setItem('openFrame', '');
                api.closeFrame({
                    name: 'addMoment'
                });
            } else {
                confirmBack();
            }

            api.setKeepScreenOn({
                keepOn: false
            });
        },
        error: function (xhr) {
            console.log(JSON.stringify(xhr));
            loading('hide');
            messages('Upload error!', 'error', 'bottom', 3);
            $('.container').removeClass('invisible');
        }

    })
}


function cutVideo(productId, userId, PreviewStartTime, PreviewDuration) {
    var token = localStorage.getItem('access_token');
    var url = getBaseUrl() + "api/users/" + userId + "/products/" + productId + "/cut";
    var method = "POST";
    var contentTypeStr = "application/json";
    var prodObj = {"PreviewStartTime": PreviewStartTime, "PreviewDuration": PreviewDuration};
    $.when(postApi(url, method, prodObj, token, contentTypeStr)).done(function (data) {
    });
}
