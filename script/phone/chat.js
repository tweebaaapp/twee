apiready = function () {
    UIChatBox = api.require('UIChatBox');
    fnOpen();
    var conversationType = api.pageParam.conversationType;
    var username = api.pageParam.userName;
    var userId = api.pageParam.userId;
    var fontSize = localStorage.getItem('chatFontSize');
    if (fontSize) {
        var style = '<style> #showChat span.sand-msg, #showChat span.get-msg\{font-size: ' + fontSize + 'px;\}</style>';
        $('head').append(style);
    }
    $('#friendsName').text(username);
    if (username === '') {
        var rong = api.require('rongCloud2');
        rong.getDiscussion({
            discussionId: userId
        }, function (ret, err) {
            $('#friendsName').text(ret.result.name)
        })
    }
    initRongAndListener();

    api.addEventListener({
        name: 'swiperight'
    }, function (ret, err) {
        back();
    });

    //监听@列表返回
    api.addEventListener({
        name: 'mention'
    }, function (ret, err) {
        UIChatBox.insertValue({
            msg: ret.value.name + ' '
        });
        UIChatBox.popupKeyboard();
    });
    //监听更改群名称
    api.addEventListener({
        name: 'getGroupInfo'
    }, function (ret, err) {
        getGroupInfo();
    });
    //切换显示图标
    if (conversationType === 'DISCUSSION') {
        $('.chat-single-icon').hide();
    } else {
        $('.chat-group-icon').hide();
    }
    //返回前台后重新连接
    api.addEventListener({
        name:'resume'
    }, function(ret, err){
        initRongAndListener();
    });
    // //开启连接并开启监听
// alert($('#showChat').length);
    function initRongAndListener() {
        //init & connect
        var rong = api.require('rongCloud2');
        rong.init(function (ret, err) {
            if (ret.status === 'error') {
                //console.log("init-r:" + JSON.stringify(err));
            } else {
                //console.log("init-s:" +JSON.stringify(ret));
            }
        });
        setConnect();

        // msgListener();
        function setConnect() {
            var rong = api.require('rongCloud2');
            rong.setConnectionStatusListener(function (ret, err) {
                //console.log(JSON.stringify(ret));
                var statuMsg = "";
                var rong_statu = ret.result.connectionStatus;
                switch (rong_statu) {
                    case "CONNECTED":
                        statuMsg = "连接成功";
                        break;
                    case "CONNECTING":
                        statuMsg = "连接中";
                        break;
                    case "DISCONNECTED":
                        statuMsg = "断开连接";
                        break;
                    case "KICKED":
                        statuMsg = "用户账户在其他设备登录，本机会被踢掉线";
                        break;
                    case "NETWORK_UNAVAILABLE":
                        statuMsg = "网络不可用";
                        break;
                    case "SERVER_INVALID":
                        statuMsg = "服务器异常或无法连接";
                        break;
                    case "TOKEN_INCORRECT":
                        statuMsg = "Token 不正确";
                        break;
                    default:
                        statuMsg = "未知错误";
                        break;
                }
                console.log("连接状态:" + statuMsg);

            });
        }

        chatListener();

        //融云消息监听器
        function chatListener() {
            var rong = api.require('rongCloud2');
            rong.setOnReceiveMessageListener(function (ret, err) {
                if (ret) {
                    if (ret.result.message.objectName === "RC:CmdMsg") {
                        if (ret.result.message.content.name === "AddFriend") {
                            var usernameData = JSON.parse(ret.result.message.content.data);
                            var username = usernameData.username;
                            var sendUserId = ret.result.message.senderUserId;
                            // console.log(username +" +  "+ sendUserId);
                            saveFriendList(username, sendUserId);
                            // getApplcationList();
                            api.sendEvent({
                                name: 'contactListReload'
                            });
                            api.sendEvent({
                                name: 'conversationListReload'
                            });
                            api.notification({
                                vibrate: [500, 500],
                                sound: 'default',
                                notify: {
                                    title: 'Tweebaa',
                                    content: $.i18n.prop('havenewMessage') // you have a new message
                                }
                            }, function (ret, err) {

                            });
                        } else if (ret.result.message.content.name === "delFriend") {
                            removeConversation(ret.result.message.senderUserId, 'PRIVATE');
                            api.sendEvent({
                                name: 'contactListReload'
                            });
                            api.sendEvent({
                                name: 'conversationListReload'
                            });
                        }

                    }
                    //console.log("聊天：" + JSON.stringify(ret));
                    if (ret.result.message.targetId !== api.pageParam.userId) {
                        return false;
                    }
                    var d = moment.utc(ret.result.message.sentTime).local().format('HH:mm');
                    if (ret.result.message.content.extra) {
                        var extra = (JSON.parse(ret.result.message.content.extra));
                        var sendName = extra.sendName;
                        var portrait = extra.sendPortrait;
                    }
                    if (ret.result.message.objectName == 'RC:TxtMsg') {
                        var extra = JSON.parse(ret.result.message.content.extra);
                        if (extra.agree == true) {
                            deleteRecordList(ret.result.message.senderUserId);
                            return false;
                        }
                        var msg = transEmo(ret.result.message.content.text).replace('" class="emo">', '');
                        var template = '<div id="' + ret.result.message.messageId + '" class="chat-box">' +
                            '<div class="chat-user-box pull-left">\n' +
                            '          <img src="' + portrait + '" class="user-box-header" data-name="' + ret.result.message.senderUserId + '" onclick="openUserProfile(\'' + ret.result.message.senderUserId + '\',\'' + sendName + '\',\'' + portrait + '\')">\n' +
                            '        </div>' +
                            '<p  class="user-box-username-get x-small">' + sendName + '</p>' +
                            '<span  class="get-msg">' + msg + '</span>' +
                            '<p class="get-msg-time">' + d + '</p>' +
                            '</div>';
                        $('#showChat').append(template);
                    } else if (ret.result.message.objectName == 'RC:VcMsg') {
                        var template = '<div id="' + ret.result.message.messageId + '" class="chat-box">' +
                            '<div class="chat-user-box pull-left">\n' +
                            '          <img src="' + portrait + '" class="user-box-header" data-name="' + ret.result.message.senderUserId + '" onclick="openUserProfile(\'' + ret.result.message.senderUserId + '\',\'' + sendName + '\',\'' + portrait + '\')">\n' +
                            '        </div>' +
                            '<p  class="user-box-username-get x-small">' + sendName + '</p>' +
                            '<span  class="get-msg  voice text-left" onclick="play(this,\'' + ret.result.message.content.voicePath + '\')"><i class="icon ion-radio-waves pr-5"></i> ' + ret.result.message.content.duration + '\" </span>' +
                            '<p class="get-msg-time">' + d + '</p>' +
                            '</div>';
                        $('#showChat').append(template);
                    } else if (ret.result.message.objectName == 'RC:ImgMsg') {
                        var template = '<div id="' + ret.result.message.messageId + '" class="chat-box">' +
                            '<div class="chat-user-box pull-left">\n' +
                            '          <img src="' + portrait + '" class="user-box-header" data-name="' + ret.result.message.senderUserId + '" onclick="openUserProfile(\'' + ret.result.message.senderUserId + '\',\'' + sendName + '\',\'' + portrait + '\')">\n' +
                            // '          <div class="user-box-username ui-nowrap x-small">' + sendName + '</div>\n' +
                            '        </div>' +
                            '<p  class="user-box-username-get x-small">' + sendName + '</p>' +
                            '<span  class="get-msg  image text-left" onclick="openImage(\'' + ret.result.message.content.imageUrl + '\')"><img class="chat-img img-fluid" src="' + ret.result.message.content.thumbPath + '"/></span>' +
                            '<p class="get-msg-time">' + d + '</p>' +
                            '</div>';
                        $('#showChat').append(template);
                    } else if (ret.result.message.objectName == 'RC:DizNtf') {
                        var text = ret.result.message.content.extension;
                        var lastMessage;
                        if (ret.result.message.content.type == 1) {
                            if (ret.result.message.targetId === api.pageParam.userId) {
                                lastMessage = (text.split(',')[0]).split('@')[0] + ' ... ' + $.i18n.prop('joinGroup');  /// 加群
                            } else {
                                lastMessage = ''
                            }
                        } else if (ret.result.message.content.type == 2) {
                            if (ret.result.message.targetId === api.pageParam.userId) {
                                lastMessage = text.split('@')[0] + ' ' + $.i18n.prop('quitGroup');  /// 退群
                            } else {
                                lastMessage = ''
                            }
                        } else if (ret.result.message.content.type == 3) {
                            if (ret.result.message.targetId === api.pageParam.userId) {
                                lastMessage = $.i18n.prop('changeGroupNameTo') + ' ' + text;
                            } else {
                                lastMessage = ''
                            }
                        } else if (ret.result.message.content.type == 4) {
                            if (text === localStorage.getItem('chatID')) {
                                lastMessage = $.i18n.prop('you') + ' ' + $.i18n.prop('kickGroup');
                                rong.clearMessages({
                                    conversationType: 'DISCUSSION',
                                    targetId: ret.result[i].message.targetId
                                }, function (ret, err) {
                                });
                                removeConversation(api.pageParam.userId, "DISCUSSION");
                            } else {
                                lastMessage = text.split('@')[0] + ' ' + $.i18n.prop('kickGroup');
                            }
                        }
                        if (lastMessage !== '') {
                            var template = '<div class="chat-day ui-nowrap">' + lastMessage + '</div>';
                            $('#showChat').append(template);
                        }

                    }
                    window.scrollTo(0, document.body.scrollHeight);
                    var messageId = ret.result.message.messageId;
                    rong.setMessageReceivedStatus({
                        messageId: messageId,
                        receivedStatus: 'READ'
                    });
                } else {
                    //console.log('监听error：' + JSON.stringify(err));
                }
            })
        }

    }

    //获取聊天记录
    getChatListFunction();

    //监听语音输入

    UIChatBox.addEventListener({
        target: 'recordBtn',
        name: 'press'
    }, function (ret, err) {
        sj1 = new Date().getTime();
        $('.mic').removeClass('d-none');
        if (ret) {
            var savePath = 'fs://lysd_' + (+new Date()) + '.amr';
            api.startRecord({
                path: savePath
            });
        }
    });
    UIChatBox.addEventListener({
        target: 'recordBtn',
        name: 'move_out_cancel'
    }, function (ret, err) {
        stopRecord();
        api.toast({
            msg: 'Slide to cancel',
            duration: 2000,
            location: 'bottom'
        });
        $('.mic').addClass('d-none');
    });

    UIChatBox.addEventListener({
        target: 'recordBtn',
        name: 'press_cancel'
    }, function (ret, err) {
        $('.mic').addClass('d-none');
        var sj2 = new Date().getTime() - sj1;
        if (sj2 <= 500) {
            stopRecord();
            api.toast({
                msg: $.i18n.prop('recodShort'),//'语音时间较短，请重新录音'
                duration: 3000,
                location: 'bottom'
            });
        } else if (sj2 > 500) {
            api.stopRecord(function (ret, err) {
                // console.log("录音了:" +JSON.stringify(ret));
                var userId = api.pageParam.userId;
                // console.log(userId);
                if (ret.duration > 0) {
                    var path = ret.path;
                    var duration = ret.duration;
                    var extra = {
                        sendName: localStorage.getItem('storename'),
                        sendPortrait: localStorage.getItem('chatPortrait'),
                        receiveName: username,
                        receivePortrait: api.pageParam.userPortrait,
                        Source: 'Friend'
                    };
                    var rong = api.require('rongCloud2');
                    rong.sendVoiceMessage({
                        conversationType: api.pageParam.conversationType,
                        targetId: userId,
                        voicePath: path,
                        duration: duration,
                        extra: JSON.stringify(extra)
                    }, function (ret, err) {
                        // console.log("语音:" +JSON.stringify(ret));
                        // console.log("语音:" +JSON.stringify(err));
                        if (ret.status == 'prepare') {
                            messageId = ret.result.message.messageId;
                            yy = ret.result.message.content.voicePath;
                            sj_len = ret.result.message.content.duration;
                        } else if (ret.status == 'success') {
                            var name = localStorage.getItem('storename');
                            var t = '<div class="chat-box"  id="' + messageId + '">' +
                                '<div class="chat-user-box pull-right">\n' +
                                '          <img src="' + localStorage.getItem('chatPortrait') + '" class="user-box-header">\n' +
                                '        </div>' +
                                '<p  class="user-box-username-send x-small">' + name + '</p>' +
                                '<span  class="sand-msg voice text-right" onclick="play(this,\'' + yy + '\')"> ' + sj_len + '\" <i class="icon ion-radio-waves pl-5"></i></span>' +
                                '</div>';
                            $('section').append(t);
                            window.scrollTo(0, document.body.scrollHeight);
                            aiLen = 0; //清楚@计数
                        } else if (ret.status == 'error') {
                            if (err.code === 21406) {
                                messages($.i18n.prop('notInGroup'), 'error', 'top', 3);
                            } else if (err.code === 405) {
                                messages($.i18n.prop('inBlacklist'), 'error', 'top', 3);
                            } else {
                                messages($.i18n.prop('sendError'), 'error', 'top', 3);
                            }
                        }
                    });
                }
            });
        }
    });

    //监听@输入
    if (api.pageParam.conversationType === 'DISCUSSION') {
        var groupId = userId;
        UIChatBox.addEventListener({
            target: 'inputBar',
            name: 'valueChanged'
        }, function (ret, err) {
            if (ret.value.split("@").length - 1 > aiLen) {
                aiLen++;
                UIChatBox.closeKeyboard();
                localStorage.setItem('openFrame', 'groupList');
                api.openFrame({
                    name: 'groupList',
                    url: 'group-list.html',
                    bounces: false,
                    animation: {
                        type: "movein",
                        subType: "from_right"
                    },
                    pageParam: {
                        groupId: groupId,
                        listType: 'groupList',
                        text: ''
                    }
                });
            }
        });
    }

    //长按头像@
    $('section').on('touchstart', '.chat-box .user-box-header', function (event) {
        var timeout = undefined;
        var that = $(this);
        var $this = this;
        var name = $(this).attr('data-name');
        var msg;
        that.addClass('opacity-5');
        timeout = setTimeout(fn, 500);

        function fn() {
            if (name) {
                if (api.pageParam.conversationType === 'DISCUSSION') {
                    msg = '@' + name.split('@')[0] + ' ';
                } else {
                    msg = name.split('@')[0] + ' ';
                }
                aiLen++;
                var UIChatBox = api.require('UIChatBox');
                UIChatBox.insertValue({
                    msg: msg
                });
                UIChatBox.popupKeyboard();
            }
        }

        $this.addEventListener('touchend', function (event) {
            clearTimeout(timeout);
            that.removeClass('opacity-5');
        }, false);
    });

//清除系统提示
    api.cancelNotification({
        id: -1
    });
    //载入聊天输入框模块
    // setTimeout(function () {
    //     fnOpen();
    // }, 1000);
};
//var aiLen = 0; // 记录已经输入的@个数
//获取群信息
function getGroupInfo() {
    var rong = api.require('rongCloud2');
    rong.getDiscussion({
        discussionId: api.pageParam.userId
    }, function (ret, err) {
        if (ret.status == 'success')
            $('#friendsName').text(ret.result.name);
        else
            api.toast({msg: err.code});
    })
}

function getPicture(type) {

    api.getPicture({
        sourceType: type,
        encodingType: 'jpg',
        mediaValue: 'pic',
        destinationType: 'url',
        allowEdit: false,
        quality: 70,
        saveToPhotoAlbum: false
    }, function (ret, err) {
        if (ret) {
            var userId = api.pageParam.userId;
            var extra = {
                sendName: localStorage.getItem('storename'),
                sendPortrait: localStorage.getItem('chatPortrait'),
                receiveName: api.pageParam.userName || '',
                receivePortrait: api.pageParam.userPortrait,
                Source: 'Friend'
            };
            var rong = api.require('rongCloud2');
            rong.sendImageMessage({
                conversationType: api.pageParam.conversationType,
                targetId: userId,
                imagePath: ret.data,
                extra: JSON.stringify(extra)
            }, function (ret, err) {
                //console.log("照片发出:" +JSON.stringify(ret));
                if (ret.status == 'prepare') {
                    var messageId = ret.result.message.messageId;
                    var bpic = ret.result.message.content.imageUrl;
                    var spic = ret.result.message.content.thumbPath;
                    var name = localStorage.getItem('storename');
                    var t = '<div class="chat-box"  id="' + messageId + '">' +
                        '<div class="chat-user-box pull-right">\n' +
                        '          <img src="' + localStorage.getItem('chatPortrait') + '" class="user-box-header">\n' +
                        '        </div>' +
                        '<p  class="user-box-username-send x-small">' + name + '</p>' +
                        '<span  class="sand-msg image text-right" onclick="openImage(\'' + bpic + '\')"><img class="chat-img img-fluid " src="' + spic + '"/></span>' +
                        '</div>';
                    $('section').append(t);
                    window.scrollTo(0, document.body.scrollHeight);
                    aiLen = 0; //清楚@计数
                }
            });

        } else {
            if (err.code === 21406) {
                messages($.i18n.prop('notInGroup'), 'error', 'top', 3);
            } else if (err.code === 405) {
                messages($.i18n.prop('inBlacklist'), 'error', 'top', 3);
            } else {
                messages($.i18n.prop('sendError'), 'error', 'top', 3);
            }
        }
        var UIChatBox = api.require('UIChatBox');
        UIChatBox.closeBoard();
    });
}


//播放录音
function play(el, path) {
    $api.addCls(el, 'play');
    api.startPlay({
        path: path
    }, function () {
        $api.removeCls(el, 'play');
    });
}

//终止录音
function stopRecord() {
    api.stopRecord(function (ret, err) {
    });
}

//打开图像
function openImage(path) {
    var imageBrowser = api.require('imageBrowser');
    imageBrowser.openImages({
        imageUrls: [path],
        showList: false,
        tapClose: true
    });
}

//发送
function sendMessages(msg) {
    // var text = $('#inputText').val();
    if (msg !== '') {
        sendText(msg);
    } else {
        api.toast({
            msg: $.i18n.prop('blankMessage'),//'You can not send blank message'
            duration: 2000,
            location: 'bottom'
        });
    }
}

function sendText(msg) {
    var conversationType = api.pageParam.conversationType;
    var userId = api.pageParam.userId;
    var receiveName = api.pageParam.userName;
    var userPortrait = api.pageParam.userPortrait;
    var rong = api.require('rongCloud2');
    var extra = {
        sendName: localStorage.getItem('storename'),
        sendPortrait: localStorage.getItem('chatPortrait'),
        receiveName: receiveName,
        receivePortrait: userPortrait,
        Source: 'Friend'
    };
    rong.sendTextMessage({
        conversationType: conversationType,
        targetId: userId,
        text: msg,
        extra: JSON.stringify(extra)
    }, function (ret, err) {
        var messageId = ret.result.message.messageId;
        var name = localStorage.getItem('storename');
        var t = '<div class="chat-box"  id="' + messageId + '">' +
            '<div class="chat-user-box pull-right">\n' +
            '          <img src="' + localStorage.getItem('chatPortrait') + '" class="user-box-header">\n' +
            '        </div>' +
            '<p  class="user-box-username-send x-small">' + name + '</p>' +
            '<span  class="sand-msg">' + msg + '</span>' +
            '</div>';
        if (ret.status == 'success') {
            //console.log("发送成功:" +JSON.stringify(ret));
            $('section').append(t);
            window.scrollTo(0, document.body.scrollHeight);
        }
            //aiLen = 0; //清楚@计数

        if (ret.status == 'error') {
            if (err.code === 21406) {
                messages($.i18n.prop('notInGroup'), 'error', 'top', 3);
            } else if (err.code === 405) {
                messages($.i18n.prop('inBlacklist'), 'error', 'top', 3);
            } else {
                messages($.i18n.prop('sendError'), 'error', 'top', 3);
            }
        }
    });
}

function back() {
    // api.sendEvent({
    //     name: 'contactListReload'
    // });
    api.sendEvent({
        name: 'conversationListReload'
    });
    var unread = 'getTotalUnreadCount();';
    api.execScript({
        name: 'index',
        script: unread
    });
    var innt = 'initRongAndListener();';
    api.execScript({
        name: 'index',
        script: innt
    });
    UIChatBox.close();
    api.closeWin({
        name: 'chatPage'
    });
}


//长按弹出聊天菜单
$('section').on('touchstart', '.chat-box span', function (event) {
    var timeout = undefined;
    var that = $(this);
    var $this = this;
    var box = $(this).closest('.chat-box');
    var id = box.attr('id'); //消息ID
    var text = $(this).first().text();//复制用信息
    var hideBtn = '';
    that.addClass('opacity-5');
    timeout = setTimeout(fn, 800);

    function fn() {
        var x = event.originalEvent.touches[0].clientX;
        var y = event.originalEvent.touches[0].clientY;
        var width = $(window).width();
        var height = window.screen.height;
        if (x < 40) {
            x = 40;
        } else if (x > (width - 140)) {
            x = width - 140
        }
        if (y > (height - 240)) {
            y = height - 240;
        }
        if (!text) {
            hideBtn = ' d-none-im';
        }
        var delBtnDiv = '<ul class="list-group popup-select-list text-center">' +
            '<li class="list-group-item' + hideBtn + '" id="copy">' + $.i18n.prop('copy') + '</li>' +
            // '<li class="list-group-item' + hideBtn + '" id="forward">' + $.i18n.prop('forward') + '</li>' +
            '<li class="list-group-item" id="delete">' + $.i18n.prop('delete') + '</li>' +
            '</ul>';
        $('.wrap').append(delBtnDiv);
        var delBtn = $('.popup-select-list');
        delBtn.css({"top": y + 'px', "left": x + 'px'});
        $('.chat-box').addClass('no-touch');
        document.body.style.overflow = 'hidden';
        $('section').one("click", function (e) {
            // console.log("aaa" + e.target.className);
            if (e.target.className !== "list-group-item") {
                $('.chat-box').removeClass('no-touch');
                document.body.style.overflow = 'auto';
                delBtn.remove();
            }
        });
        $('#forward.list-group-item').one('click', function () {
            forwardMessage(text);
            $('.chat-box').removeClass('no-touch');
            document.body.style.overflow = 'auto';
            delBtn.remove();
        });
        $('#delete.list-group-item').one('click', function () {
            deleteMessages(id);
            box.remove();
            $('.chat-box').removeClass('no-touch');
            document.body.style.overflow = 'auto';
            delBtn.remove();
        });
        $('#copy.list-group-item').one('click', function () {
            var clipBoard = api.require('clipBoard');
            clipBoard.set({
                value: text
            }, function (ret, err) {
                if (ret) {
                    messages($.i18n.prop('copied'), 'success', 'top', 2);
                    $('.chat-box').removeClass('no-touch');
                    document.body.style.overflow = 'auto';
                    delBtn.remove();
                }

            });

        })
    }

    $this.addEventListener('touchend', function (event) {
        clearTimeout(timeout);
        that.removeClass('opacity-5');
    }, false);
});


//删除信息
function deleteMessages(id) { //传入的Id是包含message ID 的数组,可以单个或多个删除

    var deleteID = [];
    deleteID.push(parseInt(id));
    var rong = api.require('rongCloud2');
    rong.deleteMessages({
        messageIds: deleteID
    }, function (ret, err) {

    })
}

//转发信息
function forwardMessage(text) {
    var groupId = api.pageParam.userId;
    localStorage.setItem('openFrame', 'groupList');
    api.openFrame({
        name: 'groupList',
        url: 'group-list.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            groupId: groupId,
            listType: 'forwardList',
            text: text
        }
    });

}

//open chat profile
function chatProfile() {
    var username = api.pageParam.userName;
    var userId = api.pageParam.userId;
    var userPortrait = api.pageParam.userPortrait;
    var conversationType = api.pageParam.conversationType;
    var isTop = api.pageParam.isTop;
    localStorage.setItem('openFrame', 'chatProfile');
    api.openFrame({
        name: 'chatProfile',
        url: 'chat-profile.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            userId: userId,
            userName: username,
            userPortrait: userPortrait,
            conversationType: conversationType,
            isTop: isTop
        }
    });
}

//删除会话
function removeConversation(chatId, type) {
    //console.log(chatId);
    var rong = api.require('rongCloud2');
    rong.removeConversation({
        conversationType: type,
        targetId: chatId
    }, function (ret, err) {
        //console.log(JSON.stringify(ret));
        api.sendEvent({
            name: 'conversationListReload'
        });
    })
}

//聊天记录
var lastId;
var first = false;

function getChatListFunction() {
    loading('show');
    var rong = api.require('rongCloud2');
    rong.getHistoryMessages({
        conversationType: api.pageParam.conversationType,
        targetId: api.pageParam.userId,
        oldestMessageId: lastId || -1,
        count: 12
    }, function (ret, err) {
        loading('hide');
        //$('section').html('');
        //console.log("聊天记录:" + JSON.stringify(ret));
        if (ret.result) {
            var len = ret.result.length;
            var chatDate;
            if (len !== 0) {
                lastId = ret.result[len - 1].messageId;
                for (i = 0; i < len; i++) {
                    //写入日期
                    if (i === 0) {
                        var d = moment.utc(ret.result[0].sentTime).local().format('MM-DD');
                        chatDate = d;
                    } else {
                        var d = moment.utc(ret.result[i].sentTime).local().format('MM-DD');
                        if (d !== chatDate) {
                            var dd = moment.utc(ret.result[i - 1].sentTime).local().format('MM-DD');
                            var t = '<div class="chat-day">' + dd + '</div>';
                            chatDate = d;
                            $('section').prepend(t);
                        }
                    }
                    var messageId = ret.result[i].messageId;
                    var rong = api.require('rongCloud2');
                    rong.setMessageReceivedStatus({
                        messageId: messageId,
                        receivedStatus: 'READ'
                    }, function (ret, err) {
                        // console.log("设置为已读:" +JSON.stringify(ret.status));
                    });
                    var d = moment.utc(ret.result[i].sentTime).local().format('HH:mm');
                    var msgData;
                    var senderUserId = ret.result[i].senderUserId;
                    if (ret.result[i].content.extra) {
                        msgData = JSON.parse(ret.result[i].content.extra)
                    } else {
                        msgData = {"sendName": "", "sendPortrait": ""};
                    }
                    var name = msgData.sendName;
                    var portrait = msgData.sendPortrait;
                    var bigText = name.split('')[0];
                    if (ret.result[i].objectName == 'RC:TxtMsg') {
                        var msg = ret.result[i].content.text;
                        if (ret.result[i].messageDirection === "SEND") {
                            var t = '<div class="chat-box" id="' + messageId + '">' +
                                '<div class="chat-user-box pull-right">\n' +
                                '          <img src="' + localStorage.getItem('chatPortrait') + '" class="user-box-header">\n' +
                                '        </div>' +
                                '<p  class="user-box-username-send x-small">' + name + '</p>' +
                                '<span  class="sand-msg">' + msg + '</span>' +
                                '<p class="sand-msg-time">' + d + '</p>' +
                                '</div>';
                        } else {
                            var t = '<div  class="chat-box" id="' + messageId + '">' +
                                '<div class="chat-user-box pull-left">\n' +
                                '          <img src="' + portrait + '" class="user-box-header" data-name="' + senderUserId + '" onclick="chatProfile()">\n' +
                                '        </div>' +
                                '<p  class="user-box-username-get x-small">' + name + '</p>' +
                                '<span  class="get-msg">' + msg + '</span>' +
                                '<p class="get-msg-time">' + d + '</p>' +
                                '</div>';
                        }
                    } else if (ret.result[i].objectName == 'RC:VcMsg') {
                        if (ret.result[i].messageDirection === "SEND") {
                            var t = '<div class="chat-box" id="' + messageId + '">' +
                                '<div class="chat-user-box pull-right">\n' +
                                '          <img src="' + localStorage.getItem('chatPortrait') + '" class="user-box-header">\n' +
                                '        </div>' +
                                '<p  class="user-box-username-send x-small">' + name + '</p>' +
                                '<span  class="sand-msg  voice text-right" onclick="play(this,\'' + ret.result[i].content.voicePath + '\')"> ' + ret.result[i].content.duration + '\" <i class="icon ion-radio-waves pl-5"></i></span>' +
                                '<p class="sand-msg-time">' + d + '</p>' +
                                '</div>';
                        } else {
                            var t = '<div class="chat-box"  id="' + messageId + '">' +
                                '<div class="chat-user-box pull-left">\n' +
                                '<img src="' + portrait + '" class="user-box-header" data-name="' + senderUserId + '" onclick="chatProfile()">\n' +
                                '        </div>' +
                                '<span  class="user-box-username-get x-small">' + name + '</span>' +
                                '<span  class="get-msg  voice text-left" onclick="play(this,\'' + ret.result[i].content.voicePath + '\')"><i class="icon ion-radio-waves pr-5"></i> ' + ret.result[i].content.duration + '\" </span>' +
                                '<span class="get-msg-time">' + d + '</span>' +
                                '</div>';
                        }

                    } else if (ret.result[i].objectName == 'RC:ImgMsg') {
                        if (ret.result[i].messageDirection === "SEND") {
                            var t = '<div class="chat-box"  id="' + messageId + '">' +
                                '<div class="chat-user-box pull-right">\n' +
                                '          <img src="' + localStorage.getItem('chatPortrait') + '" class="user-box-header">\n' +
                                '        </div>' +
                                '<p  class="user-box-username-send x-small">' + name + '</p>' +
                                '<span  class="sand-msg  image text-right" onclick="openImage(\'' + ret.result[i].content.imageUrl + '\')"><img class="chat-img img-fluid" src="' + ret.result[i].content.thumbPath + '"/></span>' +
                                '<p class="sand-msg-time">' + d + '</p>' +
                                '</div>';
                        } else {
                            var t = '<div class="chat-box" id="' + messageId + '">' +
                                '<div class="chat-user-box pull-left">\n' +
                                '<img src="' + portrait + '" class="user-box-header" data-name="' + senderUserId + '" onclick="chatProfile()">\n' +
                                '        </div>' +
                                '<p  class="user-box-username-get x-small">' + name + '</p>' +
                                '<span  class="get-msg  image text-left" onclick="openImage(\'' + ret.result[i].content.imageUrl + '\')"><img class="chat-img img-fluid" src="' + ret.result[i].content.thumbPath + '"/></span>' +
                                '<p class="get-msg-time">' + d + '</p>' +
                                '</div>';
                        }
                    } else if (ret.result[i].objectName == 'RC:DizNtf') {
                        var text = ret.result[i].content.extension;
                        var lastMessage;
                        if (ret.result[i].content.type == 1) {
                            lastMessage = (text.split(',')[0]).split('@')[0] + ' ... ' + $.i18n.prop('joinGroup');  /// 加群
                        } else if (ret.result[i].content.type == 2) {
                            lastMessage = text.split('@')[0] + ' ' + $.i18n.prop('quitGroup');  /// 退群
                        } else if (ret.result[i].content.type == 3) {
                            lastMessage = $.i18n.prop('changeGroupNameTo') + ' ' + text;
                        } else if (ret.result[i].content.type == 4) {
                            if (text === localStorage.getItem('chatID')) {
                                lastMessage = $.i18n.prop('you') + $.i18n.prop('kickGroup');
                                var rong = api.require('rongCloud2');
                                rong.clearMessages({
                                    conversationType: 'DISCUSSION',
                                    targetId: ret.result[i].targetId
                                }, function (ret, err) {
                                });
                                removeConversation(ret.result[i].targetId, "DISCUSSION");
                            } else {
                                lastMessage = text.split('@')[0] + ' ' + $.i18n.prop('kickGroup');
                            }

                        }
                        var t = '<div class="chat-day ui-nowrap">' + lastMessage + '</div>';
                    }

                    $('section').prepend(t);
                    //window.scrollTo(0, document.body.scrollHeight);
                }
                //写入最前日期
                var d = moment.utc(ret.result[len - 1].sentTime).local().format('MM-DD');
                var ti = '<div class="chat-day">' + d + '</div>';

                $('section').prepend(ti);
            }
            if (!first) {
                first = true;
                setTimeout(function () {
                    window.scrollTo(0, document.body.scrollHeight);
                }, 0);
            }

        }
    });
}

$(document).scroll(function () {
    if ($(document).scrollTop() === 0 && $(document).height() > (api.frameHeight + 100)) {
        getChatListFunction();
    }
});

//打开用户个人信息页面
function openUserProfile(id, name, userPortrait) {

    if (id === localStorage.getItem('chatID')) {
        return false;
    }
    localStorage.setItem('openFrame', 'userProfile');
    api.openFrame({
        name: 'userProfile',
        url: '../users/user-profile.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            userId: id,
            userName: name,
            userPortrait: userPortrait,
            conversationType: api.pageParam.conversationType,
            backtochat: true
        }
    });
}
