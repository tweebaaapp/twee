
// //开启连接并开启监听
// alert($('#showChat').length);
function initRongAndListener(page) {
  //init & connect
  var rong = api.require('rongCloud2');
  rong.init(function (ret, err) {
    if (ret.status === 'error') {
      //console.log("init-r:" + JSON.stringify(err));
    } else {
      //console.log("init-s:" +JSON.stringify(ret));
    }
  });
  setConnect();

  // msgListener();
  function setConnect() {
    rong.setConnectionStatusListener(function (ret, err) {
      //console.log(JSON.stringify(ret));
      var statuMsg = "";
      var rong_statu = ret.result.connectionStatus;
      switch (rong_statu) {
        case "CONNECTED":
          statuMsg = "连接成功";
          api.sendEvent({
            name: 'conversationListReload'
          });
          break;
        case "CONNECTING":
          statuMsg = "连接中";
          break;
        case "DISCONNECTED":
          statuMsg = "断开连接";
          break;
        case "KICKED":
          statuMsg = "用户账户在其他设备登录，本机会被踢掉线";
          break;
        case "NETWORK_UNAVAILABLE":
          statuMsg = "网络不可用";
          break;
        case "SERVER_INVALID":
          statuMsg = "服务器异常或无法连接";
          break;
        case "TOKEN_INCORRECT":
          statuMsg = "Token 不正确";
          break;
        default:
          statuMsg = "未知错误";
          break;
      }
      console.log("连接状态:" + statuMsg);

    });
  }

  msgListener();

  //融云消息监听器
  function msgListener() {
    var chatpage = $('#chatPage').val();
    rong.setOnReceiveMessageListener(function (ret, err) {
      if (ret) {
      //console.log("聊天：" + JSON.stringify(ret));
        if (ret.result.message.objectName == "RC:CmdMsg") {
          if(ret.result.message.content.name ==="AddFriend"){
              var usernameData = JSON.parse(ret.result.message.content.data);
              var username = usernameData.username;
              var sendUserId = ret.result.message.senderUserId;
              saveFriendList(username, sendUserId);
              $('#contactBtn').addClass('unread');
              postNotification();
          }else if(ret.result.message.content.name ==="delFriend"){
              removeConversation(ret.result.message.senderUserId,'PRIVATE');
          }

        }

        if (chatpage === 'chatpage') {
          if (ret.result.message.objectName == 'RC:TxtMsg') {
            var extra = JSON.parse(ret.result.message.content.extra);
            if (extra.agree == true) {
              deleteRecordList(ret.result.message.senderUserId);
              getFriendList();
              return false;
            }
            var msg = transEmo(ret.result.message.content.text);
            var template = '<div  class="chat-box"><span  class="get-msg">' + msg + '</span></div>';
            $('section').append(template);
            window.scrollTo(0, document.body.scrollHeight);
            var messageId = ret.result.message.messageId;
            rong.setMessageReceivedStatus({
              messageId: messageId,
              receivedStatus: 'READ'
            }, function (ret, err) {
              //console.log("设置为已读:" +JSON.stringify(ret.status));
            });
          } else if (ret.result.message.objectName == 'RC:VcMsg') {
            var template = '<div class="chat-box"><span  class="get-msg  voice text-left" onclick="play(this,\'' + ret.result.message.content.voicePath + '\')"><i class="icon ion-radio-waves pr-5"></i> ' + ret.result.message.content.duration + '\" </span></div>';
            $('#showChat').append(template);
            window.scrollTo(0, document.body.scrollHeight);
            var messageId = ret.result.message.messageId;
            rong.setMessageReceivedStatus({
              messageId: messageId,
              receivedStatus: 'READ'
            }, function (ret, err) {
              //console.log("设置为已读:" +JSON.stringify(ret.status));
            });
          } else if (ret.result.message.objectName == 'RC:ImgMsg') {
            var template = '<div class="chat-box"><span  class="get-msg  image text-left" onclick="openImage(\'' + ret.result.message.content.imageUrl + '\')"><img class="chat-img img-fluid" src="' + ret.result.message.content.thumbPath + '"/></span></div>';
            $('section').append(template);
            window.scrollTo(0, document.body.scrollHeight);
            var messageId = ret.result.message.messageId;
            rong.setMessageReceivedStatus({
              messageId: messageId,
              receivedStatus: 'READ'
            }, function (ret, err) {
              //console.log("设置为已读:" +JSON.stringify(ret.status));
            });
          }

        } else {
          //删除等待好友同意信息
          if (ret.result.message.objectName == 'RC:TxtMsg') {
            var extra = JSON.parse(ret.result.message.content.extra);
            if (extra.agree == true) {
              deleteRecordList(ret.result.message.senderUserId);
              getFriendList();
            }
          }
          // getConversationList();
          // getTotalUnreadCount();

        }
          getTotalUnreadCount();
          // api.sendEvent({
          //     name: 'contactListReload'
          // });
          api.sendEvent({
              name: 'conversationListReload'
          });
      } else {
        console.log('监听error：' + JSON.stringify(err));
      }
    })

  }

  rong.connect({
    token: localStorage.getItem('chatToken')
  }, function (ret, err) {
    if (ret.status === 'success') {
      getTotalUnreadCount();
    } else {
      console.log("connect-r:" + JSON.stringify(err));
    }
  });


}

//注册或获取Token
function getToken() {
  var tt;
  // if (getApi() === 'com') {
  //   var chatUserId = localStorage.getItem('userName');
  // } else if (getApi() === 'cn') {
  //   var chatUserId = localStorage.getItem('userName');
  // }
    var chatUserId = localStorage.getItem('userName')+'@new';
  //console.log("用户ID" + chatUserId);
  //会员id
  var chatUserName = localStorage.getItem('storename') || chatUserId;
  //console.log("用户名" + chatUserName);
  //会员昵称
  var portraitUri = 'https://api.tweebucks.com/tweebaa/downloadFile/default.jpg';
  //会员头像

  // var nonce = Math.floor(Math.random() * 1000000);
  // //随机数
  // var timestamp = Date.now();
  //时间戳
  // var signature = sha1("" + appSecret + nonce + timestamp);
  //数据签名(通过哈希加密计算)
  var paramStr = {
    "userId": chatUserId,
    "name": chatUserName,
    "portraitUri": portraitUri
  };
  var url = "https://api.tweebucks.com/tweebaa/sdkregister";
  var token = "IM";
  var method = "POST";
  var contentTypeStr = "application/json";
  //console.log("paramStr" + JSON.stringify(paramStr));
  $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
    //console.log(JSON.stringify(data));
    if (data.token) {
      localStorage.setItem('chatToken', data.token);
      localStorage.setItem('chatID', data.userId);
      localStorage.setItem('chatName', data.name);
      localStorage.setItem('chatPortrait', data.portraitUri);
      localStorage.setItem('contacts', JSON.stringify(data.contacts));
      initRongAndListener();
      clearTimeout(tt);
    } else {
      tt = setTimeout(function () {
        getToken();
      },3000);
      console.log("获取token失败");
    }

  });

}

//存储朋友请求
function saveFriendList(username, sendUserId) {
  if (localStorage.getItem('friendList')) {
    var sList = (JSON.parse(localStorage.getItem("friendList")));
    var list = {"sendUserId": sendUserId, "username": username};
    sList.push(list);
    var newList = JSON.stringify(sList);
    localStorage.setItem('friendList', newList);
    //console.log(localStorage.getItem('friendList'));
  } else {

    var list = [{"sendUserId": sendUserId, "username": username}];
    var newList = JSON.stringify(list);
    localStorage.setItem('friendList', newList);
    // console.log(localStorage.getItem('friendList'));
  }
}
