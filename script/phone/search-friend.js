function showMyQrCode() {
    $('#qrCodeBox').css('display', 'flex');
}

$('#qrCodeBox .close').on('click', function () {
    $('#qrCodeBox').fadeOut();
});

var qrcode = new QRCode("qrcode");

function makeCode() {
    var elText = localStorage.getItem('chatID');
    //var elText = 'eric16@International';
    if (!elText) {
        return;
    }
    qrcode.makeCode(elText);
}


//下面是搜索朋友相关
//打开搜索页面
$('#friendName').on('input', function () {
    if ($(this).val().length > 0) {
        $('.choose-add-friend-method,.show-my-name').fadeOut();
    } else {
        $('.choose-add-friend-method,.show-my-name').fadeIn();
        $('.clear-result').fadeOut();
        $('#searchFriendsList').empty();
    }
});

$('#searchFriendForm').submit(function (event) {
    event.preventDefault();
    $('#friendName').blur();
    searchFriend();
});

$('.clear-result,.ui-icon-close').on('click', function () {
    $('#friendName').val('');
    $('#searchFriendsList').html('');
    $('.clear-result').fadeOut();
    setTimeout(function () {
        $('.choose-add-friend-method,.show-my-name').fadeIn();
    }, 500);
});

function searchFriend(name) {
    var myId = localStorage.getItem('userId');
    var id = myId ?"&viewerId="+myId:"";
    // var searchType = $('#searchType').val();
    var friendName = name || $.trim($('#friendName').val());
    if (friendName) {
        loading('show');
        var url = getBaseUrl() + "api/users?q="+ friendName +id ;
        $('#friendName').blur();
        $('#searchFriendsList').empty();
        // var paramStr = {
        //     "itemStr": friendName,
        //     "searchType": searchType,
        //     "ownerId": localStorage.getItem('chatID'),
        //     "start": 0,
        //     "pageSize": 30
        // };
        // var url = "https://api.tweebucks.com/tweebaa/getUserList";
        var method = "GET";
        var contentTypeStr = "application/json";
        //console.log(JSON.stringify(paramStr));
        $.when(postApi(url, method, {}, null, contentTypeStr)).done(function (data) {
            loading('hide');
            console.log(JSON.stringify(data));
            if (data.length !== 0) {
                var len = data.length;
                $('.clear-result').fadeIn();
                var userinfo,userId;
                for (var i = 0; i < len; i++) {
                    userinfo = data[i];
                    userId = userinfo.UserName+'@new';
                    // var template = '<li onclick="openFriendProfile(\'' + data[i].userId + '\',\'' + data[i].name + '\',\'' + data[i].portraitUri + '\',\'PRIVATE\')">\n' +
                    var template = '<li onclick="chatTo(\'' + userId + '\',\'' + data[i].StoreName + '\',\'' + data[i].AvatarUrl + '\',\'PRIVATE\',\'' + false + '\')">\n' +
                        '        <div class="ui-avatar-s">\n' +
                        '            <span style="background-image:url('+data[i].AvatarUrl+')"></span>\n' +
                        '        </div>\n' +
                        '        <div class="ui-list-info ui-border-t">\n' +
                        '            <h4 class="ui-nowrap small">'+ data[i].StoreName +'</h4>\n' +
                        '        </div>\n' +
                        '    </li>';
                    $('#searchFriendsList').prepend(template);
                }
            } else {
                $('.clear-result').fadeOut();
                messages($.i18n.prop('nameNotFound'), 'error', 'top', 2); //'Sorry, this name not found'
            }
        });
    }

}
//进入好友profile 页面
function openFriendProfile(userId, username,userPortrait,conversationType) {
    localStorage.setItem('openFrame', 'friendProfile');
    api.openFrame({
        name: 'friendProfile',
        url: '../users/friend-profile.html',
        bounces:false,
        animation:{
            type:"movein",
            subType:"from_right"
        },
        pageParam: {
            userId: userId,
            userName: username,
            userPortrait:userPortrait,
            conversationType:conversationType
        }
    });
}

//随机好友
function randomFriend() {
    $('#randomBox').fadeIn();
    setTimeout(function () {
        $('.sk-folding-cube').removeClass('d-none');
    }, 500);
    setTimeout(function () {
        randomUser();
    }, 2000);

}

function randomUser() {
    var name = localStorage.getItem('chatID');
    var url = "https://api.tweebucks.com/tweebaa/myRandomUser";
    var token = "IM";
    var paramStr = {
        "itemStr": name
    };
    var method = "POST";
    var contentTypeStr = "application/json";
    $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        //console.log("得到结果:" + JSON.stringify(data));
        var c = data.userId.split('@')[1];
        $('.random-tips').fadeIn();
        $('.sk-folding-cube').addClass('d-none');
        $('.show-random-name span').text(data.name).data('id', data.userId);
        setTimeout(function () {
            $('.show-random-name span').fadeIn();
        }, 500);
        setTimeout(function () {
            $('.random-buttons').fadeIn();
        }, 500);

        if ($('.added-friends-list').find('span').length !== 0) {
            $('.added-friends-list').fadeIn();
        }
    });

}

$('#randomAdd').on('click', function () {
    var name = $('.show-random-name span').text();
    var id = $('.show-random-name span').data('id');
    randomAddFriend(id, name);
});

function randomAgain() {
    $('.show-random-name span,.random-buttons,.random-tips,.added-friends-list').fadeOut();
    setTimeout(function () {
        $('.sk-folding-cube').removeClass('d-none');
    }, 500);
    setTimeout(function () {
        randomUser();
    }, 2000);

}

$('.close-btn').on('click', function () {
    $('.sk-folding-cube').addClass('d-none');
    $('#randomBox,.show-random-name span,.random-buttons,.random-tips,.added-friends-list').fadeOut();
    $('.name-box').html('');
});

//随机好友添加
function randomAddFriend(userId, username) {
    var addUserId = localStorage.getItem('chatID');
    var addUserName = localStorage.getItem('storename') || addUserId;
    if (!addUserName) {
        addUserName = addUserId;
    }
    var rong = api.require('rongCloud2');
    rong.sendCommandMessage({
        conversationType: 'PRIVATE',
        targetId: userId,
        name: 'AddFriend',
        data: '{"inviteUserId":"' + addUserId + '","username":"' + addUserName + '"}'
    }, function (ret, err) {
        if (ret.status == 'success') {
            // console.log("添加后:" +JSON.stringify(ret));
            var name = '<span>' + username + '</span>';
            $('#randomAdd').fadeOut();
            $('.added-friends-list').fadeIn();
            $('.name-box').append(name);
            recordApplication(userId, username);
        }
    });
}

//申请加好友
function addFriend(el, userId, username) {
    var addUserId = localStorage.getItem('chatID');
    var addUserName = localStorage.getItem('storename');
    var list = JSON.parse(localStorage.getItem('record'));
    if (list && list.length !== 0) {
        for (i = 0; i < list.length; i++) {
            if (userId === list[i].userId) {
                messages($.i18n.prop('alreadyAdd'), 'error', 'top', 2);
                return false
            }
        }
    }
    var rong = api.require('rongCloud2');
    rong.getBlacklistStatus({
        userId: userId
    }, function (ret, err) {
        if (ret.result == 0) {
            rong.removeFromBlacklist({
                userId: userId
            }, function (ret, err) {
            })
        }
    });
    rong.sendCommandMessage({
        conversationType: 'PRIVATE',
        targetId: userId,
        name: 'AddFriend',
        data: '{"inviteUserId":"' + addUserId + '","username":"' + addUserName + '"}'
    }, function (ret, err) {
        if (ret.status == 'prepare') {
            //console.log("send-p:" +JSON.stringify(ret));
        }
        else if (ret.status == 'success') {
            $api.text(el, 'Added');
            $api.addCls(el, 'disabled');
            $api.attr(el, 'disabled', 'disabled');
            recordApplication(userId, username);
            messages($.i18n.prop('alreadyAdd'), 'success', 'top', 2);
        }
        else if (ret.status == 'error') {
            messages($.i18n.prop('systemError'), 'error', 'top', 2);
        }
    });
}

//记录申请信息
function recordApplication(userId, username) {
    if (localStorage.getItem('record')) {
        var sList = (JSON.parse(localStorage.getItem("record")));
        var list = {"userId": userId, "username": username};
        sList.push(list);
        var newList = JSON.stringify(sList);
        localStorage.setItem('record', newList);
    } else {
        var list = [{"userId": userId, "username": username}];
        var newList = JSON.stringify(list);
        localStorage.setItem('record', newList);
        //console.log(localStorage.getItem('record'));
    }
}
