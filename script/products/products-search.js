
apiready = function () {
    //db 数据库操作
    var userName = localStorage.getItem('userName');
    var db = api.require('db');
    var ret = db.openDatabaseSync({
        name: userName
    });
    //读取记录
    function loadKeys() {
        var el = $('#searchKeys');
        el.empty();
        var showKey = db.selectSqlSync({
            name: userName,
            sql: 'SELECT * FROM searchKey'
        });
        if(showKey.status){
            var template;
            var len = showKey.data.length;
            if(len>0){
                for(i=0;i < len;i++){
                    template = '<li id="'+ showKey.data[i].id +'">'+showKey.data[i].key +'</li>';
                    el.prepend(template);
                }
            }
            el.find('li').on('click',function () {
                var key  = $(this).text();
                openProductsPageByCategory(null,null,null,key);
            })
        }else{
            var createTable = db.executeSqlSync({
                name: userName,
                sql: "CREATE TABLE searchKey(id INTEGER PRIMARY KEY AUTOINCREMENT,key text unique)"
            });
            console.log(JSON.stringify(createTable));
        }
    }
    loadKeys();
    //清空数据
    $('#cleanKey').on('click',function () {
        confirmBox($.i18n.prop('youWantDelSearchRecord'),$.i18n.prop('cancel'),$.i18n.prop('sure'),'error');
        $('.dialog-box').find('button').on('click',function (e) {
            $('.dialog-box').remove();
            var index = $(this).attr('id');
            if (index === 'btn-2') {
                var cleanKey = db.selectSqlSync({
                    name: userName,
                    sql: 'DELETE FROM searchKey'
                });
                if(cleanKey.status){
                    loadKeys();
                }
            } else {
                return false;
            }
        })

    });
    //删除单个记录
    $('#searchKeys').on('touchstart','li',function (event) {
        var timeout = undefined;
        var that = $(this);
        var $this = this;
        var id = that.attr('id'); //消息ID
        that.addClass('opacity-5');
        timeout = setTimeout(fn, 800);
        function fn() {
            confirmBox($.i18n.prop('youWantDelThisRecord'),$.i18n.prop('cancel'),$.i18n.prop('sure'),'error');
            $('.dialog-box').find('button').on('click',function (e) {
                $('.dialog-box').remove();
                var index = $(this).attr('id');
                if (index === 'btn-2') {
                    var cleanKey = db.selectSqlSync({
                        name: userName,
                        sql: 'DELETE FROM searchKey WHERE id ='+id
                    });
                    if(cleanKey.status){
                        loadKeys();
                    }
                } else {
                    return false;
                }
            })
        }
        $this.addEventListener('touchend', function (event) {
            clearTimeout(timeout);
            that.removeClass('opacity-5');
        }, false);
    });


    api.addEventListener({
        name:'swiperight'
    }, function(ret, err){
        back();
    });

    var el = $('form.search-box');
    var input = el.find('#searchProductInput');
    var button = el.find('.search-button');
    var clear = el.find('.search-clear');
    input.on('input',function () {
        //console.log($(this).val().trim());
        if($(this).val().trim().length > 0){
            button.hide();
            clear.show();
        }else{
            button.show();
            clear.hide();
        }
    }).focus();
    clear.on('touchstart',function () {
        input.focus().val('');
        button.show();
        clear.hide();
    });
    button.on('click',function () {
        el.submit();
    });
    el.on('submit',function (e) {
        e.preventDefault();
        var key  = input.val().trim();
        if(!key){
            return false;
        }else{
            var insertData = db.executeSqlSync({
                name: userName,
                sql: "insert into searchKey(key) values('"+ key +"')"
            });
            input.blur();
            openProductsPageByCategory(null,null,null,key);
        }
    });
};

function back() {
    localStorage.setItem('openFrame', '');
    api.closeFrame({
        name: 'searchTweebaaProduct'
    });
}