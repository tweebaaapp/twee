// 加入国家列表
//shipping method
//产品售价计算器
//function for getting product list from the server.
function loadCategoryList(type) {
    //console.log("loadProdList method in");
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/categories?lang=" + lan;
    var method = "GET";
    var CID;
    if (localStorage.getItem('server') === 'cn') {
        CID = 526;
    } else {
        CID = 166;
    }
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        var len = data.length;
        if (type === 'physical' || type === 'presale') {
            $('#selectCategory').html('');
            for (i = 0; i < len; i++) {
                if (data[i].Id !== CID) {
                    $('#selectCategory').append('<option value="' + data[i].Id + '">' + data[i].Name + '</option>');
                }
            }
        } else {
            $('#selectCategory').html('').append('<option value="' + CID + '">' + $.i18n.prop("eProduct") + '</option>');
        }
        loadSubCategoryList($("#selectCategory").val());
    });
}

$("#selectCategory").on('change', function () {
    loadSubCategoryList($(this).val());
});
$("#secondCategory").on('change', function () {
    loadSubSubCategoryList($(this).val());
});

//function for getting product list from the server.
function loadSubCategoryList(parentId) {

    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/categories/" + parentId + "/sub?lang=" + lan;
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        $('#secondCategory').html('');
        var len = data.length;
        for (i = 0; i < len; i++) {
            $('#secondCategory').append('<option value="' + data[i].Id + '">' + data[i].Name + '</option>');
        }
        loadSubSubCategoryList($("#secondCategory").val());
    });
}

function loadSubSubCategoryList(parentId) {

    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/categories/" + parentId + "/sub?lang=" + lan;
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        $('#thirdCategory').html('');
        var len = data.length;
        for (i = 0; i < len; i++) {
            $('#thirdCategory').append('<option value="' + data[i].Id + '">' + data[i].Name + '</option>');
        }

    });
}

//load tags
function loadTags() {
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var userid = localStorage.getItem('userId');
    var url = getBaseUrl() + "api/users/" + userid + "/products/tags";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
    });
}

//国家列表
function loadContry() {
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/countries?lang=" + lan;
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        $('#countryList').html('');
        var len = data.length;
        for (i = 0; i < len; i++) {
            $('#countryList').append('<option value="' + data[i].Id + '">' + data[i].Name + '</option>');
        }
    });
}

//shipping method
$('#addShippingButton').on('click', function () {
    var country = $('#countryList').find("option:selected").text();
    var countryId = $('#countryList').val();
    var method = $('#shippingMethod').val();
    var fee = $('#shippingFee').val();
    if (method === '') {
        return false;
    }
    var template = '<tr>\n' +
        '<td class="shipping-table-country" data-id=\'{\"Id\":' + countryId + '}\'>' + country + '</td>\n' +
        '<td class="shipping-table-method">' + method + '</td>\n' +
        '<td class="shipping-table-fee">' + fee + '</td>\n' +
        '<td class="del-shipping-btn" onclick="deleteShipping(this)"><i class="icon iconfont icon-empty_fill small"></i></td>\n' +
        '</tr>';
    $('#shippingTable').prepend(template);
    $('#shippingMethod').val('');
    $('#shippingFee').val('');
    $('.shipping-table.table').removeClass('d-none');
});

//删除shipping
function deleteShipping(This) {
    $(This).closest('tr').remove();
}

// $('#shippingTable').on('touchstart', 'tr', function () {
//   var timeout = undefined;
//   var that = $(this);
//   var $this = this;
//   var delBtn = $(this).find('del-shipping-btn'); //消息ID
//   that.addClass('opacity-5');
//   timeout = setTimeout(fn, 800);
//
//   function fn() {
//     var td = '<td class="del-shipping-btn"><i class="icon iconfont icon-empty_fill small"></i></td>';
//     that.append(td);
//     $(document).on("click", function (e) {
//        console.log(e);
//       if(e.target.tagName === "I" || e.target.className ==='del-shipping-btn'){
//         that.remove();
//       }else{
//         $('.del-shipping-btn').remove();
//       }
//     });
//
//   }
//
//   $this.addEventListener('touchend', function (event) {
//     clearTimeout(timeout);
//     that.removeClass('opacity-5');
//   }, false);
//     // $(this).closest('tr').remove();
// });

$("#enableTweebaa").on('change', function () {
    if ($(this).prop('checked')) {
        $('.enable-tweebaa-box').fadeIn();
    } else {
        $('.enable-tweebaa-box').fadeOut();
    }
});

//自己产品售价计算器
$('#price').on('change input', function () {
    var price = $(this).val();
    var cost = $('#costO').val();
    $('#sellingPriceO').val(price);
    var method = $("input[name='ownPriceRadio']:checked").val();
    var fee = priceCalculator(price, method, cost);
    showResult($('#profitO'), $('#processingFeeO'), fee);
});
$('#sellingPriceO,#costO').on('change input', function () {
    var cost = $('#costO').val();
    var price = $('#sellingPriceO').val();
    $('#price').val(price);
    var method = $("input[name='ownPriceRadio']:checked").val();
    var fee = priceCalculator(price, method, cost);
    showResult($('#profitO'), $('#processingFeeO'), fee);
});
$("input[name='ownPriceRadio']").on('change', function () {
    var price = $('#sellingPriceO').val();
    var cost = $('#costO').val();
    var method = $("input[name='ownPriceRadio']:checked").val();
    var fee = priceCalculator(price, method, cost);
    showResult($('#profitO'), $('#processingFeeO'), fee);
});


//向tweebaa推荐产品售价计算器
$('#WholesalePrice').on('change input', function () {
    var price = $(this).val();
    var cost = $('#costT').val();
    $('#sellingPriceT').val(price);
    var method = $("input[name='TPriceRadio']:checked").val();
    var fee = priceCalculator(price, method, cost);
    showResult($('#profitT'), $('#processingFeeT'), fee);
});
$('#sellingPriceT,#costT').on('change input', function () {
    var price = $('#sellingPriceT').val();
    var cost = $('#costT').val();
    $('#WholesalePrice').val(price);
    var method = $("input[name='TPriceRadio']:checked").val();
    var fee = priceCalculator(price, method, cost);
    showResult($('#profitT'), $('#processingFeeT'), fee);
});
$("input[name='TPriceRadio']").on('change', function () {
    var price = $('#sellingPriceT').val();
    var cost = $('#costT').val();
    var method = $("input[name='TPriceRadio']:checked").val();
    var fee = priceCalculator(price, method, cost);
    showResult($('#profitT'), $('#processingFeeT'), fee);
});

function priceCalculator(price, method, cost) {

    var processingFee;
    var profit;
    if (price) {
        var paypal = (0.3 + (0.044 * price)).toFixed(2);
        var credit = (0.3 + (0.0164 * price)).toFixed(2);
        if (method === 'paypal') {
            processingFee = paypal;
        } else {
            processingFee = credit;
        }
        profit = (price - processingFee - cost).toFixed(2);
        //console.log(profit);
        return {'processingFee': processingFee, 'profit': profit}
    }
}

function showResult(profit, processing, fee) {
    if (fee) {
        profit.val(fee.profit);
        processing.val(fee.processingFee);
    } else {
        profit.val(0);
        processing.val(0);
    }

}