//打开产品列表页面
var getCategoryId = function (category){
    var CategoryId;
    var s = getApi();
    switch (category) {
        case 'fz':
            CategoryId = s === 'cn'? 378 : 9;
            break;
        case 'dq':
            CategoryId = s === 'cn'? 319 : 121;
            break;
        case 'jk':
            CategoryId = s === 'cn'? 460 : 207;
            break;
        case 'jj':
            CategoryId = s === 'cn'? 435 : 220;
            break;
        case 'qc':
            CategoryId = s === 'cn'? 476 : 61;
            break;
        case 'yd':
            CategoryId = s === 'cn'? 545 : 283;
            break;
        case 'xn':
            CategoryId = s === 'cn'? 526: 166;
            break;
        case 'ys':
            CategoryId = s === 'cn'? 529 : 39;
            break;
        case 'et':
            CategoryId = s === 'cn'? 362 : 94;
            break;
        case 'qj':
            CategoryId = s === 'cn'? 507 : 110;
            break;
        case 'yy':
            CategoryId = s === 'cn'? 419 : 182;
            break;
        case 'hb':
            CategoryId = s === 'cn'? 471 : 200;
            break;
        case 'bg':
            CategoryId = s === 'cn'? 288 : 248;
            break;
        case 'cw':
            CategoryId = s === 'cn'? 297 : 259;
            break;
        case 'gj':
            CategoryId = s === 'cn'? 404 : 315;
            break;
        case 'wj':
            CategoryId = s === 'cn'? 516 : 333;
            break;
        case 'djk':
            CategoryId = s === 'cn'? 345 : 259;
            break;
        case 'cx':
            CategoryId = s === 'cn'? 346 : 315;
            break;
        case 'sh':
            CategoryId = s === 'cn'? 347 : 333;
            break;
    }
    return  CategoryId;
};
//打开产品分类列表页面
function openProductsPageByCategory(type,category,categoryName,q,subcategoryName,subcategoryID) {
    var CName = categoryName ?$.i18n.prop(categoryName ):'';
    var CategoryId = category ?getCategoryId(category):subcategoryID;

    api.closeFrame({
        name: 'tweebaaProductsList'
    });
    localStorage.setItem('openFrame', 'tweebaaProductsList');
    api.openFrame({
        name: 'tweebaaProductsList',
        url: '../products/tweebaa-products-list.html',
        bounces:true,
        allowEdit:true,
        pageParam: {
            categoryId: CategoryId || '',
            ProductsType: type || '',
            Q:q || '',
            categoryName: CName,
            subcategoryName:subcategoryName ||''
        }
    });
}
//打开所有分类页面
function openCategoryListPage() {
    localStorage.setItem('openFrame', 'categoryListPage');
    api.openFrame({
        name: 'categoryListPage',
        url: '../products/category-list.html',
        bounces:false,
        allowEdit:true,
        animation:{
            type:"movein",
            subType:"from_right",
            duration:300
        }
    });
}
//分类页面切换子分类显示
function showSubCategory(type,category,This) {
    loading('show');
    var el = $('#subCategoryList');
    var CategoryId = getCategoryId(category);
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/categories/" + CategoryId + "/sub?lang=" + lan;
    var method = "GET";
    $('.products-category-list').find('li').removeClass('active');
    $(This).addClass('active');
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        el.empty();
        var len = data.length;
        var template;
        for (i = 0; i < len; i++) {
            template = '<div class="subcategory-item" onclick="openProductsPageByCategory(\'' + type +'\',null,null,null,\''+ data[i].Name +'\','+ data[i].Id +')"> '+ data[i].Name +'</div>';
            el.append(template);
        }
    });
}
//打开产品列表
function getTweebaaProductList(type, currentpage, ifScroll,categoryId,q) {
    if (!ifScroll) {
        //loading('show');
        $('#tweebaaProductList').empty();
        insertPlaceHoder('tweebaaProduct');
        page = 0;
    }
    //console.log(type);
    $('#tweebaaProductType').val(type);
    // 切换按钮显示
    $('.product-tag').each(function () {
        $(this).removeClass('current-tag');
        if ($(this).hasClass(type)) {
            $(this).addClass('current-tag');
        }
    });
    var Q = q || '';
    var CategoryId = categoryId || '';
    var userid = localStorage.getItem('userId');
    var currentPage = currentpage || 0;
    var token = localStorage.getItem('access_token');
    var paramStr = '?currentpage='+currentPage+'&itemsperpage='+12+'&q='+Q + '&categoryId='+CategoryId + '&userId='+ userid;
    var url = type === 'evaluation' ? getBaseUrl() + "api/users/"+ userid +"/tweebaa/products/presale/" + type +'?currentpage='+currentPage+'&itemsperpage='+12 : getBaseUrl() + "api/tweebaa/products/" + type + paramStr;
    var method = "GET";
    // console.log(JSON.stringify(url));
    $.when(postApi(url, method, {}, token, 'json')).done(function (data) {
        if (!ifScroll) {
            $('#tweebaaProductList').empty();
        }
        var returnObj = data.Data || data;
        //console.log(JSON.stringify(returnObj));
        var len = returnObj.length;
        //console.log(len);
        if (len === 0 && !ifScroll) {
            $('#tweebaaProductList').append('<div class="no-item" style="display: block !important;">'+ $.i18n.prop("noTweebaaProducts") +'</div>');
        } else {
            for (i = 0; i < len; i++) {
                var added = returnObj[i].AddedToTycconPlace ?'added-tag':'';
                //console.log(added);
                var pic = returnObj[i].Product ? returnObj[i].Product.FeaturedPicture : returnObj[i].FeaturedPicture;
                if (pic === null) {
                    var picture = '../../image/no-image.png';
                } else {
                    var picture = pic.DownloadUrl;
                }
                if (type === 'freeoffer') {
                    //console.log(type);
                    if (returnObj[i].StartTime == null) {
                        var startTime = 'Unlimited';
                        var endtTime = 'Unlimited';
                    } else {
                        var startTime = moment.utc(returnObj[i].StartTime).local().format('YYYY-MM-DD HH:mm');
                        var endtTime = moment.utc(returnObj[i].EndTime).local().format('YYYY-MM-DD HH:mm');
                    }
                    var template = '<li class="media ui-col ui-col-50  '+ added +'" onclick="opentweebaaProductDetail(\'' + returnObj[i].Id + '\',\'' + type + '\',\'' + added + '\')">\n' +
                        '<div class="media-box bottom-shadow  text-center">\n' +
                        '<img class="img-fluid" src="' + picture + '" alt="image" >\n' +
                        '<div class="media-body">\n' +
                        '<p class="ui-nowrap">' + returnObj[i].Name + '</p>\n' +
                        '<p class="text-left small text-blue mt-3">'+ $.i18n.prop("Fr") +' &nbsp; <span class="small">' + startTime + ' </span> </p>\n' +
                        '<p class="text-right small text-red mt-3">'+ $.i18n.prop("To") +' &nbsp; <span class="small"> ' + endtTime + '</span></p>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</li>';
                } else if(type === 'evaluation'){
                    var evaluated = returnObj[i].MeEvaluated ? 'evaluated':'';
                    var template = '<li class="media ui-col ui-col-50 '+ evaluated+'" onclick="opentweebaaProductDetail(\'' + returnObj[i].Id + '\',\'' + type + '\')">\n' +
                        '<div class="media-box  bottom-shadow text-center">\n' +
                        '<img class="img-fluid" src="' + picture + '" alt="image">\n' +
                        '<div class="media-body">\n' +
                        '<p class="ui-nowrap">' + returnObj[i].Name + '</p>\n' +
                        '<div class="ui-progress" data-count="'+ returnObj[i].EvaluationCount +'" >\n' +
                        '    <span style="width:'+ (returnObj[i].EvaluationCount/300).toFixed(2)*100 +'%"></span>\n' +
                        '</div>\n' +
                        '<h5 class="text-left small text-red mt-1"><span class="price-TAGs x-small"></span> '+ (returnObj[i].Price).toFixed(2) +'</h5>\n' +
                        '<button class="ui-btn btn-block ui-btn-success mt-3">'+ $.i18n.prop('evaluate') +'</button>\n' +
                        '<button class="ui-btn btn-block ui-btn-primary mt-3">'+ $.i18n.prop('Evaluated') +'</button>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</li>';
                }else {
                    var template = '<li class="media ui-col ui-col-50  '+ added +'" onclick="opentweebaaProductDetail(\'' + returnObj[i].Id + '\',\'' + type + '\',\'' + added + '\')">\n' +
                        '<div class="media-box bottom-shadow  text-center">\n' +
                        '<img class="img-fluid" src="' + picture + '" alt="image">\n' +
                        '<div class="media-body">\n' +
                        '<p class="ui-nowrap">' + returnObj[i].Name + '</p>\n' +
                        '<h5 class="text-left text-red mt-1"> <span class="price-TAGs small"></span> '+ (returnObj[i].Price).toFixed(2) +'</h5>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</li>';
                }
                $('#tweebaaProductList').append(template);
            }
        }
        changePriceType();
        //设置图片为正方形
        var imgW = $('.media-box').width();
        $('img').css({'min-height': ''+imgW +'','height': ''+imgW +''});
        //结束下拉刷新状态
        api.refreshHeaderLoadDone();
        loading('hide');
    });
}

$('#addTweebaaPorToStore').on('click',function () {
    var markup = $('#markup').val();
    var id = api.pageParam.productId;
    var type = api.pageParam.Ptype;
    //if(markup){
    getTweebaaPorductToStore(id,type,0); //所有人使用统一价格
    // }else{
    //     messages($.i18n.prop('markupempty'), 'error', 'bottom', 3); //'Markup Price can not be empty!'
    //     return false;
    // }
});
//添加tweebaa产品到自己店
function getTweebaaPorductToStore(id, type,markup) {

    confirmBox($.i18n.prop('wantAddProduct'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'info');//'Do you want add this product?'
    $('.dialog-box').find('button').on('click',function (e) {
        var index = $(this).attr('id');
        $('.dialog-box').remove();
        if (index === 'btn-2') {
            loading('show');
            var userId = localStorage.getItem('userId');
            var token = localStorage.getItem('access_token');
            var paramStr = {"userId":userId, "productId":id, "MarkupPrice" : markup };
            var contentTypeStr = "application/json";
            var url = getBaseUrl() + "api/users/" + userId + "/products/" + id + "/tycoon";
            var method = "POST";
            //console.log(JSON.stringify(paramStr));
            $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
                // console.log(JSON.stringify(data));
                if (type === 'digital ') {
                    type = 'virtual';
                }
                loading('hide');
                var msg = $.i18n.prop('productAdded'); //"Your product has been added!"
                messages(msg, 'success', 'bottom', 2);
                if(markup){
                    api.sendEvent({
                        name: 'tweebaaProductListReload',
                        extra: {
                            type: type
                        }
                    });
                    setTimeout(function () {
                        localStorage.setItem('openFrame', '');
                        api.closeFrame({
                            name: 'tweebaaProductDetail',
                            animation:{
                                type:"suck",
                                duration:300
                            }
                        });
                    },2000);
                }

            });
        } else {
            return false;
        }
    });
}

//evaluateProduct
$('#evaluateProductBtn').on('click',function () {
    var eval1 = $('#evaluationSelection1').prop( "checked" )?1:2;
    var eval2 = $('#evaluationSelection2').prop( "checked" )?3:4;
    var eval3 = $('#evaluationSelection3').prop( "checked" )?5:6;
    var eval4 = $('#evaluationSelection4').prop( "checked" )?7:8;
    var ecomments = $('#evaluationComments').val();
    var ratings = [{'Rating':+eval1, "Text": null},{'Rating':+eval2, "Text": null},{'Rating':+eval3, "Text": null},{'Rating':+eval4, "Text": null}];
    //console.log(JSON.stringify(ratings));
    //console.log('ecomments:'+ecomments);
    evaluateProduct(ratings,ecomments);
});
function evaluateProduct(ratings,comments) {
    loading('show');
    var userId = localStorage.getItem('userId');
    var Id = api.pageParam.productId;
    var type = api.pageParam.Ptype;
    var token = localStorage.getItem('access_token');
    var paramStr = {
        "ReviewText":comments,
        "Ratings":ratings
    };
    var url = getBaseUrl() + "api/users/"+ userId +"/products/"+ Id +"/evaluation";
    var method = "PUT";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        //console.log(JSON.stringify(data));
        if(data.status ===200){
            messages($.i18n.prop('evaluated'), 'success', 'bottom', 2);
        }else{
            messages($.i18n.prop('alreadyEvaluated'), 'error', 'bottom', 2);
        }
        api.sendEvent({
            name: 'tweebaaProductListReload',
            extra: {
                type: type
            }
        });
        setTimeout(function () {
            localStorage.setItem('openFrame', '');
            api.closeFrame({
                name: 'tweebaaProductDetail',
                animation:{
                    type:"suck",
                    duration:300
                }
            });
        },2000);
    });
}
//打开产品详细页面
function opentweebaaProductDetail(id, type, added) {
    localStorage.setItem('openFrame', 'tweebaaProductDetail');
    api.openWin({
        name: 'tweebaaProductDetail',
        url: '../products/tweebaa-product-detail-new.html',
        bounces:false,
        allowEdit:true,
        pageParam: {
            productId: id,
            Ptype: type,
            added: added
        }
    });
}
//打开搜索页面
function openSearchPage() {
    localStorage.setItem('openFrame', 'searchTweebaaProduct');
    api.openFrame({
        name: 'searchTweebaaProduct',
        bounces:false,
        url: '../products/products-search.html'
    });

}
//读取详细信息
function tweebaaProductDetaile(Id,type) {
    loading('show');
    $('body.product-detail').addClass(type);
    var userId = localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/products/" + Id;
    var method = "GET";
    if(type ==='freeoffer'){
        url = getBaseUrl() + "api/users/"+ userId +"/freeoffers/" + Id;
    }
    //console.log(Id+type);
    // console.log(JSON.stringify(url));
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        //console.log(JSON.stringify(data));
        var pic = data.Product ? data.Product.FeaturedPicture : data.Pictures;
        //console.log(JSON.stringify(pic));
        if (pic && pic.length && pic.length !== 0) {
            $('#hiddenImg').data("picturesUrl", data.Pictures[0].DownloadUrl);
            var len = pic.length;
            for (i = 0; i < len; i++) {
                var picture = pic[i].DownloadUrl;
                var img = '<img class="img-fluid" id="9'+ i +'" src="'+ picture + '" alt="image">';
                $('.swiper-wrapper').prepend('<div class="swiper-slide">'+ img +'</div>');
            }
        }else if( !pic || pic.length=== 0 ){
            $('.swiper-wrapper').prepend('<div class="swiper-slide"><img style="height: 100%"  src="../../image/no-image.png" ></div>');
        }else{
            $('.swiper-wrapper').prepend('<div class="swiper-slide"><img style="height: 100%"  src="'+ pic.DownloadUrl +'" ></div>');
            $('#hiddenImg').data("picturesUrl", pic.DownloadUrl);
        }
        $('#shareURL').data("shareUrl", data.ShareUrl);
        //推送内容
        $('#name').text(data.Name);
        $('#shortDescription').text(data.ShortDescription);
        $('#fullDescription').html(data.FullDescription);
        $('#tycoonCost').text(data.Price);
        $("#startTime").text(moment.utc(data.StartTime).local().format('YYYY-MM-DDTHH:mm:ss'));
        $("#endTime").text(moment.utc(data.EndTime).local().format('YYYY-MM-DDTHH:mm:ss'));
        $('#costO').val(data.Price);
        $('#oldprice').text(data.OldPrice);
        $("#sku").text(data.Sku);
        $('#quantity').text(data.Quantity);
        $('#evaluationProgress').attr('data-count',data.EvaluationCount);
        $('#evaluationProgress span').css('width',''+(data.EvaluationCount/300).toFixed(2)*100+'%');
        if (data.Categories) {
            if (data.Categories.length !== 0) {
                $('.selectCategory').text(data.Categories[0].Name);
                if (data.Categories[1]) {
                    $('.secondCategory').text(data.Categories[1].Name);
                }
                if (data.Categories[2]) {
                    $('.thirdCategory').text(data.Categories[2].Name);
                }
            }
        }
        if (data.Download != null) {
            getVideo(data.Download.DownloadUrl);
        }
        if(data.MeEvaluated){
            $('#evaluateProductBtn,#productCommentsPlace').remove();
            $('#evaluationComments').val(data.MyEvaluation.ReviewText).attr('disabled','disabled');
            var RatingFlat = data.MyEvaluation.RatingFlat.split(',');
            var eval1 = RatingFlat[0] === '1';
            var eval2 = RatingFlat[1] === '3';
            var eval3 = RatingFlat[2] === '5';
            var eval4 = RatingFlat[3] === '7';
            $('#evaluationSelection1').prop( "checked", eval1).attr('disabled','disabled');
            $('#evaluationSelection2').prop( "checked", eval2).attr('disabled','disabled');
            $('#evaluationSelection3').prop( "checked", eval3).attr('disabled','disabled');
            $('#evaluationSelection4').prop( "checked", eval4).attr('disabled','disabled');
        }
        $("#videoData").data('video', data.Download);
        //初始化图片展示
        initialize();
    });
}

//如果有视频、图片或音频则在下面展示
function getVideo(DownloadUrl) {
    $('#productDisplay span').addClass('show');
    var box;
    var extName = (DownloadUrl.substring(DownloadUrl.lastIndexOf('.') +1)).toLowerCase();
    if(extName === 'mp4' || extName === 'avi' || extName === 'rmvb' || extName === 'mpg' || extName === 'mpeg' || extName === 'mpeg4' || extName === 'mov' || extName === 'wmv' || extName === 'f4v') {
        box ='<video src="'+ DownloadUrl +'" id="videoDisplay" controls="controls" preload="auto" -webkit-playsinline=true></video>';
        // $("#videoDisplay")[0].addEventListener("loadedmetadata", function () {
        //     $("#videoDisplay")[0].currentTime = 1;
        // });
        $('#productDisplay span.span-v').show();
    }else if(extName === 'jpg' || extName === 'jpeg' || extName === 'png' || extName === 'bmp' || extName === 'gif' || extName === 'raw'){
        box ='<img src="'+ DownloadUrl +'" class="img-fluid" style="max-height: 50rem">';
    }else if(extName === 'amr' || extName === 'wav' || extName === 'mp3' ){
        box ='<audio src="'+ DownloadUrl +'"  controls="controls" preload="auto" ></audio>';
    }
    $('#productDisplay').append(box);
}
//初始化
function initialize() {

    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        initialSlide :0 ,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction'
        }
    });
}
