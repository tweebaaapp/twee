apiready = function () {
    var productDetail = new Vue({
        el: '#tweebaaProducts',
        mounted: function () {
            this.openTips();
            this.getHealth();
            this.getLife();
            this.getInnovation();
        },
        data: {
            productsDataHealth: '',
            productLife:'',
            productInnovation:''
        },
        methods:{
            getHealth: function () {
                var that = this;
                var userId = localStorage.getItem('userId');
                var cId = getApi() === 'cn'?588 : 345;
                var paramStr = '?currentpage='+0+'&itemsperpage='+6+'&q='+'' + '&categoryId='+cId + '&userId='+ userId;  //暂时不传userId
                var url = getBaseUrl()+"/api/tweebaa/products/" + paramStr;
                //console.log(JSON.stringify(url));
                loading('show');
                getAxois(url,"GET",{}).then(function (response) {
                    loading('hide');
                    that.productsDataHealth = response.data.Data;
                    console.log(JSON.stringify(that.productsDataHealth));
                    setTimeout(function () {
                        var imgW = $('.products-box-health .name').width();
                        $('.products-box-health img').css({'min-height': ''+imgW +'','height': ''+imgW +''});
                        changePriceType();
                        //that.picInit();
                    });
                }).catch(function (error) {
                    //console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            getLife: function () {
                var that = this;
                var userId = localStorage.getItem('userId');
                var cId = getApi() === 'cn'?590 : 346;
                var paramStr = '?currentpage='+0+'&itemsperpage='+6+'&q='+'' + '&categoryId='+cId + '&userId='+ userId;  //暂时不传userId
                var url = getBaseUrl()+"/api/tweebaa/products/" + paramStr;
                loading('show');
                getAxois(url,"GET",{}).then(function (response) {
                    loading('hide');
                    that.productLife = response.data.Data;
                    //console.log(JSON.stringify(that.productLife));
                    setTimeout(function () {
                        var imgW = $('.products-box-life .swiper-slide').width();
                        $('.products-box-life img').css({'min-height': ''+imgW +'','height': ''+imgW +''});
                        changePriceType();
                        that.picInit();
                    });
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            getInnovation: function () {
                var that = this;
                var userId = localStorage.getItem('userId');
                var cId = getApi() === 'cn'?589 : 347;
                var paramStr = '?currentpage='+0+'&itemsperpage='+6+'&q='+'' + '&categoryId='+cId + '&userId='+ userId;  //暂时不传userId
                var url = getBaseUrl()+"/api/tweebaa/products/" + paramStr;
                loading('show');
                getAxois(url,"GET",{}).then(function (response) {
                    loading('hide');
                    that.productInnovation = response.data.Data;
                    //console.log(JSON.stringify(that.productInnovation));
                    setTimeout(function () {
                        var imgW = $('.products-box-innovation .swiper-slide').width();
                        $('.products-box-innovation img').css({'min-height': ''+imgW +'','height': ''+imgW +''});
                        changePriceType();
                    });
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            picInit:function () {
                var mySwiper = new Swiper ('.swiper-container.tweebaaProduct', {
                    direction: 'horizontal',
                    loop: false,
                    freeMode : true,
                    initialSlide :0 ,
                    slidesPerView : 2.2,
                    spaceBetween : 5,
                });
                setTimeout(function () {
                    var imgW = $('.products-box-life .name').width();
                    $('.products-box-life img').css({'min-height': ''+imgW +'','height': ''+imgW +''});
                });

            },
            openTips:function () {
                var tips = localStorage.getItem('addToStoreTips');
                if(!tips){
                    openNewUserTips('addToStoreTips');
                    localStorage.setItem('addToStoreTips','yes');
                }
            },
            openDetail:function (proId) {
                openWindows('tweebaaProductDetail','tweebaa-product-detail-new.html',{productId:proId});
            }
        }
    });
};

//打开产品列表页面
var getCategoryId = function (category){
    var CategoryId;
    var s = getApi();
    switch (category) {
        case 'fz':
            CategoryId = s === 'cn'? 378 : 9;
            break;
        case 'dq':
            CategoryId = s === 'cn'? 319 : 121;
            break;
        case 'jk':
            CategoryId = s === 'cn'? 460 : 207;
            break;
        case 'jj':
            CategoryId = s === 'cn'? 435 : 220;
            break;
        case 'qc':
            CategoryId = s === 'cn'? 476 : 61;
            break;
        case 'yd':
            CategoryId = s === 'cn'? 545 : 283;
            break;
        case 'xn':
            CategoryId = s === 'cn'? 526: 166;
            break;
        case 'ys':
            CategoryId = s === 'cn'? 529 : 39;
            break;
        case 'et':
            CategoryId = s === 'cn'? 362 : 94;
            break;
        case 'qj':
            CategoryId = s === 'cn'? 507 : 110;
            break;
        case 'yy':
            CategoryId = s === 'cn'? 419 : 182;
            break;
        case 'hb':
            CategoryId = s === 'cn'? 471 : 200;
            break;
        case 'bg':
            CategoryId = s === 'cn'? 288 : 248;
            break;
        case 'cw':
            CategoryId = s === 'cn'? 297 : 259;
            break;
        case 'gj':
            CategoryId = s === 'cn'? 404 : 315;
            break;
        case 'wj':
            CategoryId = s === 'cn'? 516 : 333;
            break;
        case 'djk':
            CategoryId = s === 'cn'? 588: 345;
            break;
        case 'cx':
            CategoryId = s === 'cn'? 590 : 346;
            break;
        case 'sh':
            CategoryId = s === 'cn'? 589: 347;
            break;
    }
    return  CategoryId;
};
//打开产品分类列表页面
function openProductsPageByCategory(type,category,categoryName,q,subcategoryName,subcategoryID) {
    var CName = categoryName ?$.i18n.prop(categoryName ):'';
    var CategoryId = category ?getCategoryId(category):subcategoryID;
    console.log(type+category+categoryName+q+subcategoryName+subcategoryID);
    localStorage.setItem('openFrame', 'tweebaaProductsList');
    api.openFrame({
        name: 'tweebaaProductsList',
        url: '../products/tweebaa-products-list.html',
        bounces:true,
        allowEdit:true,
        pageParam: {
            categoryId: CategoryId || '',
            ProductsType: type || '',
            Q:q || '',
            categoryName: CName,
            subcategoryName:subcategoryName ||''
        }
    });
}
//打开所有分类页面
function openCategoryListPage() {
    localStorage.setItem('openFrame', 'categoryListPage');
    api.openFrame({
        name: 'categoryListPage',
        url: '../products/category-list.html',
        bounces:false,
        allowEdit:true,
        animation:{
            type:"movein",
            subType:"from_right",
            duration:300
        }
    });
}
function back() {
    //localStorage.setItem('openFrame', '');
    api.closeWin({
        name:'tProduct'
    });
}
changePriceType();