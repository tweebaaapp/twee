
apiready = function () {
    var prodId = api.pageParam.productId;
    var productDetail = new Vue({
        el: '#productDetail',
        mounted: function () {
            this.getProductDetail(prodId);
        },
        data: {
            productData: '',
            switchImg: true,
        },
        methods:{
            getProductDetail: function (prodId) {
                var that = this;
                var userId = localStorage.getItem('userId');
                var url = getBaseUrl() + "api/users/"+ userId +"/products/" + prodId;
                getAxois(url,"GET",{}).then(function (response) {
                    console.log(JSON.stringify(response));
                    that.productData = response.data;
                    setTimeout(function () {
                        initialize();
                        changePriceType();
                    });
                    //var ShareUrl = 'www.tweebaa.'+ getApi()+'/tycoonplace/'+ that.userInfo.userId +'/products/'+ that.productData.Id;
                    var ShareUrl = 'https://tweebaa.cn/html/' + lan + '/mproductdetails.html?userId=' + that.userInfo.Id + '&id=' + that.productData.Id + '&server=cn';
                    $('#shareURL').data("shareUrl", ShareUrl);

                }).catch(function (error) {
                    //console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            switchImgVideo:function (type) {
                this.switchImg = type === 'img';
                initialize();
            },
            addToMyProducts:function () {
                var that = this;
                confirmBox($.i18n.prop('wantAddProduct'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'info');
                $('.dialog-box').find('button').on('click',function (e) {
                    var index = $(this).attr('id');
                    $('.dialog-box').remove();
                    if (index === 'btn-2') {
                        loading('show');
                        var userId = localStorage.getItem('userId');
                        var paramStr = {"userId":userId, "productId":prodId, "MarkupPrice" : 0 };
                        var url = getBaseUrl() + "api/users/" + userId + "/products/" + prodId + "/tycoon";
                        //console.log(JSON.stringify(paramStr));
                        getAxois(url,"POST",paramStr).then(function (response) {
                            //console.log(JSON.stringify(response));
                            loading('hide');
                            that.productData.AddedToTycoonPlace = true;
                        }).catch(function (error) {
                            console.log(JSON.stringify(error));
                            messages($.i18n.prop('systemError'), 'error', 'top', 3);
                        });
                    } else {
                        return false;
                    }
                });
            }
        }
    });
    //初始化图片显示
    function initialize() {
        var mySwiper = new Swiper ('#proDetailPhoto', {
            direction: 'horizontal',
            loop: false,
            initialSlide :1 ,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets'
            }
        });

        var winWidth = api.winWidth;
        $('#proDetailPhoto').height(winWidth);
        $('.videoDisplay').height(winWidth - 36);
    }
};


function back() {
    //localStorage.setItem('openFrame', 'tycoonplace');
    api.closeWin({
        name: 'tweebaaProductDetail',
        animation: {
            type: "reveal",
            subType: "from_left",
            duration: 300
        }
    });
}
