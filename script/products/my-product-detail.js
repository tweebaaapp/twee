
apiready = function () {
    var prodId = api.pageParam.prodId;
    api.addEventListener({
        name: 'productListReload'
    }, function (ret, err) {
        location.reload();
    });
    var productDetail = new Vue({
        el: '#myProductDetail',
        mounted: function () {
            this.getProductDetail(prodId);
        },
        data: {
            productData: '',
            switchImg:true,
            tweebaaTag:api.pageParam.tweebaaTag,
            myID: localStorage.getItem('userId')
        },
        methods:{
            getProductDetail: function (prodId) {
                var that = this;
                var url = getBaseUrl() + "api/users/"+ this.myID +"/products/" + prodId;
                getAxois(url,"GET",{}).then(function (response) {
                    //console.log(JSON.stringify(response));
                    that.productData = response.data;
                    var ShareUrl = 'https://tweebaa.'+ getApi()+'/html/'+ lan +'/mproductdetails.html?userId='+ that.myID +'&id='+ prodId+'&server='+getApi();
                    $('#shareURL').attr("data-shareUrl", ShareUrl);
                    setTimeout(function () {
                        initialize();
                        changePriceType();
                    });
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            switchImgVideo:function (type) {
                this.switchImg = type === 'img';
                initialize();
            },
            deleteProduct:function () {
                confirmBox($.i18n.prop('wantDeleteProduct'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error');
                $('.dialog-box').find('button').on('click',function (e) {
                    $('.dialog-box').remove();
                    var index = $(this).attr('id');
                    if (index === 'btn-2') {
                        var userid = localStorage.getItem('userId');
                        //var productId= api.pageParam.productId; 2534
                        var url = getBaseUrl() + "api/users/" + userid + "/products/" + prodId;
                        var paramStr = {"userId": userid, "productId": prodId};
                        getAxois(url,"DELETE",paramStr).then(function (response) {
                            //console.log(JSON.stringify(response));
                            api.sendEvent({
                                name: 'productListReload',
                            });
                            setTimeout(function () {
                                back();
                            })
                        }).catch(function (error) {
                            console.log(JSON.stringify(error));
                            messages($.i18n.prop('systemError'), 'error', 'top', 3);
                        });
                    }
                });

            },
            editProduct:function () {
                api.openWin({
                    name: 'editProduct',
                    url: '../products/edit-product.html',
                    bounces:false,
                    allowEdit:true,
                    pageParam: {
                        productData: JSON.stringify(this.productData),
                    }
                });
            }
        }
    });
};
//初始化图片显示
function initialize() {
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        initialSlide :0 ,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets'
        }
    });
    var winWidth = api.winWidth;
    $('#proDetailPhoto').height(winWidth);
    $('.videoDisplay').height(winWidth - 36);
}

function back() {

    //localStorage.setItem('openFrame', 'tycoonplace');
    api.closeWin({
        name: 'myProductDetail',
        animation: {
            type: "reveal",
            subType: "from_left",
            duration: 300
        }
    });
}
