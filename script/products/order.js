function getOrderList(type, currentpage, ifScroll) {
    if (!ifScroll) {
        $('#orderList').empty();
        page = 0;
    }
    $('.no-item').hide();
    $('.product-tag').each(function () {
        $(this).removeClass('current-tag');
        if ($(this).hasClass(type)) {
            $(this).addClass('current-tag');
        }
    });
    $('#orderType').val(type);
    loading('show');
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var currentPage = currentpage || 0;
    var paramStr = {};
    if (type === 'new') {
        var url = getBaseUrl() + "api/orders?ownerId="+userId+"&paymentStatus=pending&currentpage="+currentPage+"&itemsperpage=24";
    } else if (type === 'fulfilled') {
        var url = getBaseUrl() + "api/orders?ownerId="+userId+"&orderStatus=completed&currentpage="+currentPage+"&itemsperpage=24";
    }
    var method = "GET";
    loading('show');
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        console.log(JSON.stringify(data));
        loading('hide');
        if (!ifScroll) {
            $('#orderList').empty();
        }
        //var totalOrder = data.Totolitems;
        //console.log(" Total customers: " + totalCustomers);
        var returnArray = data;
        //console.log(JSON.stringify(returnArray));
        var len = returnArray.length;
        if (len === 0 && !ifScroll) {
            $('.no-item').show();
        } else {
            $('.no-item').hide();
            for (i = 0; i < len; i++) {
                var Id = returnArray[i].Id;
                //var orderUser = returnArray[i].Username;
                var orderStatusName = returnArray[i].OrderStatus;
                var orderTotal = (returnArray[i].Total).toFixed(2);
                var UTC = returnArray[i].CreatedOnUtc;
                var Date = moment.utc(UTC).local().format('YYYY-MM-DD HH:mm:ss');
                var oName;
                switch (orderStatusName) {
                    case "Pending":
                        oName = $.i18n.prop('Pending');
                        break;
                    case "Processing":
                        oName = $.i18n.prop('Processing');
                        break;
                    case "Complete":
                        oName = $.i18n.prop('Complete');
                        break;
                    case "Canelled":
                        oName = $.i18n.prop('Canelled');
                        break;
                    case "NewOrder":
                        oName = $.i18n.prop('NewOrder');
                        break;

                }
                var template = '<li  class="pb-4 " onclick="OpenOrderDetail(' + Id + ')">\n' +
                    '      <div class="media-box bottom-shadow">\n' +
                    '        <div class="ui-row p-3">\n' +
                    '          <div class="ui-col ui-col-75">\n' +
                    '            <div><span class="x-small">' + $.i18n.prop('orderNumber') + '</span>: <span class="text-b small">' + Id + '</span></div>\n' +
                    // '            <div><span class="text-blue x-small">' + $.i18n.prop('username') + ': </span> <span class="text-b small">' + orderUser + '</span> </div>\n' +
                    '          </div>\n' +
                    '          <div class="ui-col ui-col-25 order-status">\n' +
                    '            <div class="order-status-text ' + orderStatusName + '">' + oName + '</div>\n' +
                    '          </div>\n' +
                    '        </div>\n' +
                    '        <ul class="ui-list-text border-list">\n' +
                    '          <li class="ui-border-b p-0"></li>\n' +
                    '        </ul>\n' +
                    '        <div class="ui-row p-3">\n' +
                    '          <div class="ui-col ui-col-50">\n' +
                    '            <div><span class="x-small">' + $.i18n.prop('total') + ' </span>: <span class="price-TAGs small"></span> <span class="text-b small text-red"> ' + orderTotal + '</span></div>\n' +
                    '          </div>\n' +
                    '          <div class="ui-col ui-col-50 text-right">\n' +
                    '            <div><span class="x-small">' + $.i18n.prop('date') + ' </span>: <span class="text-light-gray small">' + Date + '</span></div>\n' +
                    '          </div>\n' +
                    '        </div>\n' +
                    '      </div>\n' +
                    '    </li>';
                $('#orderList').prepend(template);
            }
        }

        changePriceType();
        //结束下拉刷新状态
        api.refreshHeaderLoadDone();
    });
}

function OpenOrderDetail(id, page) {
    localStorage.setItem('openFrame', 'order');
    api.openFrame({
        name: 'order',
        url: '../products/order-detail.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            orderId: id,
            page: page
        }
    });
}

function getOrderDetail(id) {
    var token = localStorage.getItem('access_token');
    // var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "/api/orders/"+id;
    var method = "GET";
     console.log(JSON.stringify(url));
    $.when(postApi(url, method, {}, token, null)).done(function (data) {
        var D = data;
        console.log(JSON.stringify(data));
        var len = D.OrderItems.length;
        for (i = 0; i < len; i++) {
            var itemName = D.OrderItems[i].Product.Name;
            var itemQuantity = D.OrderItems[i].Quantity;
            // var itemCost = D.OrderItems[i].OriginalProductCost.toFixed(2);
            var template = '<tr>\n' +
                '          <td class="ui-nowrap">' + itemName + '</td>\n' +
                '          <td>' + itemQuantity + '</td>\n' +
                '        </tr>';
            $('#orderItemsList').prepend(template);
        }
        var UTC = D.CreatedOnUtc;
        var Date = moment.utc(UTC).local().format('YYYY-MM-DD HH:mm:ss');
        var customerName = D.ShippingAddress.FirstName;
        var customerEmail = D.ShippingAddress.Email;
        var orderID = D.Id;
        var billAddress = D.BillingAddress.Address1;
        var shippingAddress = D.ShippingAddress.Address1;
        var OrderStatus = D.OrderStatus;
        var PaymentStatus = D.PaymentStatus;
        // var orderDiscount = D.OrderDiscount;
        var orderTotal = D.Total.toFixed(2);
        var PName;
        switch (PaymentStatus) {
            case "Paid":
                PName = $.i18n.prop('Paid');
                break;
            case "Pending":
                PName = $.i18n.prop('unpaid');
                break;
            case "Authorized":
                PName = $.i18n.prop('Authorized');
                break;
            case "PartiallyRefunded":
                PName = $.i18n.prop('PartiallyRefunded');
                break;
            case "Refunded":
                PName = $.i18n.prop('Refunded');
                break;
            case "Voided":
                PName = $.i18n.prop('Voided');
                break;
        }
        $('#orderTime').text(Date);
        $('#orderId').text(orderID);
        // $('#orderStatus').text(OrderStatus);
        $('#customerName').text(customerName);
        $('#customerEmail').text(customerEmail);
        $('#billAddress').text(billAddress);
        $('#shippingAddress').text(shippingAddress);
        $('#PaymentStatus').text(PName);
        // $('#orderDiscount').text(orderDiscount);
        $('#orderTotal').text(orderTotal);
        $('#orderCount').text(len);
        changePriceType();
        var bigF = customerName.split('')[0].toUpperCase();
        $('.round-name').text(bigF);
        if (OrderStatus === 'Canelled') {
            $('.status-bar-box').remove();
            $('.order-canceled').show();
        } else {
            $('.status-bar').addClass(OrderStatus);
        }

    });
}
