

apiready = function () {
    var data = JSON.parse(api.pageParam.productData);
    var liWith = $('#width').width();
    //console.log(JSON.stringify(data));
    loadShipping(data.ShippingMethods);
    if(data.ProductType === 2){
        $('#textUpProduct').removeClass('d-none');
        $('#getVideo').attr('accept',"*");
    }else{
        $('#textUpVideo,.shippingBox,.shipping-table,.product-quantity').removeClass('d-none');
        $('#getVideo').attr('accept',"video/*");
    }
    $('#name').val(data.Name);
    $('#shortDescription').val(data.ShortDescription);
    $('#fullDescription').val(data.FullDescription);
    $('#productPrice').text(data.Price);
    $('#productOldPrice').text(data.OldPrice);
    $('#price').val(data.Price);
    $('#oldprice').val(data.OldPrice);
    $('#quantity').val(data.Quantity);
    $("#showHomepage").prop('checked', data.ShowOnHomePage);

    var pic = data.Pictures;
    if (pic.length !== 0) {
        for (i = 0; i < pic.length; i++) {
            var picture = data.Pictures[i].DownloadUrl;
            var img = '<img class="img-fluid" id ="9'+ i +'" src="'+ picture + '" alt="image">';
            $('.swiper-wrapper').append('<div class="swiper-slide">'+ img +'</div>');
            $('#uploadImgBox').prepend('<li class="ui-col ui-col-20 img-li" style="height: '+ liWith +'px"><i class="icon iconfont icon-delete1 text-red" ></i><div class="media-box">'+ img +'</div></li>');
        }
    }
    if (data.Download) {
        showVideo(data.Download.DownloadUrl);
    }
    changePriceType();
};

// 编辑普通产品
function editGeneralProduct() {
    loading('show');
    var type = JSON.parse(api.pageParam.productData).ProductType ===2?'virtual':'physical';
    var userid = localStorage.getItem('userId');
    var name = $("#name").val();
    if (!name) {
        messages($.i18n.prop('productNameEmpty'), 'error', 'top', 3); //'product name can not be empty!'
        return false;
    }
    // //get pic
    var picArray = [];
    var pic;
    var picJson;
    //console.log($("#uploadImgBox").find('img').length);
    $("#uploadImgBox").find('img').each(function () {
        console.log($(this).attr('src'));
        if($(this).attr('src').length>300){
            pic = $(this).attr('src').split(",")[1];
            picJson = {'PictureBinaryBase64': pic, 'SeoFileName': 'picture.jpg'};
        }else{
            pic = $(this).attr('src');
            picJson = {'PictureUrl': pic, 'SeoFileName': 'picture.jpg'};
        }
        picArray.unshift(picJson);
    });

    //shipping信息
    var shippingMethods = [];
    if (type === 'physical') {
        var quantity = $("#quantity").val();
        $('#shippingTable').find('tr').each(function () {
            var countryId = $(this).find('.shipping-table-country').data('id').Id;
            var countryText = $(this).find('.shipping-table-country').text();
            var shippingMethod = $(this).find('.shipping-table-method').text();
            var fee = $(this).find('.shipping-table-fee').text();
            var shipping = {'Country': {"Id": countryId, 'Name': countryText}, 'ShippingMethod': shippingMethod, 'ShippingFee': fee};
            shippingMethods.unshift(shipping);
        });
    }

    //git others
    var productId = JSON.parse(api.pageParam.productData).Id;
    var price = $("#price").val();
    var oldprice = $("#oldprice").val();
    var displayorder = 0;
    var categories = [{"Id": $('#selectCategory').val()}, {"Id": $('#secondCategory').val()}, {"Id": $('#thirdCategory').val()}];
    var shortdescription = $("#shortDescription").val();
    var fulldescription = $("#fullDescription").val();
    var showH = $("#showHomepage").prop('checked');
    var videoInput = $('#getVideo').val();
    var token = localStorage.getItem('access_token');
    var url = getBaseUrl() + "api/users/" + userid + "/products/" + type + '/' + productId;
    var method = "PUT";
    var video = $('#delDownload').css("display") !== 'none'?JSON.parse(api.pageParam.productData).Download:null;
    var contentTypeStr = "application/json";
    var prodObj = {
        "Id": productId, "Name": name, "Quantity": quantity, "Price": price, "OldPrice": oldprice,
        "DisplayOrder": displayorder, "ShortDescription": shortdescription, "FullDescription": fulldescription,
        "Customer": {"Id": userid}, 'ShippingMethods':shippingMethods,"Categories": categories, "ShowOnHomePage": showH, "Pictures": picArray, "Download": video,
    };
    console.log("要修改的数据"+JSON.stringify(prodObj));
    console.log(JSON.stringify(url));
    $.when(postApi(url, method, prodObj, token, contentTypeStr)).done(function (data) {
        console.log("修改后返回的数据"+JSON.stringify(data));
        if(data.status ===200){
            if (videoInput) {
                uploadVideo(productId, userid,'edit');
            }
            else {
                loading('hide');
                messages($.i18n.prop('productUpdated'), 'success', 'top', 3); //'Product updated!'
                // scrollTo(0,0);
                api.sendEvent({
                    name: 'productListReload',
                });
                localStorage.setItem('openFrame', '');
                setTimeout(function () {
                    confirmBack();
                },2500)

            }
        }else{
            loading('hide');
            messages($.i18n.prop('systemError'), 'error', 'top', 2);
        }


    });

}

//showVideo
function showVideo(DownloadUrl) {
    var extName = (DownloadUrl.substring(DownloadUrl.lastIndexOf('.') +1)).toLowerCase();
    // console.log(extName);
    //console.log(fileName);
    if(extName === 'mp4' || extName === 'avi' || extName === 'rmvb' || extName === 'mpg' || extName === 'mpeg' || extName === 'mpeg4' || extName === 'mov' || extName === 'wmv' || extName === 'f4v'){
        $('#uploadVideoBox').hide();
        $('.delete-video-btn').show();
        $('#cutVideoCheckBox').removeClass('d-none');
        $("#ShowVideo").show().attr("src", DownloadUrl) ;
    }else if(extName === 'jpg' || extName === 'jpeg' || extName === 'png' || extName === 'bmp' || extName === 'gif' || extName === 'raw'){
        $('#uploadVideoBox').hide();
        $('.delete-video-btn').show();
        $('#cutVideoCheckBox').addClass('d-none');
        $('#imgProduct').attr('src',DownloadUrl).show();
    }else if(extName === 'amr' || extName === 'wav' || extName === 'mp3' ){
        // alert(objUrl);
        $('#showAudio').attr('src',DownloadUrl).css('display','block');
        $('#uploadVideoBox').hide();
        $('.delete-video-btn').show();
        $('#cutVideoCheckBox').addClass('d-none');
    }else {
        $('#uploadVideoBox').hide();
        $('.delete-video-btn').show();
        $('#otherProduct').attr('href',DownloadUrl).show();
    }
}

$('#delDownload').on('click',function () {
    $(".show-video-box,#imgProduct,#showAudio,#otherProduct").attr("src", '').hide();
    $('#uploadVideoBox').show();
    $('.delete-video-btn').hide();
    $('.slider-input').val('');
    var v = JSON.parse(api.pageParam.productData).Download;
    if(v){
        delVideo(v.Id);
    }
});
//删除视频
function delVideo(downloadId) {
    var token = localStorage.getItem('access_token');
    var url = getBaseUrl() + "api/files/" + downloadId;
    //console.log(url);
    $.ajax({
        contentType: "application/json",
        method: "DELETE",
        url: url,
        async: true,
        dataType: 'json',
        headers: {"Authorization": "Bearer " + token}
    }).done(function (data) {
        // var msg = JSON.stringify(data);
        //defer.resolve(data);
        console.log("data: " + JSON.stringify(data));
        // console.log("data=" + data);

    }).fail(function (err) {
        //defer.resolve(err);
        console.log("err: " + JSON.stringify(err));
    });
}

function loadShipping(SMD) {
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/countries?lang=" + lan;
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        $('#countryList').html('');
        var countries ={};
        var len = data.length;
        for (i = 0; i < len; i++) {
            $('#countryList').append('<option value="' + data[i].Id + '">' + data[i].Name + '</option>');
            countries[data[i].Id ]=data[i].Name;
        }
        //console.log(JSON.stringify(countries))
        if (SMD) {
            if (SMD.length !== 0) {
                for (j = 0; j < SMD.length; j++) {
                    console.log(JSON.stringify(countries));
                    var template = '<tr>\n' +
                        '<td class="shipping-table-country" data-id=\'{\"Id\":' + SMD[j].Country.Id + '}\'>' + countries[SMD[j].Country.Id] + '</td>\n' +
                        '<td class="shipping-table-method">' + SMD[j].ShippingMethod + '</td>\n' +
                        '<td class="shipping-table-fee">' + SMD[j].ShippingFee + '</td>\n' +
                        '<td class="del-shipping-btn" onclick="deleteShipping(this)"><i class="icon iconfont icon-empty_fill small"></i></td>\n' +
                        '</tr>';
                    $('#shippingTable').append(template);
                }
            }
        }
    });
}
$('#addShippingButton').on('click', function () {
    var country = $('#countryList').find("option:selected").text();
    var countryId = $('#countryList').val();
    var method = $('#shippingMethod').val();
    var fee = $('#shippingFee').val();
    if (method === '') {
        return false;
    }
    var template = '<tr>\n' +
        '<td class="shipping-table-country" data-id=\'{\"Id\":' + countryId + '}\'>' + country + '</td>\n' +
        '<td class="shipping-table-method">' + method + '</td>\n' +
        '<td class="shipping-table-fee">' + fee + '</td>\n' +
        '<td class="del-shipping-btn" onclick="deleteShipping(this)"><i class="icon iconfont icon-empty_fill small"></i></td>\n' +
        '</tr>';
    $('#shippingTable').prepend(template);
    $('#shippingMethod').val('');
    $('#shippingFee').val('');
    $('.shipping-table.table').removeClass('d-none');
});

//删除shipping
function deleteShipping(This) {
    $(This).closest('tr').remove();
}

function back(){
    confirmBox($.i18n.prop('exitNow'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error');
    $('.dialog-box').find('button').on('click',function (e) {
        var index = $(this).attr('id');
        $('.dialog-box').remove();
        if (index === 'btn-2') {
            confirmBack();
        } else {
            return false;
        }
    })
}

function confirmBack() {
    api.sendEvent({
        name: 'productListReload',
    });
    localStorage.setItem('openFrame', '');
    api.closeWin({
        name: 'editProduct',
        animation: {
            type: "reveal",
            subType: "from_left",
            duration: 300
        }
    });
}
