
//获取自己order总数
function getOrdersCount(paymentStatus,shippingStatus) {
    var customerId =  localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/orders/count?customerId="+customerId+"&paymentStatus="+paymentStatus+"&shippingStatus="+shippingStatus;
    var method = "GET";
    //console.log(JSON.stringify(url));
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        if(paymentStatus ==='pending'){
            $('#unpaid').text(data);
        }else if(paymentStatus === 'paid'&& shippingStatus ==='NotYetShipped'){
            $('#notShipped').text(data);
        }else if (paymentStatus === 'paid'&& shippingStatus ==='shipped'){
            $('#shipped').text(data);
        }
    });
}
function openMyOrderPage(paymentStatus,shippingStatus) {
    localStorage.setItem('openFrame', 'myOrderPage');
    api.openFrame({
        name: 'myOrderPage',
        url: '../products/my-order.html',
        bounces:true,
        animation:{
            type:"movein",
            subType:"from_right"
        },
        pageParam:{
            "paymentStatus":paymentStatus,
            "shippingStatus": shippingStatus
        }
    });
}

function getMyOrderList(paymentStatus,shippingStatus,currentpage,ifScroll) {
    loading('show');
    if (!ifScroll) {
        $('#myOrderList').empty();
        page = 0;
    }
    var customerId =  localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var currentPage = currentpage || 0;
    var url = getBaseUrl() + "api/orders?customerId="+customerId+"&paymentStatus="+paymentStatus+"&shippingStatus="+shippingStatus+"&currentpage="+currentPage+"&itemsperpage=10";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        //var totalOrder = data.Totolitems;
        //console.log(" Total customers: " + totalCustomers);
        var returnArray = data;
        //console.log(JSON.stringify(returnArray));
        var len = returnArray.length;
        if(!ifScroll && len === 0){
            $('#myOrderList').append('<p class="text-center text-blue mt-5 pt-5">'+ $.i18n.prop('noOrder')+'</p>');
        }
        if (len !== 0) {
            for (i = 0; i < len; i++) {
                var Id = returnArray[i].Id;
                // var orderUser = returnArray[i].Username;
                var orderStatusName = returnArray[i].OrderStatus;
                var orderTotal = (returnArray[i].Total).toFixed(2);
                var UTC = returnArray[i].CreatedOnUtc;
                var Date = moment.utc(UTC).local().format('YYYY-MM-DD HH:mm:ss');
                var oName;
                if(paymentStatus ==='pending'){
                    oName = $.i18n.prop('unpaid');
                }else if(paymentStatus ==='paid' && shippingStatus){
                    oName = shippingStatus === 'NotYetShipped' ? $.i18n.prop('notShipped'):$.i18n.prop('shipped');
                }
                var template = '<li  class="pb-4 " onclick="OpenMyOrderDetail(' + Id + ')">\n' +
                    '      <div class="media-box">\n' +
                    '        <div class="ui-row p-3">\n' +
                    '          <div class="ui-col ui-col-75">\n' +
                    '            <div><span class="text-blue x-small">' + $.i18n.prop('orderNumber') + '</span>: <span class="text-b small">' + Id + '</span></div>\n' +
                    // '            <div><span class="text-blue x-small">' + $.i18n.prop('username') + ': </span> <span class="text-b small">' + orderUser + '</span> </div>\n' +
                    '          </div>\n' +
                    '          <div class="ui-col ui-col-25 order-status">\n' +
                    '            <div class="order-status-text ' + orderStatusName + '">' + oName + '</div>\n' +
                    '          </div>\n' +
                    '        </div>\n' +
                    '        <ul class="ui-list-text border-list t">\n' +
                    '          <li class="ui-border-b p-0"></li>\n' +
                    '        </ul>\n' +
                    '        <div class="ui-row p-3">\n' +
                    '          <div class="ui-col ui-col-50">\n' +
                    '            <div><span class="text-blue x-small">' + $.i18n.prop('total') + ' </span>: <span class="price-TAGs small"></span> <span class="text-b small text-red"> ' + orderTotal + '</span></div>\n' +
                    '          </div>\n' +
                    '          <div class="ui-col ui-col-50 text-right">\n' +
                    '            <div><span class="text-blue x-small">' + $.i18n.prop('date') + ' </span>: <span class="text-light-gray small">' + Date + '</span></div>\n' +
                    '          </div>\n' +
                    '        </div>\n' +
                    '      </div>\n' +
                    '    </li>';
                $('#myOrderList').append(template);
            }
        }

        changePriceType();
        //结束下拉刷新状态
        api.refreshHeaderLoadDone();
    });
}
function OpenMyOrderDetail(id) {
    localStorage.setItem('openFrame', 'order');
    api.openFrame({
        name: 'order',
        url: '../products/order-detail.html',
        bounces: false,
        animation: {
            type: "movein",
            subType: "from_right"
        },
        pageParam: {
            orderId: id,
            page: page
        }
    });
}
