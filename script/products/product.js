//function for getting product list from the server.
function loadProdList(currentpage, ifScroll) {
    if (!ifScroll) {
        //loading('show');
        $('#productsList').empty();
        insertPlaceHoder('myProduct');
        page = 0;
    }
    $('.no-item').hide();
    loading('show');
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var currentPage = currentpage || 0;
    var paramStr = {'userId':userId,'currentPage': currentPage, "itemsperpage": 12};
    var url = getBaseUrl() + "api/users/" + userId + "/products";
    var method = "GET";
    //console.log(url);
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        if (!ifScroll) {
            $('#productsList').empty();
        }
        var returnObj = data.Data;
        //console.log(JSON.stringify(returnObj));
        var len = returnObj.length;
        if (len === 0 && !ifScroll) {
            $('.no-item').show();
        } else {
            $('.no-item').hide();
            for (i = 0; i < len; i++) {
                var pic = returnObj[i].Product ? returnObj[i].Product.FeaturedPicture : returnObj[i].FeaturedPicture;
                //console.log(JSON.stringify(pic));
                if (!pic || pic.PictureUrl===null || pic.DownloadUrl === '') {
                    var picture = '../../image/no-image.png';
                } else {
                    var picture = pic.DownloadUrl;
                }
                var tweebaaTag;
                var productSource = returnObj[i].Product ? returnObj[i].Product.ProductSource: returnObj[i].ProductSource;
                if (productSource === 1) {
                    tweebaaTag = 'tweebaa-tag';
                } else {
                    tweebaaTag = '';
                }
                var type =  returnObj[i].ProductType;
                var Type;
                switch (type) {
                    case 1:
                        Type = 'physical';
                        break;
                    case 2:
                        Type = 'virtual';
                        break;
                    case 3:
                        Type = 'presale';
                        break;
                    case 4:
                        Type = 'freeoffer';
                        break;
                }
                if (Type === 'freeoffer') {
                    //console.log(type);
                    if (returnObj[i].StartTime == null) {
                        var startTime = $.i18n.prop("unlimited"); //'Unlimited'
                        var endtTime = $.i18n.prop("unlimited"); //'Unlimited
                    } else {
                        var startTime = moment.utc(returnObj[i].StartTime).local().format('YYYY-MM-DD HH:mm');
                        var endtTime = moment.utc(returnObj[i].EndTime).local().format('YYYY-MM-DD HH:mm');
                    }
                    var template = '<li class="media ui-col ui-col-50 waves ' + tweebaaTag + '" onclick="openDetail(' + returnObj[i].Id + ',\'' + Type + '\',\'' + tweebaaTag + '\')">\n' +
                        '<div class="media-box bottom-shadow text-center">\n' +
                        '<img class="img-fluid" src="' + picture + '" alt="image">\n' +
                        '<div class="media-body">\n' +
                        '<p class="ui-nowrap">' + returnObj[i].Name + '</p>\n' +
                        '<p class="small text-left pl-2">'+ $.i18n.prop("Fr") +' &nbsp; <span class="small">' + startTime + ' </span> </p> \n' +
                        '<p class="small text-right text-red  pr-2">'+ $.i18n.prop("To") +' &nbsp; <span class="small"> ' + endtTime + '</span></p>\n' +
                        // '<small class="float-right sold-count">Sold: ' + returnObj[i].Quantity + '</small>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</li>';
                } else if (Type === 'presale') {
                    var statusCode = returnObj[i].PrdStat;
                    var status;
                    switch (statusCode) {
                        case -1:
                            status ='<span>'+ $.i18n.prop("hide") +'</span>';
                            break;
                        case 0:
                            status ='<span>'+ $.i18n.prop("draft") +'</span>';
                            break;
                        case 1:
                            status ='<span class="text-blue">'+ $.i18n.prop("evaluation") +'</span>';
                            break;
                        case 2:
                            status ='<span class="text-green">'+ $.i18n.prop("preSale") +'</span>';
                            break;
                        case 3:
                            status ='<span class="text-green">'+ $.i18n.prop("onSale") +'</span>';
                            break;
                        case 4:
                            status ='<span class="text-yellow">'+ $.i18n.prop("onTweebaaApproval") +'</span>';
                            break;
                        case 5:
                            status ='<span class="text-red">'+ $.i18n.prop("tweebaaRefusal") +'</span>';
                            break;
                        case 6:
                            status ='<span class="text-yellow">'+ $.i18n.prop("waitingToStore") +'</span>';
                            break;
                        case 7:
                            status ='<span class="text-red">'+ $.i18n.prop("preSaleFailed") +'</span>';
                            break;
                        case 8:
                            status ='<span class="text-red">'+ $.i18n.prop("evaluationFailed") +'</span>';
                            break;
                        case 9:
                            status ='<span>'+ $.i18n.prop("cancelFromStore") +'</span>';
                            break;
                        case 10:
                            status ='<span class="text-red">'+ $.i18n.prop("refuseStore") +'</span>';
                            break;
                        case 11:
                            status ='<span class="text-blue">'+ $.i18n.prop("initialEvaluation") +'</span>';
                            break;
                        case 12:
                            status ='<span class="text-red">'+ $.i18n.prop("failInitialEvaluation") +'</span>';
                            break;
                        case 251:
                            status ='<span>'+ $.i18n.prop("inactive") +'</span>';
                            break;
                        case 254:
                            status ='<span>'+ $.i18n.prop("deleted") +'</span>';
                            break;
                    }
                    var template = '<li class="media ui-col ui-col-50 waves ' + tweebaaTag + '" onclick="openDetail(' + returnObj[i].Id + ',\'' + Type + '\',\'' + tweebaaTag + '\')">\n' +
                        '<div class="media-box bottom-shadow text-center">\n' +
                        '<img class="img-fluid" src="' + picture + '" alt="image">\n' +
                        '<div class="media-body">\n' +
                        '<p class="ui-nowrap">' + returnObj[i].Name + '</p>\n' +
                        '<h5 class="text-left text-red mt-3  pl-2"<span class="price-TAGs"></span>' + (returnObj[i].Price).toFixed(2) + '</h5>\n' +
                        '<p class="text-right sold-count  pr-2"><span>Status: </span>'+ status +'</p>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</li>';
                } else {
                    //console.log(type);
                    var template = '<li class="media ui-col ui-col-50 waves ' + tweebaaTag + '" onclick="openDetail(' + returnObj[i].Id + ',\'' + Type + '\',\'' + tweebaaTag + '\')">\n' +
                        '<div class="media-box bottom-shadow text-center">\n' +
                        '<img class="img-fluid" src="' + picture + '" alt="image">\n' +
                        '<div class="media-body">\n' +
                        '<p class="ui-nowrap">' + returnObj[i].Name + '</p>\n' +
                        '<h5 class="text-left mt-3 pl-2"><span class="price-TAGs small"></span> <span class="text-red text-title">' + (returnObj[i].Price).toFixed(2) + '</span></h5>\n' +
                        '<p class="text-right sold-count  pr-2">Sold: ' + returnObj[i].Sold + '</p>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</li>';
                }
                $('#productsList').append(template);
            }

        }
        changePriceType();
        //设置图片为正方形
        var imgW = $('.media-box').width();
        $('img').height(imgW);
        //结束下拉刷新状态
        api.refreshHeaderLoadDone();
        loading('hide');
    });
}

function openDetail(id, type, tweebaaTag) {
    localStorage.setItem('openFrame', 'myProductDetail');
    api.openWin({
        name: 'myProductDetail',
        url: '../products/my-product-detail.html',
        bounces:false,
        allowEdit:true,
        pageParam: {
            prodId: id,
            Ptype: type,
            tweebaaTag:tweebaaTag
        }
    });
}

//读取普通产品详细
function loadProd(id) {
    loading('show');
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/products/" + id;
    var method = "GET";
    //console.log(JSON.stringify(url));
    var liWith = $('#width').width() +5;
    var userName = localStorage.getItem('userName');
    var ShareUrl = 'www.tweebaa.'+ getApi()+'/tycoonplace/'+ userName +'/products/'+ id;
    $('#shareURL').data("shareUrl", ShareUrl);
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log("读取到的数据"+JSON.stringify(data));
        if (data.Pictures) {
            if (data.Pictures.length !== 0) {
                $('#hiddenImg').data("picturesUrl", data.Pictures[0].DownloadUrl);
                //console.log(JSON.stringify($('#hiddenImg').data("share")));
                //downloadPic(data.Pictures[0].DownloadUrl);
                var pic = data.Pictures;
                if (pic.length !== 0) {
                    var len = pic.length;
                    for (i = 0; i < len; i++) {
                        var picture = data.Pictures[i].DownloadUrl;
                        var img = '<img class="img-fluid" id ="9'+ i +'" src="'+ picture + '" alt="image">';
                        // var canvas = '<div class="canvas-box"><i class="icon ion-close-round del-img"></i><img src="' + picture + '" id="canvas' + data.Pictures[i].Id + '" width="300" height="300" class="image-canvas mb-3"></div>';
                        // $('#imagesBox').append(canvas);
                        $('.swiper-wrapper').append('<div class="swiper-slide">'+ img +'</div>');
                        $('#uploadImgBox').append('<li class="ui-col ui-col-25 img-li" style="height: '+ liWith +'px"><div class="media-box">'+ img +'</div></li>');
                    }
                    $('.shopping-cart-img').attr('src',data.Pictures[0].DownloadUrl);
                }
            }else{
                $('.swiper-wrapper').prepend('<div class="swiper-slide"><img style="height: 100%"  src="../../image/no-image.png" ></div>');
                $('.shopping-cart-img').attr('src','../../image/no-image.png');
            }
        }
        if(data.ProductSource === 1){
            $('body').addClass('tweebaa-tag');
            $('.shopping.addShopping-bar ').removeClass('d-none');
        }

        //推送内容
        $('#name').val(data.Name);
        $('.shopping-cart-name').text(data.Name);
        $('#shortDescription').val(data.ShortDescription);
        $('#fullDescription').val(data.FullDescription);
        $('#price').val(data.Price);
        $('.shopping-cart-price').text(data.Price);
        $('#oldprice').val(data.OldPrice);
        $("#sku").val(data.Sku);
        $('#quantity').val(data.Quantity);
        $("#shippingMethod").val(data.ShippingMethod);
        $("#shippingFee").val(data.ShippingFee);
        $("#showHomepage").prop('checked', data.ShowOnHomePage);
        $('.text-count').attr('data-count',$('#fullDescription').val().length);
        if (data.Categories) {
            if (data.Categories.length !== 0) {
                $('#selectCategory').val(data.Categories[0].Id);
                $('.selectCategory').text(data.Categories[0].Name);
                if (data.Categories[1]) {
                    $('#secondCategory').val(data.Categories[1].Id);
                    $('.secondCategory').text(data.Categories[1].Name);
                }
                if (data.Categories[2]) {
                    $('#thirdCategory').val(data.Categories[2].Id);
                    $('.thirdCategory').text(data.Categories[2].Name);
                }
            }
        }
        if (data.TweebaaProduct) {
            $("#enableTweebaa").prop('checked', true);
            $('#SuggestedRetailPrice').val(data.TweebaaProduct.TycoonCost);
            $('#WholesalePrice').val(data.TweebaaProduct.WholesalePrice);
            $('#tycoonComment').val(data.TweebaaProduct.TycoonComment);
        }
        $('.container').removeClass('invisible');

        if (data.ShippingMethods) {
            var SMD = data.ShippingMethods;
            if (SMD.length !== 0) {
                for (j = 0; j < SMD.length; j++) {
                    var template = '<tr>\n' +
                        '<td class="shipping-table-country" data-id=\'{\"Id\":' + SMD[j].Country.Id + '}\'>' + SMD[j].Country.Name + '</td>\n' +
                        '<td class="shipping-table-method">' + SMD[j].ShippingMethod + '</td>\n' +
                        '<td class="shipping-table-fee">' + SMD[j].ShippingFee + '</td>\n' +
                        '<td class="deleteShippingMethod"><i class="icon ion-close-round text-red"></i></td>\n' +
                        '</tr>';
                    $('#shippingTable').append(template);
                }
            }
        }

        if (data.Download != null) {
            getVideo(data.Download.DownloadUrl);
        }
        $("#videoData").data('video', data.Download);
        loading('hide');

        //在页面展示数据
        var box= $('.edit-box');
        box.each(function () {
            var text = $(this).find('.form-ctrl:not("select")').val();
            $(this).find('.pro-text').html(text);
        });
        //初始化图片展示
        initialize();
    });

}

//读取免费产品详细
function loadFreeDetail(id) {
    loading('show');
    var liWith = $('#width').width() +5;
    var userId = localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var day = moment().format('YYYY-MM-DDTHH:mm:ss');
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/freeoffers/" + id;
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        if (data.Product.FeaturedPicture.DownloadUrl) {
            var pic = data.Product.FeaturedPicture.DownloadUrl;
            $('#hiddenImg').data("picturesUrl", pic);
            //downloadPic(pic);
            var img = '<img class="img-fluid"  src="'+ pic + '" alt="image">';
            $('.swiper-wrapper').prepend('<div class="swiper-slide">'+ img +'</div>');
            $('#uploadImgBox').prepend('<li class="ui-col ui-col-25 img-li" style="height: '+ liWith +'px"><div class="media-box">'+ img +'</div></li>');
            $('.shopping-cart-img').attr('src',pic);
        }else{
            $('.swiper-wrapper').prepend('<div class="swiper-slide"><img style="height: 100%"  src="../../image/no-image.png" ></div>');
            $('.shopping-cart-img').attr('src','../../image/no-image.png');
        }
        $('#shareURL').data("shareUrl",data.ShareUrl);


        //推送内容
        $('#freetId').val(id);
        $('#productId').val(data.Product.Id);
        $('#name').val(data.Product.Name);
        $('.shopping-cart-name').text(data.Name);
        $('#sku').val(data.Product.sku);
        $('.shopping-cart-price').text(0);
        $('#shortDescription').val(data.Product.ShortDescription);
        $('#fullDescription').val(data.Product.FullDescription);
        $("#showHomepage").prop('checked', data.Product.ShowOnHomePage);
        $('#unlimited').prop('checked', data.IsUnlimitedTime);
        $('.text-count').attr('data-count',$('#fullDescription').val().length);
        if(data.TweebaaProduct){
            $("#enableTweebaa").prop('checked',true);
            $('.enable-tweebaa-box').removeClass('d-none');
            $('#SuggestedRetailPrice').val(data.Product.TweebaaProduct.TycoonCost);
            $('#WholesalePrice').val(data.Product.TweebaaProduct.WholesalePrice);
            $('#tycoonComment').val(data.Product.TweebaaProduct.TycoonComment);
        }
        loading('hide');
        //在页面展示数据
        var box= $('.edit-box');
        box.each(function () {
            var text = $(this).find('.form-ctrl:not("select")').val();
            $(this).find('.pro-text').html(text);
        });
        $("#unlimited").on('click', function () {
            if ($(this).prop("checked")) {
                $("#startTime,#endTime").attr("disabled", "disabled").removeAttr("required").val('');
            } else {
                $("#startTime,#endTime").attr("required", "required").removeAttr("disabled").val(day);
            }
        });
        if (data.StartTime !== null) {
            $("#startTime").val(moment.utc(data.StartTime).local().format('YYYY-MM-DDTHH:mm:ss'));
            $("#endTime").val(moment.utc(data.EndTime).local().format('YYYY-MM-DDTHH:mm:ss'));
            $("#startTime").closest('.edit-box').find('.pro-text').text(moment.utc(data.StartTime).local().format('YYYY-MM-DDTHH:mm:ss'));
            $("#endTime").closest('.edit-box').find('.pro-text').text(moment.utc(data.EndTime).local().format('YYYY-MM-DDTHH:mm:ss'));
        } else {
            $("#startTime").val('');
            $("#endTime").val('');
            $("#startTime").closest('.edit-box').find('.pro-text').text($.i18n.prop("unlimited"));
            $("#endTime").closest('.edit-box').find('.pro-text').text($.i18n.prop("unlimited"));
        }
        //初始化图片展示
        initialize();
    });
}

//将图片保存在本地以便分享
function downloadPic(pic) {
    // var n = Date.parse(new Date());
    // api.download({
    //     url: pic,
    //     savePath: 'fs://tycoon/' + n + '.png', //指定路径和文件名
    //     report: true,
    //     cache: true,
    //     allowResume: true
    // }, function (ret, err) {
    //     // console.log(ret);
    //     if (ret.state === 1) {
    //         $('#hiddenImg').val(ret.savePath);
    //         // console.log($('#hiddenImg').val());
    //     }
    // });
}
//如果有视频、图片或音频则在下面展示
function getVideo(DownloadUrl) {
    $('#productDisplay span').addClass('show');
    var extName = (DownloadUrl.substring(DownloadUrl.lastIndexOf('.') +1)).toLowerCase();
    if(extName === 'mp4' || extName === 'avi' || extName === 'rmvb' || extName === 'mpg' || extName === 'mpeg' || extName === 'mpeg4' || extName === 'mov' || extName === 'wmv' || extName === 'f4v') {
        $("#ShowVideo").attr("src", DownloadUrl).show();
        var video ='<video src="'+ DownloadUrl +'" id="videoDisplay" controls="controls" preload="auto" -webkit-playsinline=true></video>';
        $('#productDisplay').append( video );
        // $("#videoDisplay")[0].addEventListener("loadedmetadata", function () {
        //     $("#videoDisplay")[0].currentTime = 1;
        // });
    }else if(extName === 'jpg' || extName === 'jpeg' || extName === 'png' || extName === 'bmp' || extName === 'gif' || extName === 'raw'){
        $("#imgProduct").attr("src", DownloadUrl).show();
        var image ='<img src="'+ DownloadUrl +'" class="img-fluid" style="max-height: 50rem">';
        $('#productDisplay').append(image);
    }else if(extName === 'amr' || extName === 'wav' || extName === 'mp3' ){
        $("#showAudio").attr("src", DownloadUrl).css('display','block');
        var audio ='<audio src="'+ DownloadUrl +'"  controls="controls" preload="auto" ></audio>';
        $('#productDisplay').append(audio);
    }
    $('#uploadVideoBox').hide();
    $('.delete-video-btn').show();
}

//点击编辑按钮
$('#editSaveBtn').on('click',function () {
    var type = $('#productType').val();
    if(type === 'freeoffer'){
        editFreeProduct();
    }else{
        editGeneralProduct();
    }
});

// 编辑普通产品
function editGeneralProduct() {
    loading('show');
    var type = $('#productType').val();
    var userid = localStorage.getItem('userId');
    var name     = $("#name").val();
    var showT    = $("#enableTweebaa").prop('checked');
    var SuggestedPrice = $('#SuggestedRetailPrice').val();
    var WholesalePrice = $('#WholesalePrice').val();
    var TycoonComment = $('#tycoonComment').val();
    if (name === '') {
        messages($.i18n.prop('productNameEmpty'), 'error', 'top', 3); //'product name can not be empty!'
        return false;
    }
    if (showT && WholesalePrice === '') {
        messages($.i18n.prop('wholesalePricePriceEmpty'), 'error', 'top', 3); //' WholesalePrice Price can not be empty!'
        return false;
    }
    // //get pic
    var picArray = [];
    var pic;
    var picJson;
    //console.log($("#uploadImgBox").find('img').length);
    $("#uploadImgBox").find('img').each(function () {
        if($(this).attr('src').length>100){
            pic = $(this).attr('src').split(",")[1];
            picJson = {'PictureBinaryBase64': pic, 'SeoFileName': 'picture.jpg'};
        }else{
            pic = $(this).attr('src');
            picJson = {'PictureUrl': pic, 'SeoFileName': 'picture.jpg'};
        }
        picArray.unshift(picJson);
    });

    //shipping信息
    var shippingMethods = [];
    if (type === 'physical') {
        var quantity = $("#quantity").val();
        $('#shippingTable').find('tr').each(function () {
            var countryId = $(this).find('.shipping-table-country').data('id').Id;
            var countryText = $(this).find('.shipping-table-country').text();
            var shippingMethod = $(this).find('.shipping-table-method').text();
            var fee = $(this).find('.shipping-table-fee').text();
            var shipping = {'Country': {"Id": countryId, 'Name': countryText}, 'ShippingMethod': shippingMethod, 'ShippingFee': fee};
            shippingMethods.unshift(shipping);
        });
    }

    //git others
    var productId = api.pageParam.productId;
    var sku = $("#sku").val();
    var price = $("#price").val();
    var oldprice = $("#oldprice").val();
    var displayorder = 0;
    var shortdescription = $("#shortDescription").val();
    var fulldescription = $("#fullDescription").val();

    var showH = $("#showHomepage").prop('checked');
    var categories = [{"Id": $('#selectCategory').val()}, {"Id": $('#secondCategory').val()}, {"Id": $('#thirdCategory').val()}];

    var video = null;
    var videoData = $("#videoData").data('video');
    var videoInput = $('#getVideo').val();
    var videoTag = $('#ShowVideo').attr('src');
    if (!videoInput && videoTag) {  //视频没更改
        video = $("#videoData").data('video');
    }
    if (!videoInput && !videoTag) {
        if (videoData) {//需要删除当前视频
            delVideo(videoData.Id);
        }
    }
    var TweebaaProduct = null;
    if(showT){
        TweebaaProduct = {
            "SuggestedRetailPrice" :SuggestedPrice,
            "WholesalePrice" : WholesalePrice,
            "TycoonComment" : TycoonComment
        }
    }
    var token = localStorage.getItem('access_token');

    var url = getBaseUrl() + "api/users/" + userid + "/products/" + type + '/' + productId;
    var method = "PUT";
    var contentTypeStr = "application/json";
    var prodObj = {
        "Id": productId, "Name": name, "Sku": sku, "Quantity": quantity, "Price": price, "OldPrice": oldprice,
        "DisplayOrder": displayorder, "ShortDescription": shortdescription, "FullDescription": fulldescription,
        "Customer": {"Id": userid}, "Categories": categories,'ShippingMethods':shippingMethods, "ShowOnHomePage": showH, "Pictures": picArray, "Download": video,"TweebaaProduct":TweebaaProduct
    };
    //console.log("要修改的数据"+JSON.stringify(prodObj));
    $.when(postApi(url, method, prodObj, token, contentTypeStr)).done(function (data) {
        //console.log("修改后返回的数据"+JSON.stringify(data));
        if(data.status ===200){
            if (videoInput) {
                if(videoData){
                    delVideo(videoData.Id);
                    uploadVideo(productId, userid,'edit');
                }else{
                    uploadVideo(productId, userid,'edit');
                }
                if($('#cutVideoCheck').prop('checked')){
                    var startime = $('.slider-input').val().split(',')[0];
                    var endtime =  $('.slider-input').val().split(',')[1];
                    var PreviewStartTime = startime;
                    var PreviewDuration  = endtime - startime;
                    cutVideo(productId,userid,PreviewStartTime,PreviewDuration);
                }
            }
            else {
                loading('hide');
                messages($.i18n.prop('productUpdated'), 'success', 'top', 3); //'Product updated!'
                // scrollTo(0,0);
                // location.reload();
                var proTpye = $('#productType').val();
                api.sendEvent({
                    name: 'productListReload',
                    extra: {
                        type: proTpye
                    }
                });
                localStorage.setItem('openFrame', '');
                setTimeout(function () {
                    back();
                },2500)

            }
        }else{
            loading('hide');
            messages($.i18n.prop('systemError'), 'error', 'top', 2);
        }


    });

}
//编辑免费产品
function editFreeProduct() {
    var name = $("#name").val();
    var showT = $("#enableTweebaa").prop('checked');
    var SuggestedPrice = $('#SuggestedRetailPrice').val();
    var WholesalePrice = $('#WholesalePrice').val();
    var TycoonComment = $('#tycoonComment').val();

    if (showT && WholesalePrice === '') {
        messages($.i18n.prop('wholesalePricePriceEmpty'), 'error', 'bottom', 3); //' WholesalePrice Price can not be empty!'
        return false;
    }
    var id = $('#freetId').val();
    var productId = $('#productId').val();
    var userid = localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var star = moment($("#startTime").val()).utc().format('YYYY-MM-DDTHH:mm:ss');
    var end = moment($("#endTime").val()).utc().format('YYYY-MM-DDTHH:mm:ss');
    var showH = $("#showHomepage").prop('checked');
    var unlimited = $('#unlimited').prop('checked');
    var TweebaaProduct = null;
    if (showT) {
        TweebaaProduct = {
            "SuggestedRetailPrice": SuggestedPrice,
            "WholesalePrice": WholesalePrice,
            "TycoonComment": TycoonComment
        }
    }
    var prodObj = {
        "Id": id,
        "Name": name,
        "UserId": userid,
        "Product": {"Id": productId},
        //"ShowInTweebaaProducts": showT,
        "ShowOnHomePage": showH,
        "IsActive": true,
        "IsUnlimitedTime": unlimited,
        "StartTime": star,
        "EndTime": end,
        "TweebaaProduct": TweebaaProduct
    };
    loading('show');
    var url = getBaseUrl() + "api/users/" + userid + "/products/freeoffer/" + id;
    var method = "PUT";
    var contentTypeStr = "application/json";
    // console.log(prodObj);
    $.when(postApi(url, method, prodObj, token, contentTypeStr)).done(function (data) {
        //console.log(JSON.stringify(data));
        loading('hide');
        if (data.status == '200') {
            messages($.i18n.prop('productUpdated'), 'success', 'bottom', 3); //'Product updated!'
            var proTpye = $('#productType').val();
            api.sendEvent({
                name: 'productListReload',
                extra: {
                    type: proTpye
                }
            });
            localStorage.setItem('openFrame', '');
            setTimeout(function () {
                back();
            },2500)

        } else {
            messages($.i18n.prop('systemError'), 'error', 'top', 2);
        }

    });
}



//点击删除按钮
$('#deleteProductBtn').on('click',function () {
    confirmBox($.i18n.prop('wantDeleteProduct'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error');
    $('.dialog-box').find('button').on('click',function (e) {
        var index = $(this).attr('id');
        if (index === 'btn-2') {
            var id = api.pageParam.productId;
            var type = api.pageParam.Ptype;
            var tweebaa = api.pageParam.tweebaaTag;
            if(type ==='freeoffer'){
                delFreeProduct(id,type);
            }else{
                deleteProd(id,type);
            }
            $('.dialog-box').remove();
        } else {
            $('.dialog-box').remove();
            return false;
        }
    });

});

//删除普通产品 //移除tweebaa产品从自己的产品库用一个方法
function deleteProd(productId,productType) {
    var userid = localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var videoData = $("#videoData").data('video');
    //var productId= api.pageParam.productId; 2534
    var url = getBaseUrl() + "api/users/" + userid + "/products/" + productId;
    var method = "DELETE";
    var paramStr = {"userId": userid, "productId": productId};
    //console.log(JSON.stringify(videoData));
    if (videoData) {//需要删除当前视频
        delVideo(videoData.Id);
    }
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        if(data.status ==200){
            delSuccess(productType);
        }else {
            messages($.i18n.prop('systemError'), 'error', 'bottom', 2);
        }

    });
}

//删除免费产品
function delFreeProduct(id,productType) {
    var userid = localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var url = getBaseUrl() + "api/users/" + userid + "/freeoffers/" + id;
    var method = "DELETE";
    var paramStr = {"userId": userid, "offerId": id};
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        delSuccess(productType);
    });
}

//删除视频
function delVideo(downloadId) {
    var token = localStorage.getItem('access_token');
    var url = getBaseUrl() + "api/files/" + downloadId;
    //console.log(url);
    $.ajax({
        contentType: "application/json",
        method: "DELETE",
        url: url,
        async: true,
        dataType: 'json',
        headers: {"Authorization": "Bearer " + token}
    }).done(function (data) {
        // var msg = JSON.stringify(data);
        //defer.resolve(data);
        console.log("data: " + JSON.stringify(data));
        // console.log("data=" + data);

    }).fail(function (err) {
        //defer.resolve(err);
        console.log("err: " + JSON.stringify(err));
    });
}

//删除后返回列表
// function delSuccess(productType) {
//     api.sendEvent({
//         name: 'productListReload',
//         extra: {
//             type: productType
//         }
//     });
//     var msg = $.i18n.prop('productDeleted'); //"Your product has been deleted!"
//     messages(msg, 'success', 'bottom', 2);
//     setTimeout(function () {
//         localStorage.setItem('openFrame', '');
//         api.closeFrame({
//             name: 'productDetail',
//             animation:{
//                 type:"suck",
//                 duration:300
//             }
//         });
//     },2000);
// }

//初始化图片显示
function initialize() {
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        initialSlide :0 ,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction'
        }
    });
    var imgW = $('#width').width();
    $('#uploadImgBox .ui-col,#uploadVideoBox .ui-col').height(imgW);
}

//添加后返回列表
function addSuccess(type) {
    var proTpye = $('#productType').val();
    api.sendEvent({
        name: 'productListReload',
        extra: {
            type: proTpye
        }
    });
    localStorage.setItem('openFrame', '');
    back();
}
//购物车数量控制
// var shoppingCartQuantity = 1;
// $('.quantity-ctrl').on('touchstart',function () {
//     if($(this).hasClass('quantity-ctrl-add')){
//         shoppingCartQuantity ++;
//         $('.quantity-ctrl-minus').removeClass('disabled')
//     }else if($(this).hasClass('quantity-ctrl-minus')){
//         shoppingCartQuantity --;
//         if(shoppingCartQuantity === 1){
//             $(this).addClass('disabled');
//         }
//     }
//     $('.shopping-cart-quantity').text(shoppingCartQuantity);
// });
//加入购物车
// function addToShoppingCart() {
//     var userId =  localStorage.getItem('userId');
//     var storeId = localStorage.getItem('storeId');
//     var productId = api.pageParam.productId;
//     var token = localStorage.getItem('access_token');
//     var paramStr = {"StoreId" : storeId, "ProductId" : productId ,"Quantity" : shoppingCartQuantity };
//     var url = getBaseUrl() + "api/users/"+ userId +"/shopping-cart";
//     var method = "POST";
//     loading('show');
//     console.log(JSON.stringify(paramStr));
//     $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
//         console.log(JSON.stringify(data));
//         loading('hide');
//         if(data.status ===200){
//             messages($.i18n.prop('AddedShoppingCart'), 'success', 'center', 2);
//             hideBounceboxBottom('shopping-cart');
//             shoppingCartQuantity = 1;
//             $('.shopping-cart-quantity').text('1');
//             api.sendEvent({
//                 name: 'shoppingCartCount'
//             });
//             api.sendEvent({
//                 name: 'updateCart'
//             });
//         }else{
//             messages($.i18n.prop('systemError'), 'error', 'center', 2);
//         }
//
//     });
// }
