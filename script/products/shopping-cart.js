
//获取购物车列表
function getShoppingCartList() {
    loading('show');
    var userId =  localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/shopping-cart/items";
    var method = "GET";
    // console.log(url);
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        //结束下拉刷新状态
        console.log(JSON.stringify(data));
        api.refreshHeaderLoadDone();
        var UL = $('.shopping-cart');
        UL.empty();
        if(!data.length){
            getTotalPrice();
            UL.append('<li class="empty-cart text-dark-gray" style="box-shadow: none; margin-top: 5rem">'+ $.i18n.prop('NoItemCart')+' </li>');
            $('#checkOutBar').addClass('d-none-im');
            return false;
        }
        var template,cartItemId,name,customerId,storeId,ownerId,productId,quantity,price,picture,productType,productSource,owner;
        for(i=0; i < data.length;i++){
            name = data[i].Product.Name;
            cartItemId = data[i].Id;
            customerId = data[i].CustomerId;
            storeId = data[i].StoreId;
            ownerId = data[i].OwnerId;
            productId = data[i].ProductId;
            quantity = data[i].Quantity;
            price =  !data[i].Product.IsFreeOffer ? data[i].Product.Price: 0;
            productType = data[i].Product.ProductType;
            productSource = data[i].Product.ProductSource;
            picture = data[i].Product.FeaturedPicture ? data[i].Product.FeaturedPicture.DownloadUrl :'../../image/no-image.png';
            owner = data[i].Owner;
            template = '<li class="ui-row container-box bottom-shadow" id="'+ cartItemId+'">\n' +
                '            <div class="text-dark-gray text-title mb-3">'+owner.StoreName+'</div>\n' +
                '            <div class="ui-col ui-col-25 text-center" onclick="openProductDetail('+ productId +','+ productType +','+ productSource +')">\n' +
                '                <div class="shopping-cart-img-box">\n' +
                '                    <img src="'+ picture +'" alt="" class="shopping-cart-img img-responsive">\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="ui-col ui-col-75 pl-3">\n' +
                '                <span class="shopping-cart-name pb-4 small">'+ name +'</span>\n' +
                '                <h4 class="text-red pb-4"><span class="small price-TAGs">$ </span><span class="shopping-cart-price">'+ price +'</span></h4>\n' +
                '                <div class="text-right">\n' +
                '                    <i class="icon iconfont icon-empty pull-left text-light-gray" onclick="delItem(this)"></i>\n' +
                '                    <span class="quantity-ctrl quantity-ctrl-minus disabled" onclick="reduceCount(this)">-</span>\n' +
                '                    <span class="shopping-cart-quantity">'+ quantity +'</span>\n' +
                '                    <span class="quantity-ctrl quantity-ctrl-add" onclick="increaseCount(this)">+</span>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <input type="hidden" class="product-id" value="'+ productId +'"> \n' +
                '            <input type="hidden" class="customer-id" value="'+ customerId +'"> \n' +
                '            <input type="hidden" class="store-id" value="'+ storeId +'"> \n' +
                '            <input type="hidden" class="owner-id" value="'+ ownerId +'"> \n' +
                '            <input type="hidden" class="quantity" value="'+ quantity +'"> \n' +
                '            <input type="hidden" class="price" value="'+ price +'"> \n' +
                '        </li>';
            UL.append(template);
        }
        getTotalPrice();
        changePriceType();
        $('#checkOutBar').removeClass('d-none-im');
    });
}
//在购物车打开产品详情
function openProductDetail(id,type,productSource){
   //todo
}
//购车数量及价格修改
function increaseCount(This) {
    ctrlCartCount(This,'increase');
}
function reduceCount(This) {
    if($(This).hasClass('disabled')){
        return false;
    }
    ctrlCartCount(This,'reduce');
}
function ctrlCartCount(This,method) {
    var el = $(This).closest('.container-box');
    var quantity = el.find('input.quantity');
    var displayQuantity = el.find('.shopping-cart-quantity');
    if(method === 'reduce'){
        quantity.val(Number(quantity.val()) - 1);
    }else{
        quantity.val(Number(quantity.val()) + 1);
    }
    displayQuantity.text(quantity.val());
    getTotalPrice();
}

//删除一个产品在购物车中
function delItem(This) {
    confirmBox($.i18n.prop('wantDeleteProduct'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error');
    $('.dialog-box').find('button').on('click',function (e) {
        var index = $(this).attr('id');
        $('.dialog-box').remove();
        if (index === 'btn-2') {
            var shoppingCartItemId = $(This).closest('.container-box').attr('id');
            var userId =  localStorage.getItem('userId');
            var token = localStorage.getItem('access_token');
            var paramStr = {};
            var url = getBaseUrl() + "api/users/"+ userId +"/shopping-cart/items/" + shoppingCartItemId;
            var method = "DELETE";
            $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
                //console.log(JSON.stringify(data));
                getShoppingCartList();
                api.sendEvent({
                    name: 'shoppingCartCount'
                });
            });
        } else {
            return false;
        }
    });
}

//统计总价格
function getTotalPrice() {
    var total = 0;
    $('.shopping-cart').find('li').each(function () {
        if($(this).length>0){
            var quantity = Number($(this).find('input.quantity').val());
            var price = Number($(this).find('input.price').val()) * quantity;
            total = total + price;
            if($(this).find('.quantity').val()>1){
                $(this).find('.quantity-ctrl-minus').removeClass('disabled');
            }else{
                $(this).find('.quantity-ctrl-minus').addClass('disabled');
            }
        }else{
            total = 0;
        }
    });
    //console.log(total);
    $('#cartTotalPrice').text(total.toFixed(2));
}
//checkOut
function checkOut() {

    var shoppingCart = [];
    var addressInfo = {
        "addressId" :$('#currentAddressId').val(),
        "addressName" :$('#currentAddressName').val(),
        "addressPhone" :$('#currentAddressPhone').val(),
        "address" :$('#currentAddressAddress').val()
    };
    if(!addressInfo.addressId){
        messages($.i18n.prop('needAddress'), 'error', 'top', 3);
        return false;
    }
    $('ul.shopping-cart').find('li.container-box').each(function () {
        var cartInfo = {};
        cartInfo.StoreId =   Number($(this).find('input.store-id').val());
        cartInfo.ProductId = Number($(this).find('input.product-id').val());
        cartInfo.Quantity =  Number($(this).find('input.quantity').val());
        shoppingCart.push(cartInfo);
    });
    if(shoppingCart.length===0){
        return false;
    }
    loading('show');
    var userId =  localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = shoppingCart;
    var url = getBaseUrl() + "api/users/"+ userId +"/shopping-cart/review?shippingAddressId=" + addressInfo.addressId;
    var method = "PUT";
     // console.log(JSON.stringify(url));
     // console.log(JSON.stringify(paramStr));
    $.when(postApi(url, method, paramStr, token, 'json')).done(function (data) {
        // console.log(JSON.stringify(data));
        //  console.log(JSON.stringify("————————————————————————————————————————————————————————————"));
        loading('hide');
        localStorage.setItem('openFrame', 'checkoutPage');
        api.openFrame({
            name: 'checkoutPage',
            url: '../products/checkout.html',
            bounces:false,
            animation:{
                type:"movein",
                subType:"from_right"
            },
            pageParam:{
                "addressInfo":addressInfo,
                "checkoutInfo": data
            }
        });
    });
}
//submitOrder
function submitOrder() {
    loading('show');
    var token = localStorage.getItem('access_token');
    var paramStr = {
        "shippingAddressId" : $('#currentAddressId').val(),
        "userId" : localStorage.getItem('userId')
    };
    var url = getBaseUrl() + "api/users/"+ paramStr.userId +"/shopping-cart/to/order?shippingAddressId="+ paramStr.shippingAddressId;
    var method = "POST";
    //console.log(JSON.stringify(url));
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        // console.log('=====================================================');
        // console.log(JSON.stringify(data));
        // console.log('=====================================================');
        var Data = data;
        loading('hide');
        if(data.Id){
            api.sendEvent({
                name: 'updateCart'
            });
            api.sendEvent({
                name: 'shoppingCartCount'
            });
            if(data.Total === 0){
                var userId =  localStorage.getItem('userId');
                var token = localStorage.getItem('access_token');
                var paramStr = {
                    "PaymentMethod" :'alipay',
                    "Amount" :0,
                    "CurrencyCode" :  getApi() === 'com' ?"USD":'CNY',
                    "Data":''
                };
                var url = getBaseUrl() + "api/users/"+ userId +"/orders/" + Data.Id;
                var method = "PUT";
                $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
                    // console.log(JSON.stringify(data));
                    paymentResult('',Data,"free");
                });
            }else {
                if(getApi() === 'cn'){
                    alipay(data);
                }else{
                    payPal(data);
                }
            }

        }
    });
}

//打开地址列表
function changeAddress() {
    localStorage.setItem('openFrame', 'addressList');
    api.openFrame({
        name: 'addressList',
        url: '../users/address-list.html',
        bounces:false,
        animation:{
            type:"movein",
            subType:"from_right"
        }
    });
}
//获取邮寄地址
function getAddressList() {
    loading('show');
    var userId =  localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/addresses";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        loading('hide');
        $('#userAddressList').empty();
        if(data.length > 0){
            $('.no-address').addClass('d-none');
            $('.has-address').removeClass('d-none');
            var Id,name,phone,address,template,defaultAddress;
            for(i=0; i< data.length; i++){
                Id = data[i].Id;
                name = data[i].FirstName;
                phone = data[i].PhoneNumber;
                address = data[i].Address1;
                defaultAddress = data[i].IsPrimaryShipping ? 'text-red':'text-dark-gray';
                template = '<li class="container-box ui-row vertical-center bottom-shadow mb-4">\n' +
                    '            <div class="ui-col ui-col-90" onclick="changeCurrentAddress('+ Id +',\''+ name+'\','+ phone +',\''+ address +'\')">\n' +
                    '                <div class="custom-name ui-col ui-col-50 pl-5">'+ name +'</div>\n' +
                    '                <div class="custom-phone ui-col ui-col-50 text-right"><i class="icon iconfont icon-phone small text-light-gray"></i> '+ phone +'</div>\n' +
                    '                <div class="ui-col ui-col-100">\n' +
                    '                    <i class="icon iconfont icon-weizhi '+ defaultAddress +'"></i>\n' +
                    '                    <span class="custom-address small text-dark-gray"> '+ address +'</span>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '            <div class="ui-col ui-col-10 text-center" onclick="openAddressPage('+ Id +')"><i class="icon iconfont icon-order text-light-gray"></i></div>\n' +
                    '            <input type="hidden" id="addressId" value="'+ Id +'" >\n' +
                    '        </li>';
                $('#userAddressList').prepend(template);
            }
        }else {
            $('.has-address').addClass('d-none');
            $('.no-address').removeClass('d-none');
        }
    });
}
//更换当前地址
function changeCurrentAddress(addressId,name,phone,address) {
    api.sendEvent({
        name: 'currentAddress',
        extra: {
            currentAddress: {
                "addressId":addressId,
                "name":name,
                "phone":phone,
                "address":address
            }
        }
    });
    //console.log(addressId);
    setTimeout(function () {
        back();
    })
}
//获取默认地址
function getDefaultAddress() {
    var userId =  localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/addresses";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        var Id,name,phone,address,template;
        for(i=0; i< data.length; i++){
            if(data[i].IsPrimaryShipping || data.length === 1){
                Id = data[i].Id;
                name = data[i].FirstName;
                phone = data[i].PhoneNumber;
                address = data[i].Address1;
                template = '     <div class="ui-col ui-col-90">\n' +
                    '                <div class="custom-name ui-col ui-col-50 pl-5">'+ name +'</div>\n' +
                    '                <div class="custom-phone ui-col ui-col-50 text-right"><i class="icon iconfont icon-phone small text-light-gray"></i> '+ phone +'</div>\n' +
                    '                <div class="ui-col ui-col-100">\n' +
                    '                    <i class="icon iconfont icon-weizhi text-blue"></i>\n' +
                    '                    <span class="custom-address small text-dark-gray"> '+ address +'</span>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '            <input type="hidden" id="currentAddressId" value="'+ Id +'" >\n' +
                    '            <input type="hidden" id="currentAddressName" value="'+ name +'" >\n' +
                    '            <input type="hidden" id="currentAddressPhone" value="'+ phone +'" >\n' +
                    '            <input type="hidden" id="currentAddressAddress" value="'+ address +'" >\n' +
                    '            <div class="ui-col ui-col-10 text-center" ><i class="icon iconfont icon-right text-light-gray"></i></div>';
                $('#defaultAddress').html(template).removeClass('empty').addClass('fill');
            }
        }
    });
}
//打开添加修改地址页面
function openAddressPage(id) {
    var ID = id || '' ;
    localStorage.setItem('openFrame', 'addAddress');
    api.openFrame({
        name: 'addAddress',
        url: '../users/add-address.html',
        bounces:false,
        animation:{
            type:"movein",
            subType:"from_right"
        },
        pageParam: {
            addressId:ID
        }
    });
}

//添加地址
$('form#addressForm').submit(function (event) {
    event.preventDefault();
    var server = getApi();
    var province = $(this).find('#selectProvince').val() ?  Number($(this).find('#selectProvince').val()):null;
    var FirstName = $(this).find('#FirstName').val().trim();
    var Email = $(this).find('#Email').val().trim();
    var CountryId = server === 'com' ?  Number($(this).find('#selectCountry').val()): 21;
    var ProvinceId = server === 'com' ?  province : null;
    var City = server === 'com'? $(this).find('#City').val() :'';
    var Address1 = $(this).find('#Address1').val().trim();
    var PostalCode = $(this).find('#PostalCode').val().trim();
    var PhoneNumber = $(this).find('#PhoneNumber').val().trim();
    var IsPrimaryShipping = $('#defaultAddress').prop('checked');
    var paramStr = {
        "FirstName": FirstName,
        "LastName": '',
        "Email": Email,
        "Company": null,
        "CountryId": CountryId,
        "ProvinceId": ProvinceId,
        "City": City,
        "Address1": Address1,
        "Address2": null,
        "PostalCode": PostalCode,
        "PhoneNumber": PhoneNumber,
        "FaxNumber": null,
        "CustomAttributes": "",
        "CompanyUrl": null,
        "IsPrimaryBilling":false,
        "IsPrimaryShipping":IsPrimaryShipping
    };
    var szRegE=/^[A-Za-z0-9\u4e00-\u9fa5._-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
    if(!FirstName){
        sendError('FirstName');
        return false;
    }else if(!Email || !szRegE.test(Email)){
        sendError('Email');
        return false;
    }else if(!Address1){
        sendError('Address1');
        return false;
    }else if(!PhoneNumber){
        sendError('PhoneNumber');
        return false;
    }
    loading('show');
    var url,method;
    var userId =  localStorage.getItem('userId');
    if($(this).hasClass('edit')){
        url = getBaseUrl() + "api/users/"+ userId +"/addresses/"+ $('#addressId').val();
        method = "PUT";
    }else{
        url = getBaseUrl() + "api/users/"+ userId +"/addresses";
        method = "POST";
    }
    //console.log(JSON.stringify(paramStr));
    var token = localStorage.getItem('access_token');
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
       //console.log(JSON.stringify(data));
        loading('hide');
        if(data.status === 200){
            api.sendEvent({
                name: 'addressListReload'
            });
            api.sendEvent({
                name: 'reloadAddressPage'
            });
            setTimeout(function () {
                back();
            },300);

        }
    });
});
//清除错误提示
$('form#addressForm').find('.form-ctrl').on('focus',function () {
    $(this).removeClass('error');
});
//发送错误消息
function sendError(name){
    messages($.i18n.prop('incorrectInformation'), 'error', 'top', 2);
    $('form#addressForm').find('#'+name).addClass('error');
}

//国家列表
function loadContry(id) {
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/countries?lang="+lan;
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        console.log(JSON.stringify(data));
        var len = data.length;
        var selected;
        for (i = 0; i < len; i++) {
            selected = id === data[i].Id ?'selected':'';
            $('#selectCountry').append('<option value="' + data[i].Id + '" '+ selected+'>' + data[i].Name + '</option>');
        }
    });
}
//获取地区
$('#selectCountry').on('change',function () {
    var cid = $(this).val();
    getProvince(cid);
});
function getProvince(cid,pid) {
    $('#selectProvince').empty();
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/countries/"+ cid +"/provinces";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        var id,name,template;
        if(data.length > 0){
            for(i=0;i<data.length;i++){
                id = data[i].Id;
                name = data[i].Name;
                template = '<option value="'+ id +'">'+ name +'</option>';
                $('#selectProvince').append(template);
            }
        }else{
            template = '<option value="null" >Other (Non US)</option>';
            $('#selectProvince').append(template);
        }
        if(pid){
            $('#selectProvince').val(pid);
        }
    });
}

//显示地址详细信息
function showAddress(id) {
    loading('show');
    var userId =  localStorage.getItem('userId');
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/addresses/" + id;
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        var el = $('form.address-form');
        el.find('#FirstName').val(data.FirstName);
        el.find('#LastName').val(data.LastName);
        el.find('#Email').val(data.Email);
        el.find('#City').val(data.City);
        el.find('#Address1').val(data.Address1);
        el.find('#PhoneNumber').val(data.PhoneNumber);
        el.find('#PostalCode').val(data.PostalCode);
        // el.find('#selectCountry').val(data.CountryId);
        el.find('#addressId').val(data.Id);
        el.find('#defaultAddress').prop('checked',data.IsPrimaryShipping);
        getProvince(data.CountryId,data.ProvinceId);
        loadContry(data.CountryId);
    });
}
//删除地址
$('#deleteAddressBtn').on('click',function () {
    confirmBox($.i18n.prop('wantDeleteAddress'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error');
    $('.dialog-box').find('button').on('click',function (e) {
        var index = $(this).attr('id');
        $('.dialog-box').remove();
        if (index === 'btn-2') {
            var id = $('input#addressId').val();
            var userId =  localStorage.getItem('userId');
            var token = localStorage.getItem('access_token');
            var paramStr = {};
            var url = getBaseUrl() + "api/users/"+ userId +"/addresses/" + id;
            var method = "DELETE";
            $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
                //console.log(JSON.stringify(data));
                if(data.status === 200){
                    api.sendEvent({
                        name: 'addressListReload'
                    });
                    setTimeout(function () {
                        back();
                    },300);
                }else if(data.responseJSON){
                    messages($.i18n.prop('cannotDelAddress'), 'error', 'top', 3);
                }
            });
        } else {
            return false;
        }
    });

});

//payPal付款
function payPal(orderInfo) {
    //console.log(JSON.stringify(orderInfo));
    var paypal = api.require('paypal');
    var shippingAdd = api.pageParam.addressInfo;
    var items = '';
    for(i=0;i < orderInfo.OrderItems.length;i++){
        items = items + (orderInfo.OrderItems[i].Product.Name + ' * ' + orderInfo.OrderItems[i].Quantity)
    }
    var description = items + ' Shipping to:'+ shippingAdd.addressName +' '+ shippingAdd.address +' phone:'+ shippingAdd.addressPhone;
    paypal.pay({
        currency: 'USD',
        price: orderInfo.Total,
        description: description,
        mode: 'production' //production or sandbox
    }, function(ret) {
        if(ret.state === "success"){
            var userId =  localStorage.getItem('userId');
            var token = localStorage.getItem('access_token');
            var paramStr = {
                "PaymentMethod" : "paypal",
                "Amount" :orderInfo.Total,
                "CurrencyCode" : "USD",
                "Data":ret
            };
            // console.log(JSON.stringify(paramStr));
            var url = getBaseUrl() + "api/users/"+ userId +"/orders/" + orderInfo.Id+"?mode=prod";
            // console.log(JSON.stringify(url));
            var contentType = "application/json";
            var method = "PUT";
            $.when(postApi(url, method, paramStr, token, contentType)).done(function (data) {
                // console.log(JSON.stringify(data));
                paymentResult(ret,orderInfo,"paypal");
            });
        }else{
            paymentResult(ret,orderInfo,"paypal");
        }

    });

}
//支付宝付款
function alipay(orderInfo) {
    loading('show');
    var token = localStorage.getItem('access_token');
    var paramStr = {
        "OrderId": orderInfo.Id,
        "Amount": orderInfo.Total,
        "mode":'prod',  //  mode 参数 （dev/prod)
        "CurrencyCode": "CNY"
    };
    var url = getBaseUrl() + "api/payments/alipay?mode=prod";//  mode 参数 （dev/prod)
    var method = "PUT";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        if(data.response){
            var aliPayPlus = api.require('aliPayPlus');
            aliPayPlus.payOrder({
                orderInfo:data.response
            }, function(ret, err) {
                console.log(JSON.stringify(ret));
                // console.log(JSON.stringify(err));
                if(ret.code === "9000"){
                    var userId =  localStorage.getItem('userId');
                    var token = localStorage.getItem('access_token');
                    var paramStr = {
                        "PaymentMethod" : "alipay",
                        "Amount" :orderInfo.Total,
                        "CurrencyCode" : "CNY",
                        "Data":null
                    };
                    var url = getBaseUrl() + "api/users/"+ userId +"/orders/" + orderInfo.Id+"?mode=prod";
                    var method = "PUT";
                    var contentType = "application/json";
                    $.when(postApi(url, method, paramStr, token, contentType)).done(function (data) {
                        console.log(JSON.stringify(data));
                        paymentResult(ret,orderInfo,"alipay");
                    });
                }else{
                    paymentResult(ret,orderInfo,"alipay");
                }
            });
        }
    });

}

function paymentResult(paymentResult,orderInfo,paymentMethod) {
    localStorage.setItem('openFrame', 'paymentResult');
    api.openFrame({
        name: 'paymentResult',
        url: '../products/payment-result.html',
        bounces:false,
        animation:{
            type:"movein",
            subType:"from_right"
        },
        pageParam: {
            addressInfo:api.pageParam.addressInfo,
            shoppingData:orderInfo,
            paymentResult:paymentResult,
            paymentMethod:paymentMethod
        }
    });
}
