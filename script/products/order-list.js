var page;
//apiready = function () {
    //上拉加载
    // api.addEventListener({
    //     name: 'scrolltobottom',
    //     extra: {
    //         threshold: 0        //设置距离底部多少距离时触发，默认值为0，数字类型
    //     }
    // }, function (ret, err) {
    //     loading('show');
    //     page++;
    //     var Type = $('#orderType').val();
    //     orderList.getOrderList(Type, page);
    // });
    //
    // api.addEventListener({
    //     name: 'swiperight'
    // }, function (ret, err) {
    //     back();
    // });

    var orderList = new Vue({
        el: '#orderList',
        mounted: function () {
            this.getOrderList(null,0);
        },
        data: {
            orderData: '',
        },
        methods:{
            getOrderList: function (type,page,event) {
                var that = this;
                var userId = localStorage.getItem('userId');
                if(event){
                    page = 0;
                    var tar = event.target;
                    $(tar).parent().addClass('current-tag').siblings().removeClass('current-tag');
                }
                var currentPage = page || 0;
                var url;
                switch (type) {
                    case 'paid':
                        url = getBaseUrl() + "api/orders?ownerId="+userId+"&paymentStatus=paid&currentpage="+currentPage+"&itemsperpage=24";
                        break;
                    case 'unpaid':
                        url = getBaseUrl() + "api/orders?ownerId="+userId+"&paymentStatus=pending&currentpage="+currentPage+"&itemsperpage=24";
                        break;
                    default:
                        url = getBaseUrl() + "api/orders?ownerId="+userId+"&currentpage="+currentPage+"&itemsperpage=24";
                }
                //console.log(url);
                getAxois(url,"GET",{}).then(function (response) {
                    //console.log(JSON.stringify(response));
                    that.orderData = response.data;
                    setTimeout(function () {
                        changePriceType();
                        setOrder();
                    });
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            openOrderDetail:function (orderId) {
                var url = 'order-detail.html';
                openWindows("orderDetail",url,{"orderId":orderId});
            }
        }
    });

//};

function setOrder() {
    $('.order-list').each(function () {
        var time = $(this).find('.order-time');
        var status = $(this).find('.order-status-text');
        var statusText;
        switch (status.text()) {
            case "Pending":
                statusText = $.i18n.prop('unpaid');
                break;
            case "Paid":
                statusText = $.i18n.prop('Paid');
                break;
        }
        time.text(moment.utc(time.attr('data-time')).local().format('YYYY-MM-DD HH:mm'));
        status.text(statusText);
    })
}

function back() {
    // localStorage.setItem('openFrame', '');
    api.closeWin({
        name: 'orders'
    });
}