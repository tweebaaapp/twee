apiready = function () {
    var proTpye = 'virtual';
    // loadCategoryList('digital'); //载入分类
    $('body').addClass(proTpye);
    // $("input[name='productType']").on('change',function () {
    //     proTpye = $("input[name='productType']:checked").val();
    //     $('body').removeClass('virtual physical presale freeoffer').addClass(proTpye);
    //     if(proTpye === 'virtual'){
    //         // loadCategoryList('digital');
    //         $('#textUpProduct').removeClass('d-none');
    //         $('#textUpVideo,.product-quantity,.shippingBox').addClass('d-none');
    //         $('#getVideo').attr('accept',"*");
    //     }else if(proTpye === 'physical' || proTpye === 'presale'){
    //         // loadCategoryList(proTpye);
    //         loadContry();
    //         $('#textUpVideo,.product-quantity,.shippingBox').removeClass('d-none');
    //         $('#textUpProduct').addClass('d-none');
    //         $('#getVideo').attr('accept',"video/*");
    //     }else if(proTpye === 'freeoffer'){
    //         loadVirtualProductsList();
    //     }
    // });
    changePriceType();
};

//自有产品创建
// function saveNewProdcut() {
//     var proType = $("input[name='productType']:checked").val();
//     if (proType !== 'freeoffer') {
//         addGeneralProduct(proType);
//     } else {
//         addFreeProduct(proType);
//     }
// }

//添加普通产品
function addGeneralProduct(proType) {
    var userId = localStorage.getItem('userId');
    var name = $("#name").val();
    var displayorder = 0;
    var shortdescription = $("#shortDescription").val();
    var fulldescription = $("#fullDescription").val();
    var categories = [{"Id": $('#selectCategory').val()}, {"Id": $('#secondCategory').val()}, {"Id": $('#thirdCategory').val()}];
    var sku = $("#sku").val();
    var price = $("#price").val();
    var oldprice = $("#oldprice").val() || price;
    // var picArray = [];
    // $("#uploadImgBox").find('img').each(function () {
    //     var pic = $(this).attr('src').split(",")[1];
    //     var picJson = {'PictureBinaryBase64': pic, 'SeoFileName': 'picture.jpg'};
    //     picArray.push(picJson);
    // });

    //cut video
    var cut = $('#cutVideoCheck').prop('checked');
    if (cut) {
        var startime = $('.slider-input').val().split(',')[0];
        var endtime = $('.slider-input').val().split(',')[1];
        var PreviewStartTime = startime;
        var PreviewDuration = endtime - startime;
    }

    var showH = $("#showHomepage").prop('checked');
    var showT = $("#enableTweebaa").prop('checked');
    var TweebaaProduct = null;
    if (showT) {
        var SuggestedPrice = $('#SuggestedRetailPrice').val();
        var WholesalePrice = $('#WholesalePrice').val();
        var TycoonComment = $('#tycoonComment').val();
        TweebaaProduct = {
            "SuggestedRetailPrice": SuggestedPrice,
            "WholesalePrice": WholesalePrice,
            "TycoonComment": TycoonComment
        }
    }
    //shipping信息
    var shippingMethods = [];
    if (proType === 'physical') {
        var quantity = $("#quantity").val();
        $('#shippingTable').find('tr').each(function () {
            var countryId = $(this).find('.shipping-table-country').data('id').Id;
            var countryText = $(this).find('.shipping-table-country').text();
            var shippingMethod = $(this).find('.shipping-table-method').text();
            var fee = $(this).find('.shipping-table-fee').text();
            var shipping = {
                'Country': {"Id": countryId, 'Name': countryText},
                'ShippingMethod': shippingMethod,
                'ShippingFee': fee
            };
            shippingMethods.unshift(shipping);
        });
    }

    if (!name) {
        messages($.i18n.prop('productNameEmpty'), 'error', 'top', 3); //'product name can not be empty!'
        return false;
    }
    if (!price) {
        messages($.i18n.prop('productPriceEmpty'), 'error', 'top', 3); //'product name can not be empty!'
        return false;
    }
    if(picArray.length===0){
        messages($.i18n.prop('noPicError'), 'error', 'top', 3); //'product name can not be empty!'
        return false;
    }
    if(!$('#getVideo').val()){
        messages($.i18n.prop('noProductError'), 'error', 'top', 3); //'product name can not be empty!'
        return false;
    }
    loading('show');
    var token = localStorage.getItem('access_token');
    var url = getBaseUrl() + "api/users/" + userId + "/products/" + proType;
    var method = "POST";
    var contentTypeStr = "application/json";
    var prodObj = {
        "Name": name,
        "Sku": sku,
        "Quantity": quantity,
        "Price": price,
        "OldPrice": oldprice,
        "DisplayOrder": displayorder,
        "ShortDescription": shortdescription,
        "FullDescription": fulldescription,
        "Customer": {"Id": userId},
        "Categories": categories,
        'ShippingMethods': shippingMethods,
        "ShowOnHomePage": showH,
        "Pictures": picArray,
        "TweebaaProduct": false
    };
    // console.log(url);
    // console.log(JSON.stringify(prodObj));
    $.when(postApi(url, method, prodObj, token, contentTypeStr)).done(function (data) {
        //console.log(JSON.stringify(data));
        loading('hide');
        var productId = data.Id;
        if (productId) {
            if ($('#getVideo').val()) { //如果有视频则上传视频
                uploadVideo(productId, userId, 'product', cut, PreviewStartTime, PreviewDuration);
            } else {
                confirmBack();
            }
        } else {
            messages($.i18n.prop('systemError'), 'error', 'top', 3); //'System error,please try again!'
        }
    });
}

//填写价格
$('#price').on('input',function () {
    $('#productPrice').html('<h4><span class="price-TAGs"></span> '+ $(this).val()+'</h4>');
    changePriceType();
});
$('#oldprice').on('input',function () {
    $('#priceBox').find('p').removeClass('d-none');
    $('#productOldPrice').html( $(this).val());
    changePriceType();
});

//textarea 自动高度
autosize(document.querySelectorAll('textarea'));
$('#fullDescription').on('input', function () {
    var len = $(this).val().length;
    $('.text-count').attr('data-count', len);
});

//设置图标预览框的高度
var imgW = $('#width').width();
$('#uploadImgBox .ui-col,#uploadVideoBox .ui-col').height(imgW);

function back(){
    confirmBox($.i18n.prop('exitNow'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error');
    $('.dialog-box').find('button').on('click',function (e) {
        var index = $(this).attr('id');
        $('.dialog-box').remove();
        if (index === 'btn-2') {
            confirmBack();
        } else {
            return false;
        }
    })
}
//上传产品提示
function uploadTips() {
    var content = 'uploadTips';
    openNewUserTips(content);
}
function confirmBack() {
    // localStorage.setItem('openFrame', '');
    api.closeWin({
        name: 'addProduct',
    });
}