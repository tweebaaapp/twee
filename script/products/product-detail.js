apiready = function () {
    var prodId = api.pageParam.prodId;
    var countryCode = api.pageParam.countryCode;
    var username = localStorage.getItem('userName');
    if(username){
        var db = api.require('db');
        var ret = db.openDatabaseSync({
            name: username
        });
    }
    var productDetail = new Vue({
        el: '#productDetail',
        mounted: function () {
            this.getProductDetail(prodId);
            this.getShoppingCartCount();
            this.getFavorite();
        },
        data: {
            userInfo: JSON.parse(api.pageParam.userInfo),
            productData: '',
            switchImg: true,
            shoppingCartCount: 0,
            myID: localStorage.getItem('userId'),
            countryCode: countryCode,
            viewerId:localStorage.getItem('userId') || 1,
        },
        methods: {
            getProductDetail: function (prodId) {
                var that = this;
                // var url = getBaseUrl(countryCode) + "api/users/" + this.userInfo.Id + "/products/" + prodId;
                var url = getBaseUrl(countryCode) + "api/users/" + this.viewerId + "/products/" + prodId;
                getAxois(url, "GET", {}).then(function (response) {
                    that.productData = response.data;
                    //console.log(JSON.stringify(response.data));
                    //var server = countryCode === 'CN' ? 'cn' : 'com';
                    var ShareUrl = 'https://tweebaa.cn/html/' + lan + '/mproductdetails.html?userId=' + that.userInfo.Id + '&id=' + that.productData.Id + '&server=cn';
                    $('#shareURL').attr("data-shareUrl", ShareUrl);
                    //console.log(url);
                    setTimeout(function () {
                        initialize();
                        changePriceType(countryCode);
                    });
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            switchImgVideo: function (type) {
                this.switchImg = type === 'img';
                initialize();
            },
            addShoppingCart: function () {
                var token = localStorage.getItem('access_token');
                var localCode = localStorage.getItem('registerCountryCode') === 'CN' ? 'CN' : 'GL';
                var usedServer = countryCode === 'CN' ? 'CN' : 'GL';
                var that = this;
                //console.log(localCode +' ### '+usedServer);
                if (token) {
                    if (localCode !== usedServer) {
                        messages($.i18n.prop('canNotBuyThis'), 'error', 'top', 3);
                        return false;
                    }
                    var userId = localStorage.getItem('userId');
                    var storeId = this.userInfo.StoreId;
                    var productId = this.productData.Id;
                    var paramStr = {"StoreId": storeId, "ProductId": productId, "Quantity": 1};
                    var url = getBaseUrl() + "api/users/" + userId + "/shopping-cart";
                    loading('show');
                    getAxois(url, "POST", paramStr).then(function (response) {
                        that.getShoppingCartCount();
                    }).catch(function (error) {
                        console.log(JSON.stringify(error));
                        messages($.i18n.prop('systemError'), 'error', 'top', 3);
                    });
                } else {
                    openLogin();
                }
            },
            getShoppingCartCount: function () {
                var that = this;
                var userId = localStorage.getItem('userId');
                var url = getBaseUrl() + "api/users/" + userId + "/shopping-cart/count";
                var token = localStorage.getItem('access_token');
                if (token) {
                    getAxois(url, "GET", {}).then(function (response) {
                        // console.log(JSON.stringify(response));
                        that.shoppingCartCount = response.data;
                        loading('hide');
                    }).catch(function (error) {
                        console.log(JSON.stringify(error));
                        messages($.i18n.prop('systemError'), 'error', 'top', 3);
                    });
                }
            },
            chatToUser: function () {
                var chatID = localStorage.getItem('chatID');
                if (chatID) {
                    var userinfo = this.userInfo;
                    var userId = userinfo.UserName + '@new';
                    chatTo(userId, userinfo.StoreName, userinfo.AvatarUrl, 'PRIVATE', false)
                } else {
                    openLogin();
                }
            },
            favorite:function(){
                var token = localStorage.getItem('access_token');
                if (token) {
                    var that = this;
                    var userId = this.userInfo.Id;
                    var prodImg = this.productData.Pictures[0].DownloadUrl;
                    var prodName = this.productData.Name;
                    var icon = $('#favoriteIcon');
                    if(icon.hasClass('active')){
                        icon.removeClass('active');
                        var cleanKey = db.selectSqlSync({
                            name: username,
                            sql: 'DELETE FROM favorite WHERE prodId ='+prodId
                        });
                    }else{
                        icon.addClass('active');
                        var insertData = db.executeSqlSync({
                            name: localStorage.getItem('userName'),
                            sql: "INSERT INTO favorite(prodId,userId,prodImg,prodName,userInfo) VALUES("+ prodId +","+ userId+",'"+ prodImg +"','"+ prodName +"','"+ JSON.stringify(that.userInfo)+"')"
                        });
                    }
                }else{
                    openLogin();
                }

            },
            getFavorite:function () {
                if(username){
                    var showFavorite = db.selectSqlSync({
                        name: localStorage.getItem('userName'),
                        sql: 'SELECT * FROM favorite'
                    });
                    if(showFavorite.status){
                        var f = showFavorite.data;
                        var len =f.length;
                        for(i = 0; i<len ; i++){
                            if(prodId == f[i].prodId){
                                $('#favoriteIcon').addClass('active');
                            }
                        }
                    }else{
                        var createTable = db.executeSqlSync({
                            name: username,
                            sql: "CREATE TABLE favorite(prodId INT PRIMARY KEY,userId INT,prodImg text,prodName text,userInfo text);"
                        });
                    }
                }
                //console.log(JSON.stringify(showFavorite));
            }
        }
    });

};
//使用内置数据库来存储搜藏信息
//初始化图片显示
function initialize() {
    var mySwiper = new Swiper('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        initialSlide: 0,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets'
        }
    });
    var winWidth = api.winWidth;
    $('#proDetailPhoto').height(winWidth);
    $('.videoDisplay').height(winWidth - 36);
}

function back() {

    //localStorage.setItem('openFrame', 'tycoonplace');
    api.closeWin({
        name: 'productDetail',
        animation: {
            type: "reveal",
            subType: "from_left",
            duration: 300
        }
    });
}
