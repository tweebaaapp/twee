
apiready = function () {
    var prodId = api.pageParam.prodId;
    var userId = api.pageParam.userId;
    var useServer = api.pageParam.useServer ==='cn'?'CN':'COM';
    var productDetail = new Vue({
        el: '#shareProduct',
        mounted: function () {
            this.getShoppingCartCount();
            this.getProductDetail(prodId);
            this.getUserDetails();
        },
        data: {
            userInfo : '',
            productData: '',
            switchImg:true,
            shoppingCartCount:0,
            myID: localStorage.getItem('userId')||'',
            useServer:useServer
        },
        methods:{
            getUserDetails: function () {
                loading('show');
                var that = this;
                var url = getBaseUrl() + "/api/users/"+ userId +"?viewerId="+this.myID;
                console.log(JSON.stringify(url));
                getAxois(url, "GET", {}).then(function (response) {
                    console.log(JSON.stringify(response.data));
                    loading('hide');
                    that.userInfo = response.data;
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '@@@@@@@@@@@@@@@@');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            getProductDetail: function (prodId) {
                var that = this;
                var url = getBaseUrl(useServer) + "api/users/"+ userId +"/products/" + prodId;
                getAxois(url,"GET",{}).then(function (response) {
                    that.productData = response.data;
                    setTimeout(function () {
                        initialize();
                        changePriceType(useServer);
                    });
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            switchImgVideo:function (type) {
                this.switchImg = type === 'img';
                initialize();
            },
            addShoppingCart:function () {
                var token = localStorage.getItem('access_token');
                var localCode = localStorage.getItem('registerCountryCode') ==='CN'?'CN':'GL';
                var usedServer = useServer ==='CN'?'CN':'GL';
                var that  = this;
                //console.log(localCode +' ### '+usedServer);
                if(token){
                    if(localCode !== usedServer){
                        messages($.i18n.prop('canNotBuyThis'),'error','top',3);
                        return false;
                    }
                    var userId =  localStorage.getItem('userId');
                    var storeId = this.userInfo.StoreId;
                    var productId = this.productData.Id;
                    var paramStr = {"StoreId" : storeId, "ProductId" : productId ,"Quantity" : 1 };
                    var url = getBaseUrl() + "api/users/"+ userId +"/shopping-cart";
                    loading('show');
                    getAxois(url,"POST",paramStr).then(function (response) {
                        that.getShoppingCartCount();
                    }).catch(function (error) {
                        console.log(JSON.stringify(error));
                        messages($.i18n.prop('systemError'), 'error', 'top', 3);
                    });
                }else{
                    openLogin();
                }
            },
            getShoppingCartCount:function () {
                var that  = this;
                var userId =  localStorage.getItem('userId');
                var url = getBaseUrl() + "api/users/"+ userId +"/shopping-cart/count";
                getAxois(url,"GET",{}).then(function (response) {
                    //console.log(JSON.stringify(response));
                    that.shoppingCartCount = response.data;
                    loading('hide');
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            chatToUser:function () {
                var chatID = localStorage.getItem('chatID');
                if(chatID){
                    var userinfo = this.userInfo;
                    var userId = getApi() === 'cn' ? userinfo.UserName + '@China' : userinfo.UserName + '@International';
                    chatTo(userId, userinfo.StoreName, userinfo.AvatarUrl, 'PRIVATE', false, 'tycoonplace')
                }else{
                    openLogin();
                }
            }
        }
    });
};
//初始化图片显示
function initialize() {
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        initialSlide :0 ,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets'
        }
    });
    var winWidth = api.winWidth;
    $('#proDetailPhoto').height(winWidth);
    $('.videoDisplay').height(winWidth - 36);
}

function back() {

    //localStorage.setItem('openFrame', 'tycoonplace');
    api.closeWin({
        name: 'productDetail',
    });
}
