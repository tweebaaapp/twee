
apiready = function () {
    var momentId =  767;
    // var momentId = api.pageParam.prodId || 767;
    // var userId = api.pageParam.userId || 75296;
    var userId =  75296;
    // var useServer = api.pageParam.useServer ==='cn'?'CN':'COM';
    var useServer = 'COM';
    var sharePost = new Vue({
        el: '#sharePost',
        mounted: function () {
            this.getMomentDetail(momentId);
            this.getUserDetails();
        },
        data: {
            userInfo : '',
            momentData: [],
            myID: localStorage.getItem('userId')||'',
            useServer:useServer
        },
        methods:{
            getUserDetails: function () {
                loading('show');
                var that = this;
                var url = getBaseUrl() + "/api/users/"+ userId +"?viewerId="+this.myID;
                // console.log(JSON.stringify(url));
                getAxois(url, "GET", {}).then(function (response) {
                    console.log(JSON.stringify(response.data));
                    loading('hide');
                    that.userInfo = response.data;
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '@@@@@@@@@@@@@@@@');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            getMomentDetail: function (momentId) {
                var that = this;
                var url = getBaseUrl(useServer) + "api/moments/" + momentId;
                getAxois(url,"GET",{}).then(function (response) {
                    //console.log(JSON.stringify(response.data));
                    that.momentData = response.data;
                    setTimeout(function () {
                        setPost();
                        loading('hide');
                    })
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            showImages:function(event,Index){
                var that = event.target;
                var pic = $(that).closest('.moment-pictures').find('.moment-pic-box');
                pic.each(function () {
                    var imgUrl = $(this).attr('data-img');
                    var img = '<img class="img-fluid" id ="' + Index + '" src=' + imgUrl + ' alt="image">';
                    $('#showImages .swiper-wrapper').append('<div class="swiper-slide"><div class="swiper-zoom-container">' + img + '</div></div>');
                });
                $('#showImages').fadeIn();
                var mySwipershowImages = new Swiper('#showImages', {
                    direction: 'horizontal',
                    loop: false,
                    zoom: true,
                    initialSlide: Index
                });
                $('.close-swiper').on('click', function () {
                    $('#showImages').fadeOut();
                    $('#showImages .swiper-wrapper').empty();
                    mySwipershowImages.destroy(false);
                })
            },
        }
    });
};


function back() {

    //localStorage.setItem('openFrame', 'tycoonplace');
    api.closeWin({
        name: 'shareMoment',
    });
}
