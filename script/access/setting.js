//首页设置
function openFrameHome(){
    localStorage.setItem('openFrame', 'home');
    api.openFrame({
        name: 'home',
        bounces:false,
        allowEdit:true,
        url: '../access/homepage-setting.html',
        animation:{
            type:"movein",
            subType:"from_right"
        }
    });
}

$('#settingSubmitButton').on('click',function () {
  var id = $('.bounce-box-left').attr('id');
  if(id ==='changetycoonName'){ //区分是哪个form提交
    $('#changeStorenameFrom').submit();
  }else if (id ==='changepassword'){
    $('#changePasswordFrom').submit();
  }else if (id ==='changeLanguage'){
    $('#changeLanguageFrom').submit();
  }else if (id ==='changeFontSize'){
    $('#changeFontSizeFrom').submit();
  }
});
//更改密码
$("#changePasswordFrom").submit(function (event) {
  event.preventDefault();
  var userName = localStorage.getItem('userName');
  var oldPassword     = $('#oldPassword').val();
  var newPassword     = $('#newPassword').val();
  var confirmPassword = $('#confirmPassword').val();
  var token    = localStorage.getItem('access_token');
  var url      = getUASApi()+"admin/changepwd?lang="+lan;
  var method   = "POST";
  var contentTypeStr = "application/json";
  var prodObj = {
    "UserName": userName,
    "CurrentPassword": oldPassword,
    "NewPassword": newPassword,
  };
    console.log(JSON.stringify(prodObj));
  if(oldPassword ===''){
    messages($.i18n.prop('currentPasswordEmpty'), 'error', 'top', 2);//'Current Password can not be empty'
    return false;
  }
  else if(newPassword.length < 6){
    messages( $.i18n.prop('password6'), 'error', 'top', 2);//'Current Password can not be empty'
    return false;
  }
  else if(confirmPassword !== newPassword){
    messages($.i18n.prop('passwordNotMatch'), 'error', 'top', 2); //'New password and confirm password do not match'
    return false;
  }else{
    $.when(postApi(url, method, prodObj, token,contentTypeStr,'node')).done(function (data) {
      console.log(JSON.stringify(data));
      if(!data.success){
        messages(data.message, 'error', 'top', 2); //'Current password not correct!'
      }else{
        messages($.i18n.prop('passwordChanged'), 'success', 'top', 2); //'Your password has been changed!'
        setTimeout(function () {
          localStorage.clear();
            api.rebootApp();
        },2000)
      }
    });
  }

});
//更改用户名
$("#changeStorenameFrom").submit(function (event) {
  event.preventDefault();
  var newStorename = $('#newStorename').val();
  if(newStorename ===''){
    messages($.i18n.prop('tycoonnameEmpty'), 'error', 'top', 2);
    return false;
  }
  $.when(checkStorename(newStorename)).done(function (data) {
    if (!data) {
      messages($.i18n.prop('tycoonnameUsed'), 'error', 'top', 2);
      return false;
    } else {
      var userId = localStorage.getItem('userId');
      var newStorename = $('#newStorename').val();
      var token = localStorage.getItem('access_token');
      var method = "PUT";
      var paramStr = {"userid": userId, "storename": newStorename};
      var url = getBaseUrl() + "api/Account/users/" + userId + "/store/name?storename=" + newStorename;

      var contentTypeStr = "application/json";

      $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        //console.log(JSON.stringify(data));
        if(data.status ===200){
          var u = localStorage.getItem('chatID');
          var param = {"itemStr": u, "nameStr": newStorename};
          var met = "POST";
          var urL= 'https://api.tweebucks.com/tweebaa/updateStoreName';
          var key = 'IM';
          $.when(postApi(urL, met, param, key, contentTypeStr)).done(function (data) {
            messages($.i18n.prop('tycoonNameChanged'), 'success', 'top', 2); //'Tycoonplace name changed'
            localStorage.setItem('storename', newStorename);
            setTimeout(function () {
              back();
            },2000)
          });
        }else{
          messages($.i18n.prop('systemError'), 'error', 'top', 2);
        }
      });
    }
  });
});
//更改语言
$('#languageList').find('#'+lan).siblings().attr('selected','selected');
$('#changeLanguageFrom').submit(function (event) {
  event.preventDefault();
  localStorage.setItem('languageSet', $('#languageList').val());
  hideBounceboxLeft();
  messages($.i18n.prop('languageChanged'), 'success','top',2);
  setTimeout(function () {
    api.rebootApp();
  },2000);
});

//更改字号
function FontSize() {
  var size = parseInt(localStorage.getItem('chatFontSize') )|| 14;
  $('#setting-chat-text').find('span').css('font-size',size);
  $('#fontSize').text(size);
  return {
    re: function (type) {
      if(type === 'plus'){
        if(size < 20){
          size += 2;
          return size;

        }else {
          size = 20;
          return 20;
        }

      }else {
        size -= 2;
        if(size >10 ){
          return size;
        }else{
          size =10;
          return 10;
        }

      }
    }
  }
}

var size = FontSize();
$('#sizePlusBtn').on('click',function () {
  var s = size.re('plus');
  $('#setting-chat-text').find('span').css('font-size',s);
  $('#chatFontSize').val(s);
  $('#fontSize').text(s);
});
$('#sizeMinusBtn').on('click',function () {
  var s = size.re('minus');
  $('#setting-chat-text').find('span').css('font-size',s);
  $('#chatFontSize').val(s);
  $('#fontSize').text(s);
});

$('#changeFontSizeFrom').submit(function (event) {
  event.preventDefault();
  localStorage.setItem('chatFontSize', $('#chatFontSize').val());
  hideBounceboxLeft();
  messages($.i18n.prop('fontSizeChanged'), 'success','top',2);
});

