//welcome page
//载入welcome page

apiready = function () {
  var thirdObj = api.pageParam.thirdLogin;
  $('#thirdObj').data('thirdObj', thirdObj);
  welcome(thirdObj);
  setTimeout(function () {
    $('#nextButton').removeClass('d-none');
  },1000);
  var location = $("input[name='loginServer']:checked").val();
  console.log(location);

};
var u =true;
var s =true;
//检查用户名
function welcome(thirdObj) {
  var userName = thirdObj.username;
  var filterUserName = filterStr(userName);
  $('.welcome-userName').text(filterUserName);
  $('.welcome-storeName').text(userName);
  $('.welcome-url').text(filterUserName + getTycoonUrl());
  $('#changeUsername').val(filterUserName);
  $('#changeStorename').val(userName);

  if (filterUserName !== '') {
    checkUsernameWelcome(filterUserName);
  } else {
    messages($.i18n.prop('usernameEmpty'), 'error', 'top', 2); //'User name cannot be empty'
    u = false;
  }
  checkStorenameWelcome(userName);
  //console.log("s="+s+' u='+u);
}

function checkUsernameWelcome(filterUserName) {
  $.when(checkUsername(filterUserName)).done(function (data) {
    if (!data) {
      messages($.i18n.prop('usernameUsed'), 'error', 'top', 2); //'Duplicate Username'
      $('.edit-usename').addClass('text-red');
      u = false;
      $('#error-info').text($.i18n.prop('usernameUsed')+', '+$.i18n.prop('pleaseChange'));
    } else {
      $('.edit-usename').removeClass('text-red');
      $('#error-info').text('');
      u = true;
    }
    console.log("s="+s+' u='+u);
  });
}
function checkStorenameWelcome(storename) {
  var s = storename|| $('.welcome-storeName').text();
  $.when(checkStorename(s)).done(function (data) {
    if (data == false) {
      messages($.i18n.prop('tycoonnameUsed'), 'error', 'top', 2); //'Duplicate Store name'
      $('.edit-storeName').addClass('text-red');
      s = false;
      $('#error-info').text($.i18n.prop('tycoonnameUsed')+','+$.i18n.prop('pleaseChange'));
    } else {
      $('.edit-storeName').removeClass('text-red');
      $('#error-info').text('');
      s = true;
    }
    //console.log("s="+s+' u='+u);
  });
}

//执行注册程序

$('#nextButton').on('click', function () {
  //console.log("s="+s+' u='+u);
  var thirdObj = $('#thirdObj').data('thirdObj');
  var username = $('.welcome-userName').text();
  var storename = $('.welcome-storeName').text();
  $('#error-info').text('');
  if(username === ''){
    messages($.i18n.prop('usernameEmpty'), 'error', 'top', 2);
    return false
  }else if(storename === ''){
    messages($.i18n.prop('tycoonnameEmpty'), 'error', 'top', 2);
    return false
  }
  if(u === false){
    messages($.i18n.prop('usernameUsed'), 'error', 'top', 2); //'Duplicate Username'
    $('.edit-usename').addClass('text-red');
    $('#error-info').text($.i18n.prop('usernameUsed')+','+$.i18n.prop('pleaseChange'));
    return false
  }else if(s === false){
    messages($.i18n.prop('tycoonnameUsed'), 'error', 'top', 2); //'Duplicate Store name'
    $('.edit-storeName').addClass('text-red');
    $('#error-info').text($.i18n.prop('tycoonnameUsed')+','+$.i18n.prop('pleaseChange'));
    return false
  }else if( u=== true && s === true){
    var paramStr = {
      storename: storename,
      ExternalIdentifier: thirdObj.id,
      Email: thirdObj.email,
      Password: md5(thirdObj.id + thirdObj.passwordType + thirdObj.ProviderSystemName),
      passwordType: thirdObj.passwordType,
      ExternalDisplayIdentifier: username,
      PasswordFormatId: 0,
      OAuthToken: thirdObj.token,
      OAuthAccessToken: thirdObj.token,
      ProviderSystemName: thirdObj.ProviderSystemName
    };
    var url = getBaseUrl() + "api/Account/ThirdPartRegister";
    var method = "POST";
    $.when(postApi(url, method, paramStr, null, null)).done(function (data) {
      var regsuccess = data.Registered;
      if (regsuccess) {
          thirdPartyLogin(thirdObj);//登陆
      }else {
        messages($.i18n.prop('emailUsed'), 'error', 'top', 3); // Email has been used!
        setTimeout(function () {
          api.closeFrame({
            name: 'welcome'
          });
        },3000)
      }
    });
  }

});

//辅助显示
$('.edit-usename').on('click',function () {
  $('.edit-usename').hide();
  $('#changeUsername').fadeIn().focus();
  $('#usernameInfo').fadeIn();
});

$('#changeUsername').on('blur',function () {
  // $(this).hide();
  // $('.edit-usename').fadeIn();
  var username = $(this).val();
  checkUsernameWelcome(username);
});

$('.edit-storeName').on('click',function () {
  $('.edit-storeName').hide();
  $('#changeStorename').fadeIn().focus();
});

$('#changeStorename').on('blur',function () {
  // $(this).hide();
  // $('.edit-storeName').fadeIn();
  checkStorenameWelcome();
});

$('#changeUsernameButton').on('click', function () {
  $('#changeUsernameButton-group').addClass('d-none');
  $('#changeUsernameInput').removeClass('d-none');
});

$('#changeUsername').on('input', function () {
  var u = $(this).val();
  var filterUserName = filterStr(u);
  $(this).val(filterUserName);
  $('.welcome-userName').text(filterUserName);
  $('.welcome-url').text(filterUserName + getTycoonUrl());
});
$('#changeStorename').on('input', function () {
  var u = $(this).val();
  $('.welcome-storeName').text(u);
});