//get thirdPartyConnect
function getConnectList() {
  loading('show');
  $('.dialog-box').remove();
  var userId = localStorage.getItem('userId');
  var token = localStorage.getItem('access_token');
  var paramStr = {'customerId': userId};
  var url = getBaseUrl() + "api/Account/GetSocialMediaBoundList";
  var method = "GET";
  $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
    loading('hide');
    $('.container').removeClass('invisible');
    var len = data.length;
    for (var i = 0; i < len; i++) {
      switch (data[i].ProviderSystemName) {
        case "we":
          $('.bound').find('.we').removeClass('d-none');
          $('.bind').find('.we').addClass('d-none');
          break;
        case "fa":
          $('.bound').find('.fa').removeClass('d-none');
          $('.bind').find('.fa').addClass('d-none');
          break;
        case "gl":
          $('.bound').find('.gl').removeClass('d-none');
          $('.bind').find('.gl').addClass('d-none');
          break;
      }
    }
    console.log(JSON.stringify(data));
  });
}


function wechatConnect() {
  confirmBox($.i18n.prop('wantConnect'), $.i18n.prop('cancel'), $.i18n.prop('sure'), 'success'); //'Do you want delete this moment?'
  $('.dialog-box').find('button').on('click', function (e) {
    var index = $(this).attr('id');
    if (index === 'btn-2') {
      $('.dialog-box').remove();
      loading('show');
      var wx = api.require('wx');
      wx.auth(function (ret, err) {
        var weChatCode = ret.code;
        var weChatToken = api.require('wx');
        weChatToken.getToken({
          code: weChatCode
        }, function (ret, err) {
          var weChatAccessToken = ret.accessToken;
          var weChatOpenId = ret.openId;
          var weChatUserInfo = api.require('wx');
          weChatUserInfo.getUserInfo({
            accessToken: weChatAccessToken,
            openId: weChatOpenId
          }, function (ret, err) {
            var weChatUserId = ret.unionid;
            var weChatUserName = ret.nickname;
            var weChatEmail = weChatAccessToken + '@weChat.com';
            var thirdObj = {
              "id": weChatUserId,
              "username": weChatUserName,
              "email": weChatEmail,
              "token": weChatAccessToken,
              "oAuthAccessToken": weChatAccessToken,
              "passwordType": 3,
              "ProviderSystemName": 'we'
            };
            var hasToken = checkTokenValid(weChatToken);
            if (hasToken) {
              thirdConnect(thirdObj);
            }
          });
        });
        if (err) {
          loading('hide');
          console.log(JSON.stringify(err));
        }
      });
    } else {
      loading('hide');
      $('.dialog-box').remove();
    }

  });

}

function facebookConnect() {
  confirmBox($.i18n.prop('wantConnect'), $.i18n.prop('cancel'), $.i18n.prop('sure'), 'success'); //'Do you want delete this moment?'
  $('.dialog-box').find('button').on('click', function (e) {
    var index = $(this).attr('id');
    if (index === 'btn-2') {
      $('.dialog-box').remove();
      loading('show');
      var userLogin = api.require('facebook');
      userLogin.login({
        permissions: 'email'
      }, function (ret, err) {
        console.log(JSON.stringify(err));
        if (ret.status) {
          var facebookToken = api.require('facebook');
          facebookToken.getCurrentToken(function (ret) {

            if (ret.isLogin) {
              var facebookAccessToken = ret.token;
              var userInfo = api.require('facebook');
              userInfo.getUserInfo(function (res, err) {

                var facebookUserId = res.result.id;
                var facebookUserEmail = res.result.email;
                var facebookUserName = res.result.name;
                var facebookToken = api.require('facebook');

                var thirdObj = {
                  "id": facebookUserId,
                  "username": facebookUserName,
                  "email": facebookUserEmail,
                  "token": facebookAccessToken,
                  "oAuthAccessToken": facebookAccessToken,
                  "passwordType": 1,
                  "ProviderSystemName": 'fa'
                };
                var hasToken = checkTokenValid(facebookToken);
                if (hasToken) {
                  thirdConnect(thirdObj);
                }
              });

            }
          });
        } else {
          loading('hide');
          console.log("error " + err);
        }
      });
    } else {
      loading('hide');
      $('.dialog-box').remove();
    }

  });

}

function googleConnect() {
  confirmBox($.i18n.prop('wantConnect'), $.i18n.prop('cancel'), $.i18n.prop('sure'), 'success'); //'Do you want delete this moment?'
  $('.dialog-box').find('button').on('click', function (e) {
    var index = $(this).attr('id');
    if (index === 'btn-2') {
      $('.dialog-box').remove();
      loading('show');
      var google = api.require('google');
      google.signIn(function (ret, err) {
        var googleUserId = ret.userInfo.userID;
        var googleUserName = ret.userInfo.profile.name;
        var googleUserEmail = ret.userInfo.profile.email;
        //var googleIdToken = ret.userInfo.authentication.idToken;

        var googleAuth = api.require('google');
        googleAuth.hasAuth(function (ret, err) {
          if (ret.status) {
            var googleToken = api.require('google');
            googleToken.getTokens(function (ret, err) {
              if (ret.status) {
                var googleToken = ret.authentication.idToken;
                var thirdObj = {
                  "id": googleUserId,
                  "username": googleUserName,
                  "email": googleUserEmail,
                  "token": googleToken,
                  "oAuthAccessToken": googleToken,
                  "passwordType": 2,
                  "ProviderSystemName": 'gl'
                };
                var hasToken = checkTokenValid(googleToken);
                if (hasToken) {
                  thirdConnect(thirdObj);
                }

              } else {
                loading('hide');
                console.log(err.code);
              }
            });
          } else {
            loading('hide');
            console.log('未登录');
          }
        });

      });
    } else {
      $('.dialog-box').remove();
      loading('hide');
    }

  });

}

function thirdConnect(thirdObj) {
  loading('show');
  var url = getBaseUrl() + "api/Account/ThirdPartRegisterConnect";
  var method = "POST";
  var paramStr = {
    CustomerId: localStorage.getItem('userId'),
    ExternalIdentifier: thirdObj.id,
    Email: thirdObj.email,
    Password: md5(thirdObj.id + thirdObj.passwordType + thirdObj.ProviderSystemName),
    passwordType: thirdObj.passwordType,
    ExternalDisplayIdentifier: thirdObj.username,
    PasswordFormatId: 0,
    OAuthToken: thirdObj.token,
    OAuthAccessToken: thirdObj.oAuthAccessToken,
    ProviderSystemName: thirdObj.ProviderSystemName
  };
  $.when(postApi(url, method, paramStr, null, null)).done(function (data) {
    loading('hide');
    console.log(JSON.stringify(data));
    if (data.Status === 200) {
      messages($.i18n.prop('connected'), 'success', 'top', 3);
      setTimeout(function () {
        location.reload();
      }, 2500);
    } else {
      messages($.i18n.prop('connectError'), 'error', 'top', 3);
    }
  });
}

//解除绑定
function unConnect(type) {
  confirmBox($.i18n.prop('wantDisconnect'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error'); //'Do you want delete this moment?'
  $('.dialog-box').find('button').on('click',function (e) {
    var index = $(this).attr('id');
    if (index === 'btn-2') {
      $('.dialog-box').remove();
      loading('show');
      var usrId = localStorage.getItem('userId');
      var method = "DELETE";
      var paramStr = {
        CustomerId: usrId,
        ProviderSystemName: type,
      };
      var url = getBaseUrl() + "api/Account/ThirdPartDisconnect?CustomerId="+ usrId +'&ProviderSystemName='+ type ;
      $.when(postApi(url, method, paramStr, null, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        loading('hide');
        if(data.status === 200 ){
          getConnectList();
          location.reload();
        }
      });
    }else{
      $('.dialog-box').remove();
    }
  });

}