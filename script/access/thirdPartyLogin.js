//WeChat Login Function:
function wechatLogin() {
  loading('show');
  var wx = api.require('wx');
  wx.auth(function (ret, err) {
    var weChatCode = ret.code;
    var weChatToken = api.require('wx');
    weChatToken.getToken({
      code: weChatCode
    }, function (ret, err) {
      var weChatDynamicToken = ret.dynamicToken;
      var weChatAccessToken = ret.accessToken;
      var weChatOpenId = ret.openId;
      var weChatUserInfo = api.require('wx');
      weChatUserInfo.getUserInfo({
        accessToken: weChatAccessToken,
        openId: weChatOpenId
      }, function (ret, err) {
        var weChatUserId = ret.unionid;
        var weChatUserName = ret.nickname;
        var weChatEmail = weChatAccessToken + '@weChat.com';
        var thirdObj = {
          "id": weChatUserId,
          "username": weChatUserName,
          "email": weChatEmail,
          "token": weChatDynamicToken,
          "oAuthAccessToken": weChatAccessToken,
          "passwordType": 3,
          "ProviderSystemName": 'we'
        };
        var hasToken = checkTokenValid(weChatAccessToken);
        if (hasToken) {
          localStorage.setItem('loginFrom', 'we');
          thirdLogin(thirdObj);
        } else {
          loading('hide');
          var msg = 'Wechat' + $.i18n.prop('loginError');
          messages(msg, 'error', 'top', 2);
        }
      });
    });
    if (err) {
      if (err.code === 3) {
        loading('hide');
        var msg = $.i18n.prop('noWechat');
        messages(msg, 'info', 'top', 2);
      }
    }
  });
}

//Facebook Login function:
function facebookLogin() {
  loading('show');
  // var userLogout = api.require('facebook');
  // userLogout.logout();
  var userLogin = api.require('facebook');
  userLogin.login({
    permissions: 'email'
  }, function (ret, err) {
    // console.log(JSON.stringify(err));
    // console.log("++++++++++++++");
    // console.log(JSON.stringify(ret));
    if (ret.status) {
      var facebookToken = api.require('facebook');
      facebookToken.getCurrentToken(function (ret) {
        if (ret.isLogin) {
          var facebookAccessToken = ret.token;
          var userInfo = api.require('facebook');
          userInfo.getUserInfo(function (res, err) {
            var facebookUserId = res.result.id;
            var facebookUserEmail = res.result.email;
            var facebookUserName = res.result.name;
            var facebookToken = api.require('facebook');
            var thirdObj = {
              "id": facebookUserId,
              "username": facebookUserName,
              "email": facebookUserEmail,
              "token": facebookAccessToken,
              "oAuthAccessToken": facebookAccessToken,
              "passwordType": 1,
              "ProviderSystemName": 'fa'
            };
            var hasToken = checkTokenValid(facebookToken);
            if (hasToken) {
              localStorage.setItem('loginFrom', 'fa');
              thirdLogin(thirdObj);
              //console.log('//////Facebook Login Info: //////' + JSON.stringify(thirdObj));
            } else {
              loading('hide');
              var msg = 'Facebook' + $.i18n.prop('loginError');
              messages(msg, 'error', 'top', 2);
            }
          });

        }else{
          loading('hide');
        }
      });
    } else {
      loading('hide');
      var msg = $.i18n.prop('loginCancel');
      messages(msg, 'error', 'top', 2);
    }
  });
}

//Google Login:
function googleLogin() {
  loading('show');
  var google = api.require('google');
  google.signIn(function (ret, err) {
    if (err) {
      loading('hide');
      var msg = $.i18n.prop('loginCancel');
      messages(msg, 'error', 'top', 2);
    } else {
      var googleUserId = ret.userInfo.userID;
      var googleUserName = ret.userInfo.profile.name;
      var googleUserEmail = ret.userInfo.profile.email;
      //var googleIdToken = ret.userInfo.authentication.idToken;

      var googleAuth = api.require('google');
      googleAuth.hasAuth(function (ret, err) {
        if (ret.status) {
          var googleToken = api.require('google');
          googleToken.getTokens(function (ret, err) {
            if (ret.status) {
              var googleToken = ret.authentication.idToken;
              var thirdObj = {
                "id": googleUserId,
                "username": googleUserName,
                "email": googleUserEmail,
                "token": googleToken,
                "oAuthAccessToken": googleToken,
                "passwordType": 2,
                "ProviderSystemName": 'gl'
              };
              var hasToken = checkTokenValid(googleToken);
              if (hasToken) {
                localStorage.setItem('loginFrom', 'gl');
                thirdLogin(thirdObj);
              } else {
                loading('hide');
                var msg = 'Google' + $.i18n.prop('loginError');
                messages(msg, 'error', 'top', 2);
              }

            }else{
              loading('hide');
            }
          });
        } else {
          loading('hide');
          var msg = $.i18n.prop('loginCancel');
          messages(msg, 'error', 'top', 2);
        }
      });
    }


  });
}
