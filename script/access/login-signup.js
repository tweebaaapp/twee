//验证token 自动登录

apiready = function () {
    api.setStatusBarStyle({
        style: 'dark',
        color: '#fff'
    });
    var token = localStorage.getItem('access_token');
    // console.log(token);
    var expires = localStorage.getItem('expires');
    if (token) {
        login();
    }
    api.addEventListener({
        name:'swiperight'
    }, function(ret, err){
        back();
    });
//获取当前地点
    $.getJSON("http://ip-api.com/json/?callback=?", function (data) {
        //console.log(JSON.stringify(data));
        localStorage.setItem('countryCode', data.countryCode);
        if (data.country === 'CN') {
            localStorage.setItem('server', 'cn');
        } else {
            localStorage.setItem('server', 'com');
        }

        $('.country-list').find('li').each(function () {
            var text= $(this).attr('data-simplename');
            var code= $(this).find('span').text();
            var country = localStorage.getItem('countryCode');
            $(this).html('<span>'+ text + '</span><i>'+ code +'</i>');
            if(text === country){
                $('.select-country-code-btn').text(code).attr('data-code',text);
            }
        })
    });

};

// 国家列表
$('.select-country-code').css('display', 'flex').on('click',function () {
    $('.country-list').toggle();
});
$('.country-list').find('li').on('click',function () {
    var code = $(this).find('i').text();
    $('.select-country-code-btn').text(code).attr('data-code',$(this).attr('data-simplename'));
    $('.country-list').toggle();
});

// 登陆
function login() {
    loading('hide');
    api.sendEvent({
        name: 'loginInit'
    });
    api.sendEvent({
        name: 'workplaceReload'
    });
    api.closeWin({
        name:'signup'
    });
    api.closeWin({
        name:'login'
    });
    api.closeWin({
        name:'signup2'
    });

}

//第三方登陆检测
function thirdLogin(thirdObj) {
    var location = $("input[name='loginServer']:checked").val();
    localStorage.setItem('server', location);
    var url = getBaseUrl() + "api/Account/CheckThirdPrtUserAvailability";
    var method = "GET";
    var paramStr = {
        ExternalIdentifier: thirdObj.id,
        ProviderSystemName: thirdObj.ProviderSystemName
    };
    $.when(postApi(url, method, paramStr, null, null)).done(function (data) {
        loading('hide');
        var regsuccess = data.Available;
        if (!regsuccess) {
            var Username = data.Username;
            thirdPartyLogin(thirdObj, Username);//登陆
        } else {
            api.openFrame({
                name: 'welcome',
                url: 'welcome.html',
                bounces: false,
                pageParam: {
                    thirdLogin: thirdObj
                }
            });
        }
    });
}

//执行第三方login
function thirdPartyLogin(thirdObj, Username) {
    var password = md5(thirdObj.id + thirdObj.passwordType + thirdObj.ProviderSystemName);
    loginFunction(Username, password);
}

//login
$('#loginForm').submit(function (event) {
    event.preventDefault();
    var userName = $.trim($(this).find('#loginUsername').val());
    var password = $.trim($(this).find('#loginPassword').val());
    var phone = $.trim($(this).find('#phoneNumber').val());
    var callingCode = phone? $('.select-country-code-btn').text().replace('+',''):null;
    if (!password) {
        var msg = $.i18n.prop('passwordEmpty');
        messages(msg, 'error', 'top', 2);
    } else {
        $('.login-signup-btn').addClass(' loading-btn');
        loginFunction(userName, password,callingCode,phone,0);
        $(this).find('#loginPassword').blur();
    }
});

function loginFunction(userName, password,callingCode,phone,t) {
    loading('show');
    var paramStr = {
        "UserName": userName,
        "CountryCode": "CN",
        "Password": password,
        "CountryCallingCode": callingCode,
        "PhoneNumber": phone,
    };
    var url = getUASApi()+"login?lang="+lan;
    var method = "POST";
    var contentType = 'application/json';
    //console.log(JSON.stringify(paramStr));
    $.when(postApi(url, method, paramStr, null, contentType)).done(function (data) {
        var obj = data;
        //console.log(JSON.stringify(data)+"!!!!!!!!!!!");
        if (obj.success) {
            var Url = getUASApi(obj.user.RegisterCountryCode)+"admin/profile";
            var method = "GET";
            var token = obj.token;
            var registerCountryCode = obj.user.RegisterCountryCode;
            // console.log(Url);
            setTimeout(function () {
                $.when(postApi(Url, method, null, token, contentType,'node')).done(function (data) {
                    loading('hide');
                    var user = data;
                    // console.log(JSON.stringify(user));
                    if (user.UserID) {
                        saveLocalStorage(user,token,registerCountryCode);
                    } else {
                        var msg = $.i18n.prop('nameOrPassIncorrect');//"User name or password is incorrect";
                        $('.login-signup-btn').removeClass(' loading-btn');
                        messages(msg, 'error', 'top', 2);
                    }
                });
            },t);

        } else {
            loading('hide');
            var msg = $.i18n.prop('nameOrPassIncorrect');//"User name or password is incorrect";
            $('.login-signup-btn').removeClass(' loading-btn');
            messages(msg, 'error', 'top', 2);
        }
    });
}

//保存信息到localStorage
function saveLocalStorage(obj,token,registerCountryCode) {
    localStorage.setItem('access_token', token);
    localStorage.setItem('userName', obj.UserName);
    localStorage.setItem('userId', obj.UserID);
    localStorage.setItem('storename', obj.UserStoreName);
    localStorage.setItem('expires', obj.expires);
    localStorage.setItem('registerCountryCode', registerCountryCode); //获取注册时国家代码
    localStorage.setItem('wallet', obj.UserWalletAddress); //钱包地址
    localStorage.setItem('email', obj.Email);
    localStorage.setItem('storeId', obj.StoreID);
    setTimeout(function () {
        login();
    }, 1000);
}

//注册

//生成手机验证码
function MathRand() {
    var Num = '';
    for (var i = 0; i < 5; i++) {
        Num += Math.floor(Math.random() * 10);
    }
    localStorage.setItem('phoneVCode', Num);
}

//发送短信
function sendVerifyCode() {
    //console.log(localStorage.getItem('phoneVCode'));
    var code = localStorage.getItem('phoneVCode');
    var phone = $('#phoneNumber').val();
    var countryCode = $('.select-country-code-btn').text().replace('+','');
    if (countryCode !=='86') {
        phone = 'I' + countryCode + phone;
    }
    var url = "https://api.tweebaa.cn/api/SMSApi/SendSMS?Phone=" + phone + "&VerifyCode=" + code + "&LanguageId=1&Key=aaa"; // + Base64.encode(localStorage.getItem('phoneVCode'))
    var paramStr = {
        "Phone": phone,
        "VerifyCode": code,
        "LanguageId": 1,
        "Key": "aaa"
    };
    var method = 'POST';
     console.log(url);
    $.when(postApi(url, method, paramStr, null, null)).done(function (data) {
        //console.log(JSON.stringify(data))
    });
}

//注册提交
$('#signupForm').submit(function (event) {
    event.preventDefault();
    $('.login-signup-btn').addClass(' loading-btn');
    var userName = 'tw'+Date.parse(new Date());
    var storeName = userName;
    var emailAddress = $.trim($(this).find("#email").val());
    var phone = $.trim($(this).find("#phoneNumber").val());
    var password = $.trim($(this).find("#password").val());
    var verifyCode = $('#verifyCode').val();
    var term = $(this).find("#customControlInline").prop("checked");
    var callingCode = phone? $('.select-country-code-btn').text().replace('+',''):null;
    var msg;
    var szRegE = /^[A-Za-z0-9\u4e00-\u9fa5._-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/; //email
    if (userName === '') {
        msg = $.i18n.prop('usernameEmpty');
        errorSubmit();
    } else if (storeName === '') {
        msg = $.i18n.prop('tycoonnameEmpty');
        errorSubmit();
    } else if (emailAddress !=='' && !szRegE.test(emailAddress)) {
        msg = $.i18n.prop('emailError');//'Email format is incorrect';
        errorSubmit();
    } else if (password === '') {
        msg = $.i18n.prop('passwordEmpty');//'Password can not be empty';
        errorSubmit();
    } else if (password.length < 6) {
        msg = $.i18n.prop('password6');//'Password need more then 6 letters or numbers';
        errorSubmit();
    } else if (!term) {
        msg = $.i18n.prop('agreeTweebaa');//'You must agree Tweebaa\'s Terms and policy';
        errorSubmit();
    } else if (phone !=='' && verifyCode !== localStorage.getItem('phoneVCode')) {
        msg = $.i18n.prop('verifyCodeIncorrect');//'Verify Code Incorrect';
        errorSubmit();
    } else if (phone ==='' && emailAddress ==='') {
        msg = $.i18n.prop('emailOrPhone');//'Verify Code Incorrect';
        errorSubmit();
    } else {
        //singupFunction(userName, storeName, emailAddress,phone, password);
        singup2(userName, storeName, emailAddress,phone,callingCode, password)
    }

    function errorSubmit() {
        messages(msg, 'error', 'top', 2);
        $('.login-signup-btn').removeClass(' loading-btn');
        return false;
    }
});
function singup2(userName, storeName, emailAddress,phone, callingCode,password) {
    var paramStr = {
        userName : userName,
        storeName: storeName,
        emailAddress: emailAddress,
        phone:phone,
        callingCode:callingCode,
        password:password
    };
    openWindows('signup2','singup2.html',paramStr);
}
$('#nickname').on('input',function () {
    if($(this).length>0){
        $('#singup2').addClass('active');
    }else{
        $('#singup2').removeClass('active');
    }
});
$('#singup2').on('click',function () {
    if($(this).hasClass('active')){
        var storeName = $('#nickname').val();
        var userName = api.pageParam.userName;
        var emailAddress = api.pageParam.emailAddress;
        var phone = api.pageParam.phone;
        var callingCode = api.pageParam.callingCode;
        var password = api.pageParam.password;
        // console.log("userName: "+userName+'   storeName:'+storeName+"    emailAddress: "+emailAddress+"  phone: "+phone+'  callingCode: '+callingCode+' password:'+password)
        singupFunction(userName, storeName, emailAddress,phone, callingCode,password);
    }

});

function singupFunction(userName, storeName, emailAddress,phone,callingCode, password) {
    loading('show');
    var paramStr = {
        "UserName": userName,
        "StoreName": storeName,
        "Email": emailAddress,
        "Password": password,
        "CountryCallingCode": callingCode,
        "PhoneNumber":phone,
        "RegisterCountryCode":localStorage.getItem('countryCode')
    };
    var url = getUASApi()+'register?lang=' +lan;
    var method = "POST";
    console.log(JSON.stringify(paramStr));
    $.when(postApi(url, method, paramStr, null, 'contentTypeStr')).done(function (data) {
       // console.log(JSON.stringify(data));
        loading('hide');
        if (data.success) {
            loginFunction(userName, password,callingCode,phone,5000);
        } else {
            messages(data.message, 'error', 'top', 2);
        }

    });
}
