var byId = function (e) {
    return document.getElementById(e)
};

function changeIndex() {
    $("#my-ui-list").find("li").each(function (e) {
        $(this).find(".sortOrder").text(e), $(this).find(".set-order").val(e)
    })
}

function getWidgetList() {
    loading("show");
    var e = localStorage.getItem("access_token"), t = localStorage.getItem("userId"),
        n = {customerid: t, currentpage: 1, itemsperpage: 20}, o = getBaseUrl() + "api/users/" + t + "/widgets";
    $.when(postApi(o, "GET", n, e, null)).done(function (e) {
        //console.log(JSON.stringify(e));
        var t = e.length;
        for (i = 0; i < t; i++) {
            var n = e[i].EntityOriginalName, o = e[i].SortOrder,
                a = '<li class="ui-row home-pro-list text-center">\n  ' +
                    ' <div class="ui-col ui-col-10 drag-handle"><i class="icon iconfont icon-other"></i></div>\n   ' +
                    '<div class="ui-col ui-col-10"><span class="h5 sortOrder pr-2">1</span></div>\n   ' +
                    '<div class="ui-col ui-col-67 text-left ui-nowrap">' + n + '</div>\n     ' +
                    '<input type="hidden" data-detail=\'{"Id":' + e[i].Id + ',"EntityName":"' + e[i].EntityName + '","EntityId":' + e[i].EntityId + ',"EntityOriginalName":"' + e[i].EntityOriginalName + '"}\' class="set-order form-control p-0 text-center" value="' + o + '">\n     ' +
                    ' <div class="ui-col ui-col-10 text-right"><i class="icon iconfont icon-empty_fill btn"   data-id="' + e[i].Id + '"></i></div>\n    ' +
                    '</li>';
            $(".homepage-setting-page").prepend(a)
        }
        loading("hide");
        changeIndex();
    })
}

Sortable.create(byId("my-ui-list"), {
    handle: ".drag-handle", group: "words", animation: 150, onEnd: function (e) {
        changeIndex()
    }
}), $("#widgetSaveBtn").on("click", function () {
    loading("show");
    var e = [];
    $("#my-ui-list").find(".set-order").each(function () {
        var t = $(this).data("detail"), i = $(this).val(), n = {Id: t.Id, SortOrder: i};
        e.push(n)
    });
    var t = localStorage.getItem("access_token"), i = localStorage.getItem("userId"),
        n = getBaseUrl() + "api/users/" + i + "/widgets/sortorder";
    $.when(postApi(n, "PUT", e, t, "application/json")).done(function (e) {
        200 === e.status && messages($.i18n.prop("orderHasBeenSaved"), "success", "bottom", 2), loading("hide")
    })
}), $(".homepage-setting-page").on("click", ".btn", function () {
    var e = $(this);
    confirmBox($.i18n.prop("deleteFromHomepage"), $.i18n.prop("cancel"), $.i18n.prop("sure"), "error"), $(".dialog-box").find("button").on("click", function (t) {
        if ("btn-2" !== $(this).attr("id")) return $(".dialog-box").remove(), !1;
        var i = localStorage.getItem("access_token"), n = localStorage.getItem("userId"), o = e.data("id"),
            a = getBaseUrl() + "api/users/" + n + "/widgets/" + o;
        $.when(postApi(a, "DELETE", {}, i, "application/json")).done(function (e) {
            //console.log(JSON.stringify(e));
            200 === e.status && ($(".homepage-setting-page").html(""), getWidgetList(), $(".dialog-box").remove())
        })
    })
});
