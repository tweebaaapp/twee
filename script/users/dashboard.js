apiready = function () {

    api.addEventListener({
        name: 'conversationListReload'
    }, function(ret, err) {
        $('#conversationList').html('');
    });
    api.addEventListener({
        name:'swiperight'
    }, function(ret, err){
        back();
    });

    var baseUrl = getTycoonUrl();
    var user = localStorage.getItem('userName');
    var elText = user + baseUrl;
    var today = moment().format('YYYY-MM-DD');
    var day7 = moment().subtract(6, 'days').format('YYYY-MM-DD');
    //主页点击量显示
    getFinancialData();
    isOnLineStatus();
    // getPoint();
   // getPoint();
    // function pageView() {
    //
    //   var url = "https://report.tycoonplace.com/GARepoting/" + elText + day7 + "/" + today + "/PV ";
    //   console.log(url);
    //   var method = "GET";
    //   $.when(postApi(url, method, null, null, null)).done(function (re) {
    //     console.log(JSON.stringify(re));
    //     // if (re.Data) {
    //     //     var data = [];
    //     //     var rs = re.Data;
    //     //     var d = rs.split("],").join('@').split("@");
    //     //     var len = d.length;
    //     //     for (i = 0; i < len; i++) {
    //     //         var t = moment.utc(eval(d[i].split("),")[0] + ')]')[0]).local().format('MM-DD');
    //     //         var c = d[i].split("),")[1];
    //     //         if (d[len - 1]) {
    //     //             c = c.split("]")[0];
    //     //         }
    //     //         data.push({day: t, value: parseInt(c)});
    //     //     }
    //     //
    //     //     const chart = new F2.Chart({
    //     //         id: 'pageView',
    //     //         width: window.innerWidth,
    //     //         height: window.innerWidth > window.innerHeight ? (window.innerHeight - 54) : window.innerWidth * 0.707,
    //     //         pixelRatio: window.devicePixelRatio
    //     //     });
    //     //
    //     //     chart.source(data, {
    //     //         value: {
    //     //             tickCount: 5,
    //     //             min: 0
    //     //         },
    //     //         day: {
    //     //             range: [0, 1]
    //     //         }
    //     //     });
    //     //     chart.tooltip({
    //     //         showCrosshairs: true,
    //     //         showItemMarker: false,
    //     //         onShow(ev) {
    //     //             const {items} = ev;
    //     //             items[0].name = null;
    //     //             items[0].value = items[0].value;
    //     //         }
    //     //     });
    //     //     chart.axis('day', {
    //     //         label(text, index, total) {
    //     //             const textCfg = {};
    //     //             if (index === 0) {
    //     //                 textCfg.textAlign = 'left';
    //     //             }
    //     //             if (index === total - 1) {
    //     //                 textCfg.textAlign = 'right';
    //     //             }
    //     //             return textCfg;
    //     //         }
    //     //     });
    //     //     chart.line().position('day*value').shape('smooth');
    //     //     chart.point().position('day*value').style({
    //     //         stroke: '#fff',
    //     //         lineWidth: 1
    //     //     });
    //     //     chart.render();
    //     // }
    //
    //   });
    // }

    function contentView() {
        var url = "https://report.tycoonplace.com/GARepoting/" + elText + "/2018-01-01/" + today + "/CV ";
        var method = "GET";
        var homepage = {"uniquePageviews": 0, "avgTimeOnPage": 0};
        var virtual = {"uniquePageviews": 0, "avgTimeOnPage": 0};
        var physical = {"uniquePageviews": 0, "avgTimeOnPage": 0};
        var free = {"uniquePageviews": 0, "avgTimeOnPage": 0};
        var moment = {"uniquePageviews": 0, "avgTimeOnPage": 0};
        loading('show');
        $.when(postApi(url, method, null, null, null)).done(function (re) {
            //console.log(JSON.stringify(re));
            loading('hide');
            if (re.Data) {
                var rs = re.Data;
                var len = rs.length;
                for (i = 0; i < len; i++) {
                    if (rs[i].strUrl === (user + baseUrl)) {
                        homepage = {"uniquePageviews": rs[i].uniquePageviews, "avgTimeOnPage": rs[i].avgTimeOnPage}
                    } else if (rs[i].strUrl === (user + baseUrl + "virtualproducts")) {
                        virtual = {"uniquePageviews": rs[i].uniquePageviews, "avgTimeOnPage": rs[i].avgTimeOnPage}
                    } else if (rs[i].strUrl === (user + baseUrl + "PhysicalProducts/Index")) {
                        physical = {"uniquePageviews": rs[i].uniquePageviews, "avgTimeOnPage": rs[i].avgTimeOnPage}
                    } else if (rs[i].strUrl === (user + baseUrl + "FreeOffers/Index")) {
                        free = {"uniquePageviews": rs[i].uniquePageviews, "avgTimeOnPage": rs[i].avgTimeOnPage}
                    } else if (rs[i].strUrl === (user + baseUrl + "Moment")) {
                        moment = {"uniquePageviews": rs[i].uniquePageviews, "avgTimeOnPage": rs[i].avgTimeOnPage}
                    } else if (rs[i].strUrl === (user + baseUrl + "login")){
                        continue;
                    }else{
                        var pageURL = rs[i].strUrl.split(baseUrl)[1];
                        var template = '<li class="ui-row">\n' +
                            '            <div class="ui-col ui-col-100 ui-nowrap">\n' +
                            '                <span class="small text-dark-gray">'+ $.i18n.prop('page') +'</span>\n' +
                            '                <span class="small" onclick="openBrowser(\''+ rs[i].strUrl +'\')">'+ pageURL +'</span>\n' +
                            '            </div>\n' +
                            '            <div class="ui-col ui-col-50">\n' +
                            '                <span class="small text-dark-gray">'+ $.i18n.prop('pageViews') +'</span>\n' +
                            '                <span class="small text-blue">'+ rs[i].uniquePageviews +'</span>\n' +
                            '            </div>\n' +
                            '            <div class="ui-col ui-col-50">\n' +
                            '                <span class="small  text-dark-gray">'+ $.i18n.prop('AvgTime') +'</span>\n' +
                            '                <span class="small text-green">'+ rs[i].avgTimeOnPage +' s</span>\n' +
                            '            </div>\n' +
                            '        </li>';
                        $('#pageViewList').append(template);
                    }
                }
                var showData = [homepage.uniquePageviews, virtual.uniquePageviews, physical.uniquePageviews, free.uniquePageviews, moment.uniquePageviews];
                //console.log(JSON.stringify(showData));
                var ctx = document.getElementById("myChart").getContext('2d');
                Chart.defaults.global.defaultFontColor = '#000';
                Chart.defaults.global.defaultFontSize = '10';
                var myChart = new Chart(ctx, {
                    type: "horizontalBar",
                    data: {
                        labels: [$.i18n.prop('homepage'), $.i18n.prop('digitalProduct'), $.i18n.prop('physicalProduct'), $.i18n.prop('limitFreeProduct'), $.i18n.prop('moment')], //["Homepage", "Digital", "Physical", "Free Offer", "Moment"]
                        datasets: [{
                            label: '# of Votes',
                            data: showData,
                            backgroundColor: [
                                'rgba(118, 192, 236, 1)',
                                'rgba(118, 192, 236, 0.9)',
                                'rgba(118, 192, 236, 0.8)',
                                'rgba(118, 192, 236, 0.7)',
                                'rgba(118, 192, 236, 0.6)'
                            ]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: $.i18n.prop('contentViews')
                        },
                        legend: false,
                        animation: {
                            duration: 200
                        },
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    color: 'rgba(250, 250, 250, 1)'
                                },
                                ticks:{
                                    autoSkipPadding:1
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    color: 'rgba(250, 250, 250, 0)'
                                }
                            }]
                        }
                    }
                });
            }
        });

    }
    // pageView();

    contentView();
};
function back() {
    localStorage.setItem('openFrame', '');
    api.closeFrame({
        name:'dashboard'
    });
}
function getFinancialData() {
    //console.log("getFinancialData");
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var paramStr = {"customerId": userId};
    var url = getBaseUrl() + "api/RewardPointcheckApi/GetCombineTotalPointSalesCustomersOrders/" + userId;
    var method = "GET";
    var contentTypeStr = "application/json";
    console.log(userId);
    $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        //console.log(JSON.stringify(data));
        if (data) {
            $('#totalSale').text(data.totalsales);
            //$('#totalCustomers').text(data.totalcustomers);
            $('#totalOrders').text(data.totalordercounts);
            //$('#totalPoint').text(data.totalpoints);
        }
        changePriceType();
    });
}

// function getPoint() {
//     var token = localStorage.getItem('access_token');
//     var userId = localStorage.getItem('userId');
//     var paramStr = {};
//     var url = getBaseUrl() + "/api/users/"+ userId +"/points";
//     var method = "GET";
//     $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
//         console.log(JSON.stringify(data));
//         $('#totalPoint').text(data[0].Points);
//     });
// }
