
//获取用户等级和奖励
function getRewards() {
    var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "api/users/" + userId + "/rewards";
    var paramStr = {};
    var token = localStorage.getItem('access_token');
    var method = 'GET';
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
       //console.log(JSON.stringify(data));
        $('#coinsBalance').text(data.Coins);
        //$('.vip-level').text(data.InfluenceLevel);
        localStorage.setItem('rewards',JSON.stringify(data));
    });
}
//获取用户情况
function getOverView() {
    var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "/api/v2/users/profile";
    var paramStr = {
        "ownerId": userId
    };
    var priceType = getApi() === 'cn' ?'￥ ':'$ ';
    var token = localStorage.getItem('access_token');
    var method = 'POST';
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        $('#countPosts').html(data.MomentCount);
        $('#countProducts').html(data.ProductCount);
        $('#countOrders').html(data.SalesCount);
        $('#countSale').html(priceType + data.SalesAmount);
    });
}

//获取用户详细
function getMyDetails() {
    var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "api/users/" + userId;
    var paramStr = {};
    var token = localStorage.getItem('access_token');
    var method = 'GET';
    //console.log(url);
    loading('show');
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        console.log(JSON.stringify(data));
        $('#myPortrait').css('background-image','url('+ data.AvatarUrl +')');
        localStorage.setItem('myCover',data.BackgroundUrl);
        localStorage.setItem('chatPortrait', data.AvatarUrl);
        localStorage.setItem('followersCount', data.FollowersCount);
        localStorage.setItem('followingCount', data.FollowingCount);
        localStorage.setItem('introduction',data.Introduction);
        localStorage.setItem('myTags',JSON.stringify(data.PersonalTags));
        loading('hide');
    });
}

//get TIV
function getTIV() {
    var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "api/users/"+ userId +"/tiv";
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        console.log(JSON.stringify(data));
        if(data){
            $('#tiv').text(data.InfluenceValue);
            $('#vipLevel').text(data.InfluenceLevel.Code + '  ' + data.InfluenceLevel.Name);
            loading('hide');
        }
    });
}
function getCheckInStatus() {
    var date = moment().format('YYYY-MM-DD');
    var userId = localStorage.getItem('userId');
    var method = "GET";
    var url = getBaseUrl() + "api/users/"+ userId +"/rewards/attendance?date=" + date;
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    //console.log(JSON.stringify(url));
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        var nextTime = moment.utc(data.LastAttendanceTime).add(1,'days').toDate();
        var consecutiveDays = data.ConsecutivePresentDays;
        var todayPresent = data.TodayPresent;
        if(todayPresent){
            $('#checkInButton').addClass('active');
            getNextCheckInTime(nextTime);
        }else{
            $('#checkInButton').removeClass('active');
        }
        $('#continuousCheckIn div:nth-child(-n+'+ consecutiveDays +')').find('span.circle-border').addClass('active');
    });
}

function openPages(name) {
    var url ='';
    switch(name){
        case "wallet":
            url = '../users/wallet.html';
            break;
        case "invite":
            url = '../access/invite.html';
            break;
        case "home":
            url = '../access/homepage-setting.html';
            break;
        case "setting":
            url = '../access/setting.html';
            break;
        case "addProduct":
            url = '../products/add-product-new.html';
            break;
        case "myProducts":
            url = '../products/myproducts.html';
            break;
        case "tProduct":
            url = '../products/tweebaaPorducts-new.html';
            break;
        case "orders":
            url = '../products/order-list.html';
            break;
        case "addMoment":
            url = '../users/add-moment.html';
            break;
        case "moments":
            url = '../users/my-post-list.html';
            break;
        case "myProfile":
            url = '../users/my-profile.html';
            break;
    }
    //localStorage.setItem('openFrame', name);
    openWindows(name,url);
}

//显示下次签到倒计时
function getNextCheckInTime(nextTime) {
    var now = moment();
    var duration = moment.duration(nextTime  - now, 'ms');
    if(duration.toString().indexOf('-')=== -1){
        setTimeout(function () {
            var hours = duration.get('hours');
            var mins = duration.get('minutes')>9?duration.get('minutes'):'0'+duration.get('minutes');
            var ss = duration.get('seconds')>9?duration.get('seconds'):'0'+duration.get('seconds');
            var next = hours + ':' + mins + ':' + ss;
            $('#nextTime').text(next);
            getNextCheckInTime(nextTime);
        },1000);
    }else{
        $('#checkInButton').removeClass('active');
    }
}

function checkInBtn(This) {
    if($(This).hasClass('active')){
        return false;
    }
    loading('show');
    var userId = localStorage.getItem('userId');
    var method = "POST";
    var url = getBaseUrl() + "api/users/"+ userId +"/rewards/attendance";
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        console.log(JSON.stringify(data));
        loading('hide');
        getCheckInStatus();
        if(data.status ===200){
            messages($.i18n.prop('checkInSuccessfully'), 'success', 'top', 3);
        }else if(data.status ===400){
            messages($.i18n.prop('checkInError'), 'error', 'top', 3);
        }
    });
}

function myStore() {
    var userName = localStorage.getItem('userName');
    var url = 'www.tweebaa.'+ getApi()+'/tycoonplace/'+ userName ;
    var name = $.i18n.prop('myTycoonPlace');
    openBrowser(url,name);
}

function logout() {
    confirmBox($.i18n.prop('areYouWantLogout'),$.i18n.prop('cancel'),$.i18n.prop('sure'),'error');
    $('.dialog-box').find('button').on('click',function (e) {
        $('.dialog-box').remove();
        var index = $(this).attr('id');
        if (index === 'btn-2') {
            var loginFrom = localStorage.getItem('loginFrom');
            localStorage.clear();
            if(loginFrom === 'gl' ){
                googleLogout();
            }
            else if(loginFrom === 'fa' ){
                facebookLogout();
            }else{
                api.rebootApp();
            }

        } else {
            return false;
        }
    })
}

function facebookLogout() {
    var userLogout = api.require('facebook');
    userLogout.logout();
    api.rebootApp();
}
function googleLogout() {
    var googelLogout = api.require('google');
    googelLogout.signOut();
    api.rebootApp();
}
