apiready = function () {
    api.addEventListener({
        name:'swiperight'
    }, function(ret, err){
        back();
    });
    var type = api.pageParam.type;
    var UserId = api.pageParam.userId;
    var followedList = new Vue({
        el: '#followList',
        mounted: function () {
            this.getFollowList();
        },
        data: {
            listData:[],
            title:$.i18n.prop(type),
            myID:localStorage.getItem('userId')
        },
        methods:{
            getFollowList: function () {
                var that = this;
                var userId = UserId || localStorage.getItem('userId');
                if(this.searchInput !==''){
                    loading('show');
                    var t = type ==='following'?'followees':'followers';
                    var url = getBaseUrl() + "/api/users/"+ userId +"/"+t +"?viewerId=" +this.myID ;
                    getAxois(url,"GET",{}).then(function (response) {
                        console.log(JSON.stringify(response.data));
                        that.listData = response.data;
                        loading('hide');
                    }).catch(function (error) {
                        console.log(JSON.stringify(error));
                        messages($.i18n.prop('systemError'), 'error', 'top', 3);
                    });
                }
            },
            followBtn: function (userId,event,index) {
                var token = localStorage.getItem('access_token');
                if(token){
                    var that = this;
                    var myId = localStorage.getItem('userId');
                    var tar = event.target;
                    var url = getBaseUrl() + "/api/users/"+ myId +"/follow/users/"+ userId;
                    getAxois(url,"PUT",{}).then(function (response) {
                        //console.log(JSON.stringify(response));
                        if(response.statusText==="OK"){
                            $(tar).toggleClass('active');
                            that.listData[index].Followed = !that.listData[index].Followed;
                            if(that.listData[index].Followed){
                                that.listData[index].FollowersCount ++;
                            }else{
                                that.listData[index].FollowersCount --;
                            }
                            api.sendEvent({
                                name: 'reloadFollowing'
                            });
                            api.sendEvent({
                                name: 'profileReload'
                            });
                        }
                    }).catch(function (error) {
                        console.log(JSON.stringify(error));
                        messages($.i18n.prop('systemError'), 'error', 'top', 3);
                    });
                }else{
                    openLogin();
                }
            },
            openTycoonPlace: function (index) {
                openUserTycoonPlace(this.listData[index].Id,this.listData[index].StoreName,this.listData[index].AvatarUrl,'searchUsers')
            }
        }

    });

};
function back() {
    // localStorage.setItem('openFrame', '');
    api.closeWin({
        name: 'followList'
    });
}