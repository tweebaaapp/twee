
var Pages = 0;
apiready = function () {
    //getmonents(mPages, false);
    api.addEventListener({
        name: 'scrolltobottom',
        extra: {
            threshold: 0            //设置距离底部多少距离时触发，默认值为0，数字类型
        }
    }, function (ret, err) {
            Pages++;
            myPostList.getProducts(Pages);
    });

    var myPostList = new Vue({
        el: '#myPostList',
        mounted: function () {
            this.getmonents();
        },
        data: {
            myID: localStorage.getItem('userId'),
            momentsData: [],
        },
        methods: {
            getmonents:function(mPages){
                var that = this;
                var url = getBaseUrl() + "/api/v2/moments";
                var paramStr = {
                    "ownerId": this.myID,
                    "currentpage": mPages,
                    "itemsperpage": 24,
                    "ShowOnHomePage": true,
                };
                loading('show');
                getAxois(url, "POST", paramStr).then(function (response) {
                    loading('hide');
                    //console.log(JSON.stringify(response));
                    if (response.data.Data.length !== 0) {
                        that.momentsData = that.momentsData.concat(response.data.Data);
                    }
                    setTimeout(function () {
                        setPost();
                        setCopy();
                    })
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '#######################');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            deleteMoment:function(id,event){
                var tar = event.target;
                confirmBox($.i18n.prop('wantDeleteMoment'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error'); //'Do you want delete this moment?'
                $('.dialog-box').find('button').on('click',function (e) {
                    var index = $(this).attr('id');
                    $('.dialog-box').remove();
                    if (index === 'btn-2') {
                        loading('show');
                        var userid = localStorage.getItem('userId');
                        var token = localStorage.getItem('access_token');
                        // var contentTypeStr = "application/json";
                        var url = getBaseUrl() + "api/users/" + userid + "/moments/" + id;
                        var method = "DELETE";
                        // var paramStr = {"userId": userid, "momentId": momentId};
                        $.when(postApi(url, method, null, token, null)).done(function (data) {
                            loading('hide');
                            if(data.status === 200){
                                $(tar).closest('li.moment-item').remove();
                            }else{
                                messages($.i18n.prop('systemError'), 'error', 'bottom', 3);
                            }
                        });
                    }
                });
            },
            openShareBar:function(content,id){
                //var server = this.userInfo.RegisterCountryCode ==='CN'?'cn':'com';
                var ShareUrl = 'https://tweebaa.cn/html/'+ lan +'/mblog.html?userId='+ api.pageParam.userId +'&type=blog&id='+ id+'&server=cn';
                $('#shareURL').attr("data-shareUrl", ShareUrl);
                $("#name").text(content);
                new OpenShareBox();
            },
            showImages:function(event,Index){
                var that = event.target;
                var pic = $(that).closest('.moment-pictures').find('.moment-pic-box');
                pic.each(function () {
                    var imgUrl = $(this).attr('data-img');
                    var img = '<img class="img-fluid" id ="' + Index + '" src=' + imgUrl + ' alt="image">';
                    $('.swiper-wrapper').append('<div class="swiper-slide"><div class="swiper-zoom-container">' + img + '</div></div>');
                });
                $('.swiper-container').fadeIn();
                var mySwiper = new Swiper('.swiper-container', {
                    direction: 'horizontal',
                    loop: false,
                    zoom: true,
                    initialSlide: Index,
                    on: {
                        init: function(){
                            $('#downloadImage').val($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                        transitionEnd: function(){
                            $('#downloadImage').val($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                    },
                });
                addListenerForDownloadImg();
                $('.close-swiper').on('click', function () {
                    $('.swiper-container').fadeOut();
                    $('.swiper-wrapper').empty();
                    mySwiper.destroy(false);
                })
            },
            openProdDetail: function (prodId,userInfo) {
                api.openWin({
                    name: 'productDetail',
                    url: 'widget://html/products/product-detail-new.html',
                    bounces: false,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    },
                    pageParam: {
                        prodId: prodId,
                        userInfo:JSON.stringify(userInfo),
                        useServer:userInfo.RegisterCountryCode
                    }
                });
            },
        }


    });

};


function back() {
    //localStorage.setItem('openFrame', '');
    api.closeWin();
}

