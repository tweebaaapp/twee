var mPages = 0;
apiready = function () {
    //getmonents(mPages, false);
    api.addEventListener({
        name: 'scrolltobottom',
        extra: {
            threshold: 0            //设置距离底部多少距离时触发，默认值为0，数字类型
        }
    }, function (ret, err) {
        mPages++;
        square.getAllMoments(mPages);
    });
    //下拉刷新
    var pullDownRefresh = lan === 'zh' ? '下拉刷新' : 'Pull down to refresh';
    var refreshing = lan === 'zh' ? '刷新中...' : 'Refreshing...';
    var releaseRefresh = lan === 'zh' ? '放开开始刷新' : 'Release refresh';
    api.setRefreshHeaderInfo({
        bgColor: '#fff',
        textColor: '#000',
        textDown: pullDownRefresh,//'Pull down to refresh'
        textLoading: refreshing, //'Refreshing...'
        showTime: false,
        textUp: releaseRefresh //'Release refresh'
    }, function (ret, err) {
        mPages = 0;
        square.momentsData = [];
        square.getAllMoments(0);
    });
    api.addEventListener({
        name: 'reloadSquare',
    }, function (ret, err) {
        mPages = 0;
        square.momentsData = [];
        square.getAllMoments(0);
    });
    var square = new Vue({
        el: '#square',
        mounted: function () {
            this.getAllMoments(0);
            this.randomTycoon();
        },
        data: {
            randomData: null,
            momentsData: []
        },
        methods: {
            searchFocus: function () {
                localStorage.setItem('openFrame', 'searchUsers');
                api.openFrame({
                    name: 'searchUsers',
                    url: '../users/search-users.html',
                    bounces: true,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    }
                });
            },
            recommendInit: function () {
                var mySwiper = new Swiper('.recommend-container', {
                    direction: 'horizontal',
                    loop: false,
                    freeMode: true,
                    initialSlide: 0,
                    slidesPerView: 2.5,
                    spaceBetween: 10,
                });
            },
            randomTycoon: function () {
                var that = this;
                var url = getBaseUrl() + "/api/users/random/" + 6;
                getAxois(url, "GET", {}).then(function (response) {
                    // console.log(JSON.stringify(response));
                    that.randomData = response.data;
                    setTimeout(function () {
                        that.recommendInit();
                    });

                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            getAllMoments: function (page,scroll) {
                loading('show');
                var that = this;
                var url = getBaseUrl() + "/api/v2/moments";
                var paramStr = {
                    "currentpage": page,
                    "itemsperpage": 24,
                    "ShowOnHomePage": true,
                };
                getAxois(url, "POST", paramStr).then(function (response) {
                    //console.log(JSON.stringify(response.data));
                    api.refreshHeaderLoadDone();
                    if (response.data.Data.length !== 0) {
                        that.momentsData = that.momentsData.concat(response.data.Data);
                    }
                    setTimeout(function () {
                        setPost();
                        loading('hide');
                        setCopy();
                    })
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '#######################');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            openShareBar:function(content,id,name){
                var userId = localStorage.getItem('userId');
                var ShareUrl = 'https://tweebaa.cn/html/'+ lan +'/mblog.html?userId='+ userId +'type=blog&&id='+ id+'&server=cn';
                $('#shareURL').attr("data-shareUrl", ShareUrl);
                $("#name").text(name);
                $('#shortDescription').text(content);
                new OpenShareBox();
            },
            showImages:function(event,Index){
                var that = event.target;
                var pic = $(that).closest('.moment-pictures').find('.moment-pic-box');
                pic.each(function () {
                    var imgUrl = $(this).attr('data-img');
                    var img = '<img class="img-fluid" src=' + imgUrl + ' alt="image">';
                    $('#showImages .swiper-wrapper').append('<div class="swiper-slide"><div class="swiper-zoom-container">' + img + '</div></div>');
                });
                $('#showImages').fadeIn();
                var mySwipershowImages = new Swiper('#showImages', {
                    direction: 'horizontal',
                    loop: false,
                    zoom: true,
                    initialSlide: Index,
                    on: {
                        init: function(){
                            $('#downloadImage').val($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                        transitionEnd: function(){
                            $('#downloadImage').val($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                    },
                });
                addListenerForDownloadImg();
                $('.close-swiper').on('click', function () {
                    $('#showImages').fadeOut();
                    $('#showImages .swiper-wrapper').empty();
                    mySwipershowImages.destroy(false);
                })
            },
            openProdDetail: function (prodId,userInfo) {
                api.openWin({
                    name: 'productDetail',
                    url: 'widget://html/products/product-detail-new.html',
                    bounces: false,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    },
                    pageParam: {
                        prodId: prodId,
                        userInfo:JSON.stringify(userInfo),
                        useServer:userInfo.RegisterCountryCode
                    }
                });
            },
            openTycoonPlace: function (user) {
                openUserTycoonPlace(user.Id, user.StoreName, user.AvatarUrl,  'square')
            },
        }
    });

};

