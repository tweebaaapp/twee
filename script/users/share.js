//share to weChat

function shareToWeChat(type) {
    var pic = '../../image/120.png';
    var url = $('#shareURL').attr("data-shareUrl");
    var title = $("#name").text() || $("#content").text();
    var content = $('#shortDescription').text();
    var wx = api.require('wx');
    wx.isInstalled(function (ret, err) {
        if (ret.installed) {
            wx.shareWebpage({
                apiKey: 'wx50fa519ee596a49d',
                scene: type,
                title: title,
                thumb: pic,
                description:content,
                contentUrl: url
            }, function (ret, err) {
                //console.log(JSON.stringify(err));
                if (ret.status) {
                    api.toast({
                        msg: $.i18n.prop('shareSuccessful'),
                        duration: 2000,
                        location: 'bottom'
                    });
                } else {
                    if (err.code === 3) {

                    } else if (err.code === 2) {
                        api.toast({
                            msg: $.i18n.prop('shareCanceled'),
                            duration: 2000,
                            location: 'bottom'
                        });
                    }
                }

            });
        } else {
            api.toast({
                msg: $.i18n.prop('wechatNotInstall'),
                duration: 2000,
                location: 'bottom'
            });
        }
    });
}

function shareToFacebook() {
    var url = $('#shareURL').attr("data-shareUrl");
    var pic = '../../image/120.png';
    var title = $("#name").text() || $("#content").text();
    var content = $('#shortDescription').text();

    var systemType = api.systemType;
    if(systemType === 'ios'){
        api.openApp({
            iosUrl: "https://www.facebook.com/sharer.php?u=" + url + "&t=" + title,
            androidPkg: 'android.intent.action.VIEW',
            mimeType: 'text/html',
            uri: "https://www.facebook.com/sharer.php?u=" + url + "&t=" + title
        });
    }else {
        var facebook = api.require('facebook');
        facebook.shareLinked({
            url: url,
            imgUrl: pic,
            description: content,
            title: title
        }, function(ret, err){
            if(ret.status) {
                //api.alert({msg:JSON.stringify(ret)});
            } else {
                api.toast({
                    msg: $.i18n.prop('shareCanceled'),
                    duration: 2000,
                    location: 'bottom'
                });
            }
        });
    }

}

function shareToGoogle() {
    var url = $('#shareURL').attr("data-shareUrl");
    var title = $("#name").text() || $("#content").text();
    api.openApp({
        iosUrl: "https://plus.google.com/share?url=" + url,
        androidPkg: 'android.intent.action.VIEW',
        mimeType: 'text/html',
        uri: "https://plus.google.com/share?url=" + url
    });
}

function shareToTwitter() {
    var url = $('#shareURL').attr("data-shareUrl");
    var title = $("#name").text() || $("#content").text();
    api.openApp({
        iosUrl: "https://twitter.com/intent/tweet?url=" + url + "&text=" + title,
        androidPkg: 'android.intent.action.VIEW',
        mimeType: 'text/html',
        uri: "https://twitter.com/intent/tweet?url=" + url + "&text=" + title
    });
}

function shareToQQZone() {
    var title = $("#name").text() || $("#content").text();
    var pic = $('#hiddenImg').val();
    var url = $('#shareURL').attr("data-shareUrl");
    api.openApp({
        iosUrl: "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?summary=" + title + "&url=" + encodeURIComponent(url),
        androidPkg: 'android.intent.action.VIEW',
        mimeType: 'text/html',
        uri: "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?summary=" + title + "&url=" + encodeURIComponent(url)
    });
}

function shareToWeibo() {
    var title = $("#name").text() || $("#content").text();
    var url = $('#shareURL').attr("data-shareUrl");
    api.openApp({
        iosUrl: "http://v.t.sina.com.cn/share/share.php?title=" + title + "&url=" + url,
        androidPkg: 'android.intent.action.VIEW',
        mimeType: 'text/html',
        uri: "http://v.t.sina.com.cn/share/share.php?title=" + title + "&url=" + encodeURIComponent(url)
    });
    // console.log('aa');
    // var weiboPlus= api.require('weiboPlus');
    // console.log(weiboPlus);
    // weiboPlus.shareWebPage({
    //     text: title,
    //     title: '测试标题',
    //     description: '分享内容的描述',
    //     thumb: '../../image/120.png',
    //     contentUrl: url
    // }, function(ret, err) {
    //     console.log('bb');
    //     if (ret.status) {
    //         alert('分享成功');
    //     }else{
    //         alert(JSON.stringify(err));
    //     }
    // });
}

function shareToMail() {
    var title = $("#name").text() || $("#content").text();
    var url = 'mailto:?body=' + title + $('#shareURL').attr("data-shareUrl");
    $('#mailLink').attr('href', url);
    setTimeout(function () {
        $('#mailLink')[0].click();
    }, 500);
}

function shareToclipboard() {
    var text = $('#shareURL').attr("data-shareUrl");
    var clipBoard = api.require('clipBoard');
    clipBoard.set({
        value: text
    }, function (ret, err) {
        if (ret) {
            messages($.i18n.prop('copied'), 'success', 'top', 2);
            hideBounceboxBottom();
        }
    });
}
