
var searchUsers = new Vue({
    el: '#searchUsers',
    data: {
        searchInput: null,
        searchData:null,
        myID: localStorage.getItem('userId')
    },
    methods:{
        clearInput: function () {
            this.searchInput = this.searchData = null;
        },
        submitSearch: function () {
            var that = this;
            var myId = localStorage.getItem('userId');
            var id = myId ?"&viewerId="+myId:"";
            if(this.searchInput !==''){
                loading('show');
                var url = getBaseUrl() + "api/users?q="+ this.searchInput +id ;
                $('input').blur();
                getAxois(url,"GET",{}).then(function (response) {
                    //console.log(JSON.stringify(response.data));
                    that.searchData = response.data;
                    loading('hide');
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            }
        },
        followBtn: function (userId,event,index) {
            var token = localStorage.getItem('access_token');
            if(token){
                var that = this;
                var myId = localStorage.getItem('userId');
                var tar = event.target;
                var url = getBaseUrl() + "/api/users/"+ myId +"/follow/users/"+ userId;
                getAxois(url,"PUT",{}).then(function (response) {
                    //console.log(JSON.stringify(response));
                    if(response.statusText==="OK"){
                        $(tar).toggleClass('active');
                        that.searchData[index].Followed = !that.searchData[index].Followed;
                        if(that.searchData[index].Followed){
                            that.searchData[index].FollowersCount ++;
                        }else{
                            that.searchData[index].FollowersCount --;
                        }
                        api.sendEvent({
                            name: 'reloadFollowing'
                        });
                    }
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            }else{
                openLogin();
            }
        },
        openTycoonPlace: function (index) {
            openUserTycoonPlace(this.searchData[index].Id,this.searchData[index].StoreName,this.searchData[index].AvatarUrl,'searchUsers')
        }
    }

});