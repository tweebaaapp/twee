apiready = function () {
    var username = localStorage.getItem('userName');
    var db = api.require('db');
    var ret = db.openDatabaseSync({
        name: username
    });
    var favoriteList = new Vue({
        el: '#favoriteList',
        mounted: function () {
            this.getFavoriteList();
        },
        data: {
            favoriteList: []
        },
        methods: {
            getFavoriteList:function () {
                var showFavorite = db.selectSqlSync({
                    name: localStorage.getItem('userName'),
                    sql: 'SELECT * FROM favorite'
                });
                var d = showFavorite.data;
                var len = d.length;
                for(i = 0; i<len; i++){
                    d[i].userInfo = (JSON.parse(d[i].userInfo));
                }
                console.log(JSON.stringify(d));
                this.favoriteList = d
            },
            removeFavorite:function (prodId,event) {
                var tar  = event.target;
                $(tar).closest('li').remove();
                var cleanKey = db.selectSqlSync({
                    name: username,
                    sql: 'DELETE FROM favorite WHERE prodId ='+prodId
                });
            },
            openDetail: function (prodId,userInfo) {
                api.openWin({
                    name: 'productDetail',
                    url: 'widget://html/products/product-detail-new.html',
                    bounces: false,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    },
                    pageParam: {
                        prodId: prodId,
                        userInfo:JSON.stringify(userInfo),
                        countryCode:userInfo.RegisterCountryCode
                    }
                });
            }
        }
    })
};


function back() {
    api.closeWin({
        name: 'favoriteList'
    });
}