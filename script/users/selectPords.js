
apiready = function () {

    api.addEventListener({
        name:'swiperight'
    }, function(ret, err){
        back();
    });
    var data = JSON.parse(api.pageParam.prodsData);
    setTimeout(function () {
        var selectProds = new Vue({
            el: '#selectProds',
            mounted: function () {
                changePriceType();
                loading('hide');
            },
            data: {
                prodsData: data
            },
            methods:{
                selected:function (index) {
                    // console.log(JSON.stringify(this.prodsData[index]));
                    api.sendEvent({
                        name: 'selectedProd',
                        extra: {
                            selectedProd: JSON.stringify(this.prodsData[index])
                        }
                    });
                    setTimeout(function () {
                        back();
                    })
                }
            }

        });
    },500);


};


function back() {
    api.closeWin({
        name: 'selectProds'
    });
}