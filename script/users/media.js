var Page = 0;
apiready = function () {
    loadpost();
    function loadpost() {
        loading('show');
        var tt;
        if(localStorage.getItem('contacts')){
            getPostList(null,null);
            clearTimeout(tt);
        }else{
            tt = setTimeout(function () {
                loadpost();
            },500);
        }
    }


    //上拉加载
    api.addEventListener({
        name: 'scrolltobottom',
        extra: {
            threshold: 20            //设置距离底部多少距离时触发，默认值为0，数字类型
        }
    }, function (ret, err) {
        loading('show');
        Page++;
        getPostList(Page,true);
    });
    //下拉刷新
    api.setRefreshHeaderInfo({
        bgColor: '#fff',
        textColor: '#000',
        textDown: $.i18n.prop('pullDownRefresh'),//'Pull down to refresh'
        textLoading: $.i18n.prop('refreshing'), //'Refreshing...'
        showTime:false,
        textUp: $.i18n.prop('releaseRefresh') //'Release refresh'
    }, function(ret, err) {
        getPostList(null,null);
    });
    isOnLineStatus();
};

function getPostList(page,ifScroll) {
    loading('show');
    if (!ifScroll) {
        page = 0;
    }
    var url = 'https://api.tweebucks.com/tweebaa/getUserContactPostList';
    var token = "IM";
    var method = "POST";
    var contentTypeStr = "application/json";
    var currentPage = page || 0;
    if (getApi() === 'com') {
        var chatUserId = localStorage.getItem('userName') + '@International';
    } else if (getApi() === 'cn') {
        var chatUserId = localStorage.getItem('userName') + '@China';
    }
    var paramStr = {
        "itemStr":chatUserId,
        "searchType":moment().format('YYYY/MM/DD')
    };
    //console.log(JSON.stringify(paramStr));
    $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        //console.log(JSON.stringify(data));
        if (!ifScroll) {
            $('#post-list').empty();
        }
        var sharePic,showVideos;
        var len = data.length;
        if (len !== 0) {
            for (i = 0; i < len; i++) {
                var pic = data[i].picUrlList;
                var p = '';
                var userid = (data[i].subdomain).split('.')[0];
                var userName = data[i].userIm.name;
                var userPortrait = data[i].userIm.portraitUri;
                var date = moment.utc(data[i].CreatedTime).local().format('YYYY-MM-DD HH:mm');
                if(data[i].videoUrl){
                    var videoUrl =  data[i].videoUrl;
                    showVideos = '';
                }else{
                    showVideos = 'd-none';
                    videoUrl= '';
                }
                if (!pic) {
                    sharePic = '';
                } else {
                    for(r = 0; r <  pic.length; r++){
                        var picture = pic[r];
                        p +=   '<div class="moment-pic-box" data-img="'+ picture.replace(' ','%20') +'" onclick="showPostImg(this,'+ r +')">\n' +
                            '   <div class="img-box"></div>\n' +
                            '</div>';
                    }
                    sharePic = pic[0];
                }
                var template ='<li class="media-post-box clearfix mt-4 mm-15">\n' +
                    '            <div class="media-post-box-left">\n' +
                    '                <img src="'+ userPortrait +'" class="post-user-box-header" alt="">\n' +
                    '                <span class="x-small text-dark-gray text-center post-date">'+ date +'</span>\n' +
                    '            </div>\n' +
                    '            <div class="media-post-box-right">\n' +
                    '            <div class="clearfix">\n' +
                    '                    <span class="post-username small pull-left text-blue">'+ userName +'</span>\n' +
                    '                    <i class="icon iconfont icon-shop_fill pull-right" onclick="gotoTyconnplace(\'' + data[i].ShareUrl + '\')"></i>\n' +
                    '            </div>\n' +
                    '                <p class="post-content">' + data[i].post + '</p>\n' +
                    '                <div class="media-post-media-box">\n' +
                    '                   <div class="moment-pictures clearfix">\n' +
                    p +
                    '                   </div>\n' +
                    '                   <div class="momnet-video-box '+ showVideos +'">\n' +
                    '                       <video src="'+ videoUrl +'" class="moment-video" controls="controls" preload="auto" -webkit-playsinline=true></video>\n' +
                    '                   </div>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '        </li>';
                $('#post-list').append(template);
            }
        }
        //结束下拉刷新状态
        api.refreshHeaderLoadDone();
        loading('hide');
        //在页面载入图像
        $('#post-list').find('li').each(function () {
            var pic = $(this).find('.moment-pic-box');
            if(pic){
                var len =pic.length;
                var w;
                var h;
                switch (len){
                    case 1:
                        w = 100;
                        h = 5;
                        break;
                    case 2:
                        w = 50;
                        h = 5;
                        break;
                    case 3:
                        w = 33.3;
                        break;
                    default:
                        w = 25;
                        break;
                }

                pic.each(function () {
                    var bk = $(this).attr('data-img');
                    $(this).css('width',(w+'%'));
                    if(h){
                        $(this).height(h+'rem');
                    }else{
                        $(this).height($(this).width());
                    }

                    $(this).find('.img-box').css({'background-image':'url('+bk+')','background-size':'cover'});
                });
            }

        });

        //video前进一秒
        $(".moment-video").each(function () {
            $(this)[0].addEventListener("loadedmetadata", function () {
                $(this)[0].currentTime = 0.5;
            });
        })
    });

}

function showPostImg(that,Index) {
    var pic = $(that).closest('.moment-pictures').find('.moment-pic-box');
    pic.each(function () {
        var imgUrl = $(this).attr('data-img');
        var img = '<img class="img-fluid" id ="'+ Index +'" src='+ imgUrl + ' alt="image">';
        $('.swiper-wrapper').append('<div class="swiper-slide"><div class="swiper-zoom-container">'+ img +'</div></div>');
    });
    $('.swiper-container').fadeIn();
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        zoom : true,
        initialSlide :Index,
        pagination: {
            el: '.swiper-pagination',
            type:'fraction'
        }
    });
    $('.close-swiper').on('click',function () {
        $('.swiper-container').fadeOut();
        $('.swiper-wrapper').empty();
        mySwiper.destroy(false);
    })
}
