
function loadMomentList(page,ifScroll) {
    $('.no-item').hide();
    if (!ifScroll) {
        page = 0;
    }
    loading('show');
    var el = $('.moment-ul');
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var currentPage = page || 0;
    var paramStr = {"CustomerId": userId,'currentPage': currentPage, "itemsperpage": 12};
    var url = getBaseUrl() + "api/users/" + userId + "/moments";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        api.refreshHeaderLoadDone();
        loading('hide');
        if (!ifScroll) {
            el.empty();
        }
        var sharePic;
        var len = data.length;
        if (len !== 0) {
            $('.no-item').hide();
            for (i = 0; i < len; i++) {
                var pic = data[i].Pictures;
                var p = '';
                if (pic === null || pic === '' ||  pic.length === 0) {
                    sharePic = '';
                } else {
                    for(r = 0; r < pic.length; r++){
                        var picture = pic[r].DownloadUrl;
                        p +=   '<div class="moment-pic-box" data-img="'+ picture.replace(/\s/ig,'%20') +'" onclick="showPostImg(this,'+ r +')">\n' +
                            '   <div class="img-box"></div>\n' +
                            '</div>';
                    }
                    sharePic = pic[0].DownloadUrl;
                }
                var showVideos,videoUrl;
                if(data[i].Attachments.length !==0){
                    videoUrl =  data[i].Attachments[0].DownloadUrl;
                    showVideos = '';
                }else{
                    showVideos = 'd-none';
                    videoUrl ='';
                }
                var hasProduct = '';
                //console.log(JSON.stringify(data[i].Owner));
                if(data[i].MomentProducts.length>0){
                    hasProduct = '        <div id="popularize-product-box" class="ui-row" onclick="openProdDetail(\''+data[i].MomentProducts[0].Product.Id+'\',\''+ data[i].MomentProducts[0].Product.ProductSource +'\')">\n' +
                        '                <div class="pl-4 pb-2 small text-dark-gray text-title">'+ data[i].MomentProducts[0].Description +'</div>\n' +
                        '                <div class="ui-col ui-col-10"><img src="'+ data[i].MomentProducts[0].Product.FeaturedPicture.DownloadUrl +'" alt=""></div>\n' +
                        '                <div class="ui-col ui-col-90">\n' +
                        '                    <span class="x-small text-title ui-nowrap">'+ data[i].MomentProducts[0].Product.Name +'</span>\n' +
                        '                </div>\n' +
                        '        </div>';
                }

                var storename = localStorage.getItem('storename');
                var ShareUrl = 'https://tweebaa.'+ getApi()+'/html/'+ lan +'/mblog.html?userId='+ userId +'&id='+ data[i].Id+'&type=blog&server='+getApi();
                var date = moment.utc(data[i].CreatedTime).local().format('YYYY-MM-DD HH:mm');
                var template = '<li class="moment-item bottom-shadow clearfix mm-15">\n' +
                    '        <div class="moment-header ui-row-flex">\n' +
                    '          <div class="moment-date ui-col ui-col-3 text-dark-gray x-small pl-4">'+ date +'</div>\n' +
                    '          <div class="moment-btn-group ui-col ui-col-2 text-right">' +
                    '<i class="icon iconfont icon-zhuanhuanzhe" onclick="showBounceboxLeft(\'' + data[i].Id + '\',this)"></i>&nbsp;&nbsp; ' +
                    '<i class="icon iconfont icon-share" style="font-size: 1.8rem" onclick="openShareBox(\'' + ShareUrl + '\',\''+ storename +'\',\''+ data[i].Content.substring(0,50) +'\')"></i> &nbsp;&nbsp; ' +
                    '<i class="icon iconfont icon-empty"  style="font-size: 1.6rem" onclick="deleteMoment(' + data[i].Id + ',this)"></i></div>\n' +
                    '        </div>\n' +
                    '        <p class="p-4 text-subtitle moment-content">' + data[i].Content + '</p>\n' +
                    '        <div class="moment-content-extend d-none" onclick="showExtend(this)"> <span class="text-blue  p-2"> '+ $.i18n.prop('fullText') +'</span> </div>\n' +
                    '        <div class="moment-pictures clearfix">\n' +
                    p +
                    '        </div>\n' +
                    '        <div class="momnet-video-box '+ showVideos +'" data-video=\''+ JSON.stringify(data[i].Attachments[0]) +'\'>\n' +
                    '          <video src="'+ videoUrl +'" class="moment-video" controls="controls" preload="auto" -webkit-playsinline="true"></video>\n' +
                    '        </div>\n' +
                    hasProduct+
                    '      </li>';
                el.append(template);
            }
        } else if (len === 0){
            if (!ifScroll){
                $('.no-item').show();
            }
        }

        //在页面载入图像
        el.find('li').each(function () {
            var pic = $(this).find('.moment-pic-box');
            if(pic){
                var len =pic.length;
                var w;
                var h;
                switch (len){
                    case 1:
                        w = 50;
                        h = 10;
                        break;
                    case 2:
                        w = 50;
                        h = 5;
                        break;
                    case 3:
                        w = 33.3;
                        break;
                    default:
                        w = 25;
                        break;
                }

                pic.each(function () {
                    var bk = $(this).attr('data-img');
                    $(this).css('width',(w+'%'));
                    if(h){
                        $(this).height(h+'rem');
                    }else{
                        $(this).height($(this).width());
                    }

                    $(this).find('.img-box').css({'background-image':'url('+bk+')','background-size':'cover'});
                });
            }
            var extendBtn = $(this).find('.moment-content-extend');
            var testContent = $(this).find('.moment-content');
            var contentHeight = testContent.height();
            if(contentHeight > 80 ){
                testContent.addClass('overflow');
                extendBtn.removeClass('d-none');
            }
        });

        //video前进一秒
        $(".moment-video").each(function () {
            if($(this).attr('src') !==''){
                $(this)[0].addEventListener("loadedmetadata", function (d) {
                    $(this)[0].currentTime = 1;
                });
            }
        })
    });
    setCopy();
}

function showExtend(that) {
    $(that).toggleClass('extend');
    if($(that).hasClass("extend")){
        $(that).html('<span class="text-blue  p-2">'+ $.i18n.prop('collapse')+'</span>').prev().removeClass('overflow');
    }else{
        $(that).html('<span class="text-blue  p-2">'+ $.i18n.prop('fullText')+'</span>').prev().addClass('overflow');
    }

}

function openProdDetail(prodId,tweebaaTag) {
    var Tag = tweebaaTag =='1'?'tweebaa-tag':'';
    api.openWin({
        name: 'myProductDetail',
        url: '../products/my-product-detail.html',
        bounces:false,
        allowEdit:true,
        pageParam: {
            prodId: prodId,
            Ptype: 'virtual',
            tweebaaTag:Tag
        }
    });
}

//add moment
function saveNewMoment() {
    loading('show');
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var comment = $('#fullDescription').val();
    var productId = $('#prodName').attr('data-id');
    var description = $('#description').val();
    var products = productId?[{'ProductId':parseInt(productId),"Description":description}]:[];
    // var picArray = [];
    // $("#uploadImgBox").find('img').each(function () {
    //     var pic = $(this).attr('src').split(",")[1];
    //     var picJson = {'PictureBinaryBase64': pic, 'FileName': 'picture.jpg','MediaType': 'Picture'};
    //     picArray.push(picJson);
    // });
    var showH = $("#showHomepage").prop('checked');
    var paramStr = {"Content": comment, "Attachments": picArray, "ShowOnHomePage": showH,"MomentProducts":products};
    var contentTypeStr = "application/json";
    var url = getBaseUrl() + "api/users/" + userId + "/moments";
    var method = "POST";
    //console.log(JSON.stringify(paramStr));
    $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        //console.log(JSON.stringify(data));
        var momentId = data.Id;
        if (momentId) {
            if ($('#getVideo').val()) {
                uploadVideo(momentId, userId, 'moment');
            } else {
                addMonetSuccess();
                loading('hide');
            }
        } else {
            loading('hide');
            messages($.i18n.prop('systemError'), 'error', 'bottom', 3);
        }
    });
}

function addMonetSuccess() {
    back();
}

//del Moment
function deleteMoment(momentId,This) {
    confirmBox($.i18n.prop('wantDeleteMoment'), $.i18n.prop('cancel'), $.i18n.prop('sure'),'error'); //'Do you want delete this moment?'
    $('.dialog-box').find('button').on('click',function (e) {
        var index = $(this).attr('id');
        if (index === 'btn-2') {
            $('.dialog-box').remove();
            loading('show');
            var userid = localStorage.getItem('userId');
            var token = localStorage.getItem('access_token');
            // var contentTypeStr = "application/json";
            var url = getBaseUrl() + "api/users/" + userid + "/moments/" + momentId;
            var method = "DELETE";
            // var paramStr = {"userId": userid, "momentId": momentId};
            $.when(postApi(url, method, null, token, null)).done(function (data) {
                loading('hide');
                if(data.status === 200){
                    $(This).closest('li.moment-item').remove();
                }else{
                    messages($.i18n.prop('systemError'), 'error', 'bottom', 3);
                }
            });
        } else {
            $('.dialog-box').remove();
            return false;
        }
    });
}

// convert moment
$('#convertSubmitButton').on('click',function () {
    var userId = localStorage.getItem('userId');
    var momentId = $('#convertInfo').attr('data-momentId');
    var ParamStr = {
        "Name":$('#productName').val(),
        "OldPrice":$('#oldprice').val(),
        "Price":$('#price').val(),
        "FullDescription":$('#fullDescription').val(),
        "Categories":[{"Id": $('#selectCategory').val()}, {"Id": $('#secondCategory').val()}, {"Id": $('#thirdCategory').val()}],
        "DisplayOrder": 0,
        "ShowOnHomePage": false
    };
    // console.log(JSON.stringify(ParamStr));
    // console.log("momentId:"+momentId);
    // console.log('userId:'+userId);
    if(!ParamStr.Name){
        messages($.i18n.prop('productNameEmpty'), 'error', 'top', 3); //'product name can not be empty!'
        return false;
    }
    convertMoment(userId,momentId,ParamStr)
});

function convertMoment(userId,momentId,ParamStr) {
    loading('show');
    var token = localStorage.getItem('access_token');
    var url = getBaseUrl() + "api/users/" + userId + "/moments/"+ momentId +"/to/product/virtual";
    var method = "POST";
    var contentTypeStr = "application/json";
    $.when(postApi(url, method, ParamStr, token, contentTypeStr)).done(function (data) {
        // console.log(JSON.stringify(data));
        $('.container').removeClass('invisible');
        if(data.status){
            var token = localStorage.getItem('access_token');
            var url = getBaseUrl() + "api/users/" + userId + "/moments/" + momentId;
            var method = "DELETE";
            var paramStr = {"userId": userId, "momentId": momentId};
            $.when(postApi(url, method, paramStr, token, null)).done(function (res) {
                console.log(JSON.stringify(res));
                loading('hide');
                messages($.i18n.prop('momentConvertSuccess'), 'success', 'bottom', 3); //'Moment convert success!'
                setTimeout(function () {
                    loadMomentList(null,null);
                    hideBounceboxLeft();
                },1000);
            });
        }else{
            loading('hide');
            messages($.i18n.prop('systemError'), 'error', 'bottom', 3);
            hideBounceboxLeft();
        }
    });
}

//share moment
function openShareBox(url,storename,content) {
    //console.log(url);
    $('#shareURL').attr("data-shareUrl", url);
    //onsole.log($('#shareURL').length);
    $("#name").text(storename);
    $('#shortDescription').text(content);
    //console.log($('#shareURL').attr("data-shareUrl"));
    new OpenShareBox();
}
//展示图片
function showPostImg(that,Index) {
    var pic = $(that).closest('.moment-pictures').find('.moment-pic-box');
    pic.each(function () {
        var imgUrl = $(this).attr('data-img');
        var img = '<img class="img-fluid" id ="'+ Index +'" src='+ imgUrl + ' alt="image">';
        $('.swiper-wrapper').append('<div class="swiper-slide"><div class="swiper-zoom-container">'+ img +'</div></div>');
    });
    $('.swiper-container').fadeIn();
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        zoom : true,
        initialSlide :Index,
        pagination: {
            el: '.swiper-pagination',
            type:'fraction'
        },navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    });
    $('.close-swiper').on('click',function () {
        $('.swiper-container').fadeOut();
        $('.swiper-wrapper').empty();
        mySwiper.destroy(false);
    })
}

//获取产品列表
function loadProdsList() {
    loading('show');
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    console.log(userId);
    var paramStr = {'userId':userId,'currentPage': 0, "itemsperpage": 999};
    var url = getBaseUrl() + "api/users/" + userId + "/products";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        //console.log(JSON.stringify(data));
        var obj = data.Data;
        $('#selectBtn').removeClass('d-none').on('click',function () {
            api.openWin({
                name: 'selectProds',
                url: 'selectPords.html',
                bounces: false,
                rect: {
                    x: 0,
                    y: 0,
                    w: 'auto',
                    h: 'auto'
                },
                slidBackType:false,
                bgColor: '#fff',
                animation:{
                    type:"movein",
                    subType:"from_right",
                },
                pageParam: {
                    prodsData:JSON.stringify(obj)
                }
            });
        });

    });
}

function back() {
    //localStorage.setItem('openFrame', '');
    api.closeWin();
}


function showVideo(file) {
    var url = null ;
    if (window.createObjectURL!=undefined) { // basic
        url = window.createObjectURL(file) ;
    } else if (window.URL!=undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file) ;
    } else if (window.webkitURL!=undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file) ;
    }
    return url ;
}