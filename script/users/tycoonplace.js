var mPages = 0;
var pPages = 0;
apiready = function () {
    //getmonents(mPages, false);
    api.addEventListener({
        name: 'scrolltobottom',
        extra: {
            threshold: 0            //设置距离底部多少距离时触发，默认值为0，数字类型
        }
    }, function (ret, err) {
        if (tycoonplace.list1) {
            mPages++;
            tycoonplace.getmonents(mPages);
        } else {
            pPages++;
            tycoonplace.getProducts(pPages);
        }
    });
    $(window).scroll(function () {
        var top = $(this).scrollTop();
        if (top > 200) {
            $('.tycoonNav').addClass('fixed').removeClass('bottom-shadow');
            $('header').hide();
        } else {
            $('.tycoonNav').removeClass('fixed').addClass('bottom-shadow');
            $('header').show();
        }
    });


    var tycoonplace = new Vue({
        el: '#tycoonplace',
        mounted: function () {
            this.getUserDetails();
        },
        data: {
            userInfo: '',
            storeName:api.pageParam.storeName,
            avatarUrl:api.pageParam.AvatarUrl,
            list1: true,
            myID: localStorage.getItem('userId'),
            momentsData: [],
            productsData: []
        },
        methods: {
            getUserDetails: function () {
                loading('show');
                var that = this;
                var url = getBaseUrl() + "/api/users/"+api.pageParam.userId +"?viewerId="+this.myID;
                //console.log(JSON.stringify(url));
                getAxois(url, "GET", {}).then(function (response) {
                    //console.log(JSON.stringify(response.data));
                    loading('hide');
                    that.userInfo = response.data;
                    that.getmonents(null,response.data.RegisterCountryCode);
                    that.getProducts(null,response.data.RegisterCountryCode);
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '@@@@@@@@@@@@@@@@');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            followBtn: function (userId, event) {
                var token = localStorage.getItem('access_token');
                if (token) {
                    var that = this;
                    var myId = localStorage.getItem('userId');
                    var tar = event.target;
                    var url = getBaseUrl() + "/api/users/" + myId + "/follow/users/" + userId;
                    getAxois(url, "PUT", {}).then(function (response) {
                        //console.log(JSON.stringify(response));
                        if (response.statusText === "OK") {
                            $(tar).toggleClass('active');
                            if ($(tar).hasClass('active')) {
                                that.userInfo.FollowersCount++;
                            } else {
                                that.userInfo.FollowersCount--;
                            }
                            api.sendEvent({
                                name: 'reloadFollowing'
                            });
                        }
                    }).catch(function (error) {
                        console.log(JSON.stringify(error));
                        messages($.i18n.prop('systemError'), 'error', 'top', 3);
                    });
                } else {
                    openLogin();
                }
            },
            chatToUser: function () {
                var chatID = localStorage.getItem('chatID');
                if (chatID) {
                    var userinfo = this.userInfo;
                    var userId = userinfo.UserName + '@new';
                    chatTo(userId, userinfo.StoreName, userinfo.AvatarUrl, 'PRIVATE', false)
                } else {
                    openLogin();
                }
            },
            momentList: function (event) {
                var tar = event.target;
                if (!$(tar).hasClass('active')) {
                    this.list1 = true;
                }
            },
            productList: function (event) {
                this.list1 = false;
            },
            getProducts: function (pPages,countryCode) {
                var that = this;
                var url = getBaseUrl(countryCode) + "/api/v2/users/products";
                var paramStr = {
                    "ownerId": api.pageParam.userId,
                    "currentpage": pPages || 0,
                    "itemsperpage": 24
                };
                getAxois(url, "POST", paramStr).then(function (response) {
                    //console.log(JSON.stringify(response));
                    if (response.data.Data.length !== 0) {
                        that.productsData = that.productsData.concat(response.data.Data);
                    }
                    setTimeout(function () {
                        changePriceType(countryCode);
                    });

                    //console.log(JSON.stringify(that.productsData));
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '+++++++++++++++++++++++++++++');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            getmonents:function(mPages,countryCode){
                var that = this;
                var url = getBaseUrl(countryCode) + "/api/v2/moments";
                var paramStr = {
                    "ownerId": api.pageParam.userId,
                    "currentpage": mPages,
                    "itemsperpage": 24,
                    "ShowOnHomePage": true,
                };
                getAxois(url, "POST", paramStr).then(function (response) {
                    //console.log(JSON.stringify(response));
                    if (response.data.Data.length !== 0) {
                        that.momentsData = that.momentsData.concat(response.data.Data);
                    }
                    setTimeout(function () {
                        setPost();
                        setCopy();
                    })
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '#######################');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            openShareBar:function(content,id){
                //var server = this.userInfo.RegisterCountryCode ==='CN'?'cn':'com';
                var ShareUrl = 'https://tweebaa.cn/html/'+ lan +'/mblog.html?userId='+ api.pageParam.userId +'&type=blog&id='+ id+'&server='+server;
                $('#shareURL').attr("data-shareUrl", ShareUrl);
                $("#name").text(content);
                new OpenShareBox();
            },
            showImages:function(event,Index){
                var that = event.target;
                var pic = $(that).closest('.moment-pictures').find('.moment-pic-box');
                pic.each(function () {
                    var imgUrl = $(this).attr('data-img');
                    var img = '<img class="img-fluid" id ="' + Index + '" src=' + imgUrl + ' alt="image">';
                    $('.swiper-wrapper').append('<div class="swiper-slide"><div class="swiper-zoom-container">' + img + '</div></div>');
                });
                $('.swiper-container').fadeIn();
                var mySwiper = new Swiper('.swiper-container', {
                    direction: 'horizontal',
                    loop: false,
                    zoom: true,
                    initialSlide: Index,
                    on: {
                        init: function(){
                            $('#downloadImage').val($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                        transitionEnd: function(){
                            $('#downloadImage').val($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('.swiper-wrapper').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                    },
                });
                addListenerForDownloadImg();
                $('.close-swiper').on('click', function () {
                    $('.swiper-container').fadeOut();
                    $('.swiper-wrapper').empty();
                    mySwiper.destroy(false);
                })
            },
            openPortrait:function(){
                var imageBrowser = api.require('imageBrowser');
                var that = this;
                imageBrowser.openImages({
                    imageUrls: [that.avatarUrl],
                    showList: false,
                    tapClose: true
                });
            },
            openIntroduction:function(){
                var name  = 'userIntroduction';
                var url = 'widget://html/users/user-introduction.html';
                var param = {
                    userId:api.pageParam.userId
                };
                openWindows(name,url,param);
            },
            openDetail: function (prodId) {
                var param = {
                    prodId: prodId,
                    userInfo:JSON.stringify(this.userInfo),
                    countryCode:this.userInfo.RegisterCountryCode
                };
                openWindows('productDetail','widget://html/products/product-detail-new.html',param);
            },
            openProdDetail: function (prodId,userInfo) {
                var param = {
                    prodId: prodId,
                    userInfo:JSON.stringify(userInfo),
                    useServer:userInfo.RegisterCountryCode
                };
                openWindows('productDetail','widget://html/products/product-detail-new.html',param);
            },
            toFollowList:function (type,userId) {
                console.log(userId);
                var param = {
                    type:type,
                    userId:userId
                };
                openWindows('followList','widget://html/users/follow-list.html',param);
            }
        }


    });

};


function back() {
    //localStorage.setItem('openFrame', JSON.parse(localStorage.getItem("pageParam")).fromPage);
    //localStorage.setItem("pageParam",'');
    api.closeWin({
        name: 'tycoonplace'
    });
}

