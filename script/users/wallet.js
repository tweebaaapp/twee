
//钱包二维码
// var qrcode = new QRCode("qrcode");
// function makeQRCode() {
//     var wallet = localStorage.getItem('wallet');
//     $('#walletAddress').text(wallet);
//     $('#walletCopy').val(wallet);
//     if (!wallet) {
//         return;
//     }
//     qrcode.makeCode(wallet);
// }
// function showMyQrCode() {
//     $('#qrCodeBox').css('display','flex');
// }
// $('#qrCodeBox .close').on('click',function () {
//     $('#qrCodeBox').fadeOut();
// });
// //打开扫码
// function openScaner() {
//     var module = api.require('easyQRCodeScan');
//
//     var resultCallback = function(ret, err) {
//         var msg = ret.msg;
//         api.startPlay({
//             path: 'widget://image/beep.mp3'
//         });
//         if(msg.indexOf("http://")!==-1 || msg.indexOf("https://")!==-1){
//             api.openApp({
//                 iosUrl: msg,
//                 androidPkg: 'android.intent.action.VIEW',
//                 mimeType: 'text/html',
//                 uri: msg
//             });
//         }else if(msg.indexOf("@")!==-1){
//             searchFriend(msg);
//             $('.choose-add-friend-method,.show-my-name').hide();
//         }
//     };
//     module.qrScan(resultCallback);
// }

//更新coins 数量
function getRewards() {
    var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "api/users/" + userId + "/rewards";
    var paramStr = {};
    var token = localStorage.getItem('access_token');
    var method = 'GET';
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        $('#coinsBalance').text(data.Coins);
        localStorage.setItem('rewards',JSON.stringify(data));
    });
}
//拷贝钱包地址
$('#copyWalletAddress').on('click',function () {
    var clipBoard = api.require('clipBoard');
    clipBoard.set({
        value: localStorage.getItem('wallet')
    }, function (ret, err) {
        if (ret) {
            messages($.i18n.prop('copied'), 'success', 'top', 2);
        }
    });
});
//根据server 切换PayPal或支付宝
function switchCard() {
    var server = getApi();
    //console.log(server);
    $('.wallet-bind-title-'+server).show();
    if(server ==='cn'){
        $('#moneyCard').find('.balance-logo').find('img').attr('src','../../image/zhifubao.png');
    }else{
        $('#moneyCard').find('.balance-logo').find('img').attr('src','../../image/paypal.png');
    }
}

//获取历史记录
function getCoinsHistory(page) {
    loading('show');
    var userId = localStorage.getItem('userId');
    var url =  getBaseUrl() + "/api/users/"+ userId +"/rewards/history";
    var currentPage = page || 0;
    var paramStr = {
        'currentPage': currentPage, "itemsperpage": 20
    };
    var token = localStorage.getItem('access_token');
    var method = 'GET';
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        loading('hide');
        var date,template;
        if(data.length){
            for(i=0; i<data.length;i++){
                if(data[i].RewardCoins !== 0){
                    date = moment.utc(data[i].CreatedTimeUtc).local().format('YYYY-MM-DD HH:mm');
                    template = '<li class="ui-row-flex p-4" >\n' +
                        '                <div class="ui-col ui-col-1 text-light-gray x-small">'+ date +'</div>\n' +
                        '                <div class="ui-col ui-col-2 small" >'+ $.i18n.prop(data[i].RewardActivityCode) +'</div>\n' +
                        '                <div class="ui-col ui-col-1 small text-green text-right"><strong>+ '+ data[i].RewardCoins+'</strong></div>\n' +
                        '            </li>';
                    $('#coinsHistory').append(template);
                }
            }
        }

    });
}

//获取earning account
function getEarningAccount() {
    loading('show');
    var userId = localStorage.getItem('userId');
    var url =  getBaseUrl() + "/api/users/"+ userId +"/earning-account";
    var paramStr = {};
    var token = localStorage.getItem('access_token');
    var method = 'GET';
    //console.log(userId);
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
       //console.log(JSON.stringify(data));
        loading('hide');
        $('#cardInfo').val(JSON.stringify(data));
        if(!data.PaypalAccount && !data.AlipayAccount && !data.StripeAccount ){
            $('.balance-bind-n').removeClass('d-none');
            $('.balance-bind-y').addClass('d-none');
        }else{
            $('#moneyBalance').text((data.HoldingEarning + data.AvailableEarning).toFixed(2));
            $('#pendingMoneyBalance').text(data.HoldingEarning.toFixed(2));
            $('#availableMoneyBalance').text(data.AvailableEarning.toFixed(2));
            $('.balance-bind-n,.close-change-btn').addClass('d-none');
            $('.balance-bind-y').removeClass('d-none');
            var cardAccount = getBaseUrl()=== "https://api.tycoonplace.com/" ? data.PaypalAccount : data.AlipayAccount;
            $('.card-account').removeClass('d-none').text(cardAccount);
        }
    });
}

//绑定或修改 earning account
$('#bandEarningAccountForm').submit(function (e) {
    e.preventDefault();
    var account =  $('#cardAccount').val();
    var reAccount =  $('#reCardAccount').val();
    if(!account || !reAccount){
        messages($.i18n.prop('pleaseInputAccount'), 'error', 'top', 2);
    }else{
        if(account !== reAccount){
            messages($.i18n.prop('AccountNotMatch'), 'error', 'top', 2);
        }else{
            putEarningAccount(account);
        }
    }
});


function putEarningAccount(account) {
    var userId = localStorage.getItem('userId');
    var cardInfo = JSON.parse(api.pageParam.cardInfo);
    var accountID = cardInfo.Id === undefined ? '' : cardInfo.Id;
    var url =  getBaseUrl() + "/api/users/"+ userId +"/earning-account";
    var paramStr = {};
    var cardType = getBaseUrl()=== "https://api.tycoonplace.com/" ? 'PaypalAccount' :'AlipayAccount';
    switch (cardType) {
        case "PaypalAccount":
            paramStr ={
                'Id': accountID,
                "PaypalAccount": account
            };
            break;
        case "AlipayAccount":
            paramStr ={
                'Id': accountID,
                "AlipayAccount": account
            };
            break;
    }
    var token = localStorage.getItem('access_token');
    var method = 'PUT';

    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        getEarningAccount();
        api.sendEvent({
            name: 'walletReload'
        });
        var tutorial = localStorage.getItem('tutorialMoney');
        if(!tutorial){
            showTutorialMoney();
        }
    });
}
//展示教学内容
function showTutorialMoney() {
    $('.swiper-container.tutorial.money').fadeIn();
    $('.balance-status').addClass('tutorialActive');
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        zoom : true,
        initialSlide :0,
        pagination: {
            el: '.swiper-pagination'
        },on: {
            slideChangeTransitionStart: function(){
                var index = this.activeIndex;
                switch(index){
                    case 0:
                        $('.balance-status').addClass('tutorialActive');
                        $('.transfer-history-box').removeClass('tutorialActive');
                        break;
                    case 1:
                        $('.balance-status,.ui-header-positive').removeClass('tutorialActive');
                        $('.transfer-history-box').addClass('tutorialActive');
                        break;
                    case 2:
                        $('.transfer-history-box').removeClass('tutorialActive');
                        $('.ui-header-positive').addClass('tutorialActive');
                        break;
                }
            }
        }
    });
    function closeTutorial(){
        $('.swiper-container').fadeOut();
        $('.ui-header-positive').removeClass('tutorialActive');
        mySwiper.destroy();
        localStorage.setItem('tutorialMoney','true');
    }
    $('.close-swiper.tutorial,.close-tutorial').on('click',function () {
        closeTutorial();
    });
}


$('.change-account-btn,.close-change-btn').on('click',function () {
    $('.balance-bind-n,.balance-bind-y,.close-change-btn,.change-account-btn').toggleClass('d-none');
});

$('.history-title-btn').on('click',function () {
    if($(this).hasClass('inactive')){
        var id = $(this).attr('id');
        $('.history-title-btn').toggleClass('inactive');
        $(this).closest('.transfer-history-box').toggleClass('transferHistory');
        $('.transfer-list').toggleClass('d-none');
        $('input#currentUl').val(id);
    }
});
//获取完成的订单
function getEarningList(currentpage) {
    loading('show');
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var currentPage = currentpage || 0;
    var paramStr = {"customerid": userId, "currentpage": currentPage, "itemsperpage": 20};
    var url = getBaseUrl() + "api/users/"+ userId +"/earnings";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        loading('hide');
        //console.log(JSON.stringify(data));
        var returnArray = data;
        var len = returnArray.length;
        if(len === 0 && !currentpage){
            $('#transactionsHistory').append('<div style="font-size: 16px; color: #cccccc; text-align: center; margin-top: 40px;">'+ $.i18n.prop("noTransactions") +'</div>');
            return false;
        }
        for (i = 0; i < len; i++) {
            var Id = returnArray[i].OrderId;
            var orderTotal = returnArray[i].OrderTotal;
            // var orderUser= returnArray[i].Customer;
            var profit =  (returnArray[i].EarningAmount).toFixed(2);
            var UTC = returnArray[i].CreatedOnUtc;
            var Date = moment.utc(UTC).local().format('YYYY-MM-DD HH:mm:ss');
            var template ='<li class="ui-row-flex p-3" onclick="OpenOrderDetail(' + Id + ',\'money-card\')">\n' +
                '                <div class="ui-col ui-col-1 x-small text-light-gray text-center">'+ Date +'</div>\n' +
                '                <div class="ui-col ui-col-1 small text-center" >'+ Id +'</div>\n' +
                '                <div class="ui-col ui-col-2 small text-center" >'+ orderTotal +'</div>\n' +
                '                <div class="ui-col ui-col-1 small text-green text-center "><strong>'+ profit  +'</strong></div>\n' +
                '            </li>';
            $('#transactionsHistory').prepend(template);
        }
    });
}

//限制输入最大转出金额
$('#inputTransferAmount').on('input',function () {
    checkInputAmount();
});

function checkInputAmount(submit) {
    var el = $('#inputTransferAmount');
    var val = el.val();
    var max = parseFloat($('#availableMoneyBalance').text());
    if(val <= 0 || val > max){
        el.css('background','#efc7c7');
        if(submit){
            messages($.i18n.prop('transferAmountError'), 'error', 'top', 3);
        }
        return false;
    }else{
        el.css('background','none');
        return true;
    }
}
//提现
function earningTransferBtn() {
    if(checkInputAmount('submit')){
        loading('show');
        var token = localStorage.getItem('access_token');
        var userId = localStorage.getItem('userId');
        var paramStr = {
            "Amount": $('#inputTransferAmount').val(),
            "TweebaaCoins": true,
            "TweebaaCoinsPercentage": $('.slider-input').val(),
            "Cash": true,
            "CashPercentage":100 -  $('.slider-input').val()
        };
        var url = getBaseUrl() + "api/users/"+ userId +"/earning-requests";
        var method = "POST";
        //console.log(JSON.stringify(paramStr));
        $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
            loading('hide');
            console.log(JSON.stringify(data));
            if(data.status === 200){
                hideBounceboxLeft();
                getEarningTransfeHistory();
                $('#transfer').click();
                $('#inputTransferAmount').val('').css('background','none');
            }
        });
    }
}

//获取转出记录
function getEarningTransfeHistory(currentpage) {
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    //var currentPage = currentpage || 0;
    var paramStr = {};
    var url = getBaseUrl() + "api/users/"+ userId +"/earning-requests";
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        if(data.length){
            $('#transferHistory').empty();
            for(i=0; i<data.length; i++){
                var createdOn = moment.utc(data[i].CreatedOn).local().format('YYYY-MM-DD HH:mm:ss');
                var paymentStatus = data[i].PaymentStatus === 'Pending' ? '<span class="small text-yellow"> '+ $.i18n.prop('Pending')+'</span>' : '<span class="small text-green"> '+ $.i18n.prop('Complete')+' </span>';
                var percentage = data[i].CashPercentage +'% : ' + data[i].TweebaaCoinsPercentage +'%';
                var amount = data[i].Amount;
                var template ='<li class="ui-row-flex p-3" onclick="getEarningTransfeHistoryDetail('+ data[i].PaymentRequestId +','+ data[i].PreferredPaymentId +')">\n' +
                    '                <div class="ui-col ui-col-1 x-small text-light-gray text-center">'+ createdOn +'</div>\n' +
                    '                <div class="ui-col ui-col-1 small text-green text-center "><strong>'+ amount.toFixed(2) +'</strong></div>\n' +
                    '                <div class="ui-col ui-col-1 small text-center ">'+ percentage +'</div>\n' +
                    '                <div class="ui-col ui-col-1 small text-center" >'+ paymentStatus +'</div>\n' +
                    '            </li>';
                $('#transferHistory').append(template);
            }
        }
    });
}
