//极速分享
$('#instantShare').on('click', function () {
//生成moment
  loading('show');
  $('.container').addClass('invisible');
  var token = localStorage.getItem('access_token');
  var userId = localStorage.getItem('userId');
  var comment = $('#fullDescription').val();
  var picArray = [];
  $("#uploadImgBox").find('img').each(function () {
    var pic = $(this).attr('src').split(",")[1];
    var picJson = {'PictureBinaryBase64': pic, 'FileName': 'picture.jpg','MediaType': 'Picture'};
    picArray.unshift(picJson);
  });
  var showH = $("#showHomepage").prop('checked');
  var paramStr = {"Content": comment, "Attachments": picArray, "ShowOnHomePage": showH};
  var contentTypeStr = "application/json";
  var url = getBaseUrl() + "api/users/" + userId + "/moments";
  var method = "POST";
  //console.log(JSON.stringify(paramStr));
  if(!$('#getVideo').val() && !comment && !picArray){
    messages($.i18n.prop('contentAllEmpty'), 'error', 'top', 2);
    return false
  }
  $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
    //console.log(JSON.stringify(data));
    var momentId = data.Id;
    if (momentId) {
      if ($('#getVideo').val()) {

        var token = localStorage.getItem('access_token');
        var file = $('#uploadVideoForm')[0];
        var formData = new FormData(file);
        api.setKeepScreenOn({
          keepOn: true
        });

        $.ajax({
          url: getBaseUrl() + "api/users/" + userId + "/moments/" + momentId + '/files?mediaType=video',
          type: 'POST',
          data: formData,
          async: true,
          cache: false,
          contentType: false,
          enctype: 'multipart/form-data',
          processData: false,
          headers: {"Authorization": "Bearer " + token},
          xhr: function(){
            myXhr = $.ajaxSettings.xhr();
            if(myXhr.upload){
              myXhr.upload.addEventListener('progress',function(e) {
                if (e.lengthComputable) {
                  var percent = Math.floor(e.loaded/e.total*100);
                  if(percent <= 100) {
                    // $("#J_progress_bar").progress('set progress', percent);
                    $(".percentage").html(percent+'%');
                  }
                  if(percent >= 100) {
                    $(".percentage").html('');
                  }
                }
              }, false);
            }
            return myXhr;
          },
          success: function (response) {
            fastShare(momentId);
            api.setKeepScreenOn({
              keepOn: false
            });
          },
          error: function (xhr) {
            loading('hide');
            messages('Video upload error!', 'error', 'top', 3);
            $('.container').removeClass('invisible');
          }

        })

      } else {
        $('.container').removeClass('invisible');
        fastShare(momentId);
      }
    } else {
      $('.container').removeClass('invisible');
      loading('hide');
      messages($.i18n.prop('systemError'), 'error', 'top', 3);
    }
  });


});

// 极速分享

function fastShare(momentId) {
    //读取moment
    var token = localStorage.getItem('access_token');
    var userId = localStorage.getItem('userId');
    var paramStr = {"CustomerId": userId};
    var url = getBaseUrl() + "api/users/" + userId + "/moments/" + momentId;
    var method = "GET";
    // console.log(url);
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        console.log(JSON.stringify(data));
        if(data.Pictures !== null && data.Pictures.length !==0){
           var pic =  data.Pictures[0].DownloadUrl;
        }
        var content = data.Content;
        var ShareUrl ='http://'+ data.ShareUrl;
      var n = Date.parse(new Date());
         console.log(pic);
        api.download({
            url: pic,
            savePath: 'fs://tycoon/' + n + '.png', //指定路径和文件名
            report: true,
            cache: true,
            allowResume: true
        }, function (ret, err) {
            if (ret.state === 1) {
                $('#hiddenImg').val(ret.savePath);
            }

        });
      setTimeout(function () {
        shareing(pic,ShareUrl, content);
      },2000)
    });
}

function shareing(pic,ShareUrl, content) {
  var pics = $('#hiddenImg').val();
  $.when(shareWechat(pics, ShareUrl, content)).done(function (data) {
    $.when(shareFacebook(pic, ShareUrl, content)).done(function (data) {
      console.log(data);
      loading('hide');
      if (data === 'success') {
        messages($.i18n.prop('shareSuccessful'), 'success', 'top', 3);

      } else {
        messages($.i18n.prop('shareError'), 'error', 'top', 3);
      }

    });
  });
}