var mPages = 0;
apiready = function () {
    api.addEventListener({
        name: 'scrolltobottom',
        extra: {
            threshold: 0            //设置距离底部多少距离时触发，默认值为0，数字类型
        }
    }, function (ret, err) {
        mPages++;
        following.getAllMoments(mPages,'scroll');
    });

    api.addEventListener({
        name: 'reloadFollowing'
    }, function (ret, err) {
        following.momentsData = [];
        following.getAllMoments(0);
    });

//下拉刷新
    var pullDownRefresh = lan === 'zh' ? '下拉刷新' : 'Pull down to refresh';
    var refreshing = lan === 'zh' ? '刷新中...' : 'Refreshing...';
    var releaseRefresh = lan === 'zh' ? '放开开始刷新' : 'Release refresh';
    api.setRefreshHeaderInfo({
        bgColor: '#fff',
        textColor: '#000',
        textDown: pullDownRefresh,//'Pull down to refresh'
        textLoading: refreshing, //'Refreshing...'
        showTime: false,
        textUp: releaseRefresh //'Release refresh'
    }, function (ret, err) {
        mPages = 0;
        following.momentsData = [];
        following.getAllMoments(0);
    });
    var following = new Vue({
        el: '#following',
        mounted: function () {
            this.getAllMoments(0);
            this.openTips();
        },
        data: {
            momentsData: []
        },
        methods: {
            searchFocus: function () {
                localStorage.setItem('openFrame', 'searchUsers');
                api.openFrame({
                    name: 'searchUsers',
                    url: '../users/search-users.html',
                    bounces: true,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    }
                });
            },
            getAllMoments: function (page,Scroll) {
                var that = this;
                var url = getBaseUrl() + "/api/v2/moments";
                var paramStr = {
                    "currentpage": mPages,
                    "itemsperpage": 24,
                    "viewerId":localStorage.getItem('userId'),
                    "ShowOnHomePage": true,
                    "ShowFollowee": true,
                };
                //console.log(JSON.stringify(paramStr));
                getAxois(url, "POST", paramStr).then(function (response) {
                    api.refreshHeaderLoadDone();
                    $('.no-item').hide();
                    //console.log(JSON.stringify(response));
                    if (response.data.Data.length !== 0) {
                        that.momentsData = that.momentsData.concat(response.data.Data);
                    }else{
                        if(!Scroll) {
                            $('.no-item').show();
                        }
                    }
                    setTimeout(function () {
                        setPost();
                        setCopy();
                    })
                }).catch(function (error) {
                    console.log(JSON.stringify(error) + '#######################');
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            openShareBar:function(content,id){
                var userId = localStorage.getItem('userId');
                var ShareUrl = 'https://tweebaa.cn/html/'+ lan +'/mblog.html?userId='+ userId +'type=blog&&id='+ id+'&server=cn';
                $('#shareURL').attr("data-shareUrl", ShareUrl);
                $("#name").text(content);
                new OpenShareBox();
            },
            showImages:function(event,Index){
                var that = event.target;
                var pic = $(that).closest('.moment-pictures').find('.moment-pic-box');
                pic.each(function () {
                    var imgUrl = $(this).attr('data-img');
                    var img = '<img class="img-fluid" src=' + imgUrl + ' alt="image">';
                    $('#showImages .swiper-wrapper').append('<div class="swiper-slide"><div class="swiper-zoom-container">' + img + '</div></div>');
                });
                $('#showImages').fadeIn();
                var mySwipershowImages = new Swiper('#showImages', {
                    direction: 'horizontal',
                    loop: false,
                    zoom: true,
                    initialSlide: Index,
                    on: {
                        init: function(){
                            $('#downloadImage').val($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                        transitionEnd: function(){
                            $('#downloadImage').val($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                            //console.log($('#showImages').find('.swiper-slide').eq(this.activeIndex).find('img').attr('src'));
                        },
                    },
                });
                addListenerForDownloadImg();
                $('.close-swiper').on('click', function () {
                    $('#showImages').fadeOut();
                    $('#showImages .swiper-wrapper').empty();
                    mySwipershowImages.destroy(false);
                })
            },
            openProdDetail: function (prodId,userInfo) {
                //localStorage.setItem("pageParam",JSON.stringify(userInfo));
                //localStorage.setItem("openFrame",'productDetail');
                // console.log(JSON.stringify(this.userInfo)+'$$$$$$$$$$$$$$$$$$$$$');
                api.openWin({
                    name: 'productDetail',
                    url: 'widget://html/products/product-detail-new.html',
                    bounces: false,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    },
                    pageParam: {
                        prodId: prodId,
                        userInfo:JSON.stringify(userInfo),
                        useServer:userInfo.RegisterCountryCode
                    }
                });
            },
            followedList:function(type){
                api.openWin({
                    name: 'followList',
                    url: 'widget://html/users/follow-list.html',
                    bounces: false,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    },
                    pageParam: {
                        type:type
                    }
                });
            },
            openTycoonPlace: function (user) {
                openUserTycoonPlace(user.Id, user.StoreName, user.AvatarUrl,  'square')
            },
            openTips:function () {
                var tips = localStorage.getItem('followingTips');
                if(!tips){
                    openNewUserTips('followingTips');
                    localStorage.setItem('followingTips','yes');
                }
            }
        }
    });

};
