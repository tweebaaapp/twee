apiready = function () {
    var myPurchases = new Vue({
        el: '#myPurchases',
        mounted: function () {
            this.getPurchases();
        },
        data: {
            purchasesData: []
        },
        methods: {
            getPurchases:function (currentpage) {
                var that = this;
                var customerId = localStorage.getItem('userId');
                var currentPage = currentpage || 0;
                var url = getBaseUrl() + "api/orders?customerId="+customerId+"&currentpage="+currentPage+"&itemsperpage=24";
                loading('show');
                getAxois(url,"GET",{}).then(function (response) {
                    loading('hide');
                    that.purchasesData = response.data;
                    console.log(JSON.stringify(response.data));
                    setTimeout(function () {
                        changePriceType();
                        setPurchases();
                    });
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                    messages($.i18n.prop('systemError'), 'error', 'top', 3);
                });
            },
            openDetail: function (prodId,userInfo) {
                api.openWin({
                    name: 'productDetail',
                    url: 'widget://html/products/product-detail-new.html',
                    bounces: false,
                    rect: {
                        x: 0,
                        y: 0,
                        w: 'auto',
                        h: 'auto'
                    },
                    bgColor: '#fff',
                    animation: {
                        type: "movein",
                        subType: "from_right",
                    },
                    pageParam: {
                        prodId: prodId,
                        userInfo:JSON.stringify(userInfo),
                        countryCode:userInfo.RegisterCountryCode
                    }
                });
            }
        }
    });
};

function setPurchases() {
    $('.purchase-order').each(function () {
        var time = $(this).find('.purchase-time');
        var status = $(this).find('.order-status-text');
        var statusText;
        switch (status.text()) {
            case "Pending":
                statusText = $.i18n.prop('Pending');
                break;
            case "Completed":
                statusText = $.i18n.prop('Completed');
                break;
            case "Canelled":
                statusText = $.i18n.prop('Canelled');
                break;
        }
        //console.log(status.text());
        time.text(moment.utc(time.attr('data-time')).local().format('YYYY-MM-DD HH:mm'));
        status.text(statusText);
    })
}


function back() {
    api.closeWin({
        name: 'purchasesList'
    });
}