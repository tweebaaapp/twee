
//获取用户等级和奖励
function getRewards() {
    var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "api/users/" + userId + "/rewards";
    var paramStr = {};
    var token = localStorage.getItem('access_token');
    var method = 'GET';
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
       console.log(JSON.stringify(data));
        $('#coinsBalance').text(data.Coins);
        //$('.vip-level').text(data.InfluenceLevel);
        localStorage.setItem('rewards',JSON.stringify(data));
    });
}
//get TIV
function getTIV() {
    var userId = localStorage.getItem('userId');
    var url = getBaseUrl() + "api/users/"+ userId +"/tiv";
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    var method = "GET";
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        if(data){
            $('#tiv').text(data.InfluenceValue);
            $('#vipLevel').text(data.InfluenceLevel.Code + '  ' + data.InfluenceLevel.Name);
            loading('hide');
        }
    });
}
function getCheckInStatus() {
    loading('show');
    var date = moment().format('YYYY-MM-DD');
    var userId = localStorage.getItem('userId');
    var method = "GET";
    var url = getBaseUrl() + "api/users/"+ userId +"/rewards/attendance?date=" + date;
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    //console.log(JSON.stringify(url));
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        //console.log(JSON.stringify(data));
        loading('hide');
        var nextTime = moment.utc(data.LastAttendanceTime).add(1,'days').toDate();
        var consecutiveDays = data.ConsecutivePresentDays;
        var todayPresent = data.TodayPresent;
        if(todayPresent){
            $('#checkInButton').addClass('active');
            getNextCheckInTime(nextTime);
        }else{
            $('#checkInButton').removeClass('active');
        }
        $('#continuousCheckIn div:nth-child(-n+'+ consecutiveDays +')').find('span.circle-border').addClass('active');
    });
}

//显示下次签到倒计时
function getNextCheckInTime(nextTime) {
    var now = moment();
    var duration = moment.duration(nextTime  - now, 'ms');
    if(duration.toString().indexOf('-')=== -1){
        setTimeout(function () {
            var hours = duration.get('hours');
            var mins = duration.get('minutes')>9?duration.get('minutes'):'0'+duration.get('minutes');
            var ss = duration.get('seconds')>9?duration.get('seconds'):'0'+duration.get('seconds');
            var next = hours + ':' + mins + ':' + ss;
            $('#nextTime').text(next);
            getNextCheckInTime(nextTime);
        },1000);
    }else{
        $('#checkInButton').removeClass('active');
    }
}

function checkInBtn(This) {
    if($(This).hasClass('active')){
        return false;
    }
    loading('show');
    var userId = localStorage.getItem('userId');
    var method = "POST";
    var url = getBaseUrl() + "api/users/"+ userId +"/rewards/attendance";
    var token = localStorage.getItem('access_token');
    var paramStr = {};
    $.when(postApi(url, method, paramStr, token, null)).done(function (data) {
        console.log(JSON.stringify(data));
        loading('hide');
        getCheckInStatus();
        if(data.status ===200){
            messages($.i18n.prop('checkInSuccessfully'), 'success', 'top', 3);
        }else if(data.status ===400){
            messages($.i18n.prop('checkInError'), 'error', 'top', 3);
        }
    });
}
function profilePage(name) {
    var url ='';
    switch(name){
        case "dashboard":
            url = '../frame/dashboard.html';
            break;
        case "share":
            url = '../access/share-place.html';
            break;
        case "invite":
            url = '../access/invite.html';
            break;
        case "connect":
            url = '../access/bind-setting.html';
            break;
        case "setting":
            url = '../access/setting.html';
            break;
    }
    localStorage.setItem('openFrame', name);
    api.openFrame({
        name: name,
        bounces:false,
        url: url,
        animation:{
            type:"movein",
            subType:"from_right"
        }
    });
}
function myStore() {
    var userName = localStorage.getItem('userName');
    var url = 'www.tweebaa.'+ getApi()+'/tycoonplace/'+ userName ;
    var name = $.i18n.prop('myTycoonPlace');
    openBrowser(url,name);
}
function openWallet() {
    //获取是否已经通过邮箱验证
    //var emailConfirmed =  JSON.parse(localStorage.getItem('EmailConfirmed')); //是否经过邮箱验证
    //if(emailConfirmed){
        localStorage.setItem('openFrame', 'wallet');
        api.openFrame({
            name: 'wallet',
            bounces:false,
            allowEdit:true,
            url: '../users/wallet.html',
            animation:{
                type:"movein",
                subType:"from_right"
            }
        });
    // }else{
    //     var msg = $.i18n.prop('doNotEmailConfirmed');//"User name or password is incorrect";
    //     messages(msg, 'error', 'top', 5);
    // }
}
function logout() {
    confirmBox($.i18n.prop('areYouWantLogout'),$.i18n.prop('cancel'),$.i18n.prop('sure'),'error');
    $('.dialog-box').find('button').on('click',function (e) {
        $('.dialog-box').remove();
        var index = $(this).attr('id');
        if (index === 'btn-2') {
            var loginFrom = localStorage.getItem('loginFrom');
            localStorage.clear();
            localStorage.setItem('tutorialMoney','true');
            if(loginFrom === 'gl' ){
                googleLogout();
            }
            else if(loginFrom === 'fa' ){
                facebookLogout();
            }else{
                api.closeWin({
                    name: 'index'
                });
            }

        } else {
            return false;
        }
    })
}

function facebookLogout() {
    var userLogout = api.require('facebook');
    userLogout.logout();
    api.closeWin({
        name: 'index'
    });
}
function googleLogout() {
    var googelLogout = api.require('google');
    googelLogout.signOut();
    api.closeWin({
        name: 'index'
    });
}
