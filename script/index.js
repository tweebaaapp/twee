
apiready = function(){
    checkPermission();

    //获取当前地点
    $.getJSON("http://ip-api.com/json/?callback=?", function (data) {
        //console.log(data.countryCode);
        localStorage.setItem('countryCode', data.countryCode);
        if (data.country === 'CN') {
            localStorage.setItem('server', 'cn');
        } else {
            localStorage.setItem('server', 'com');
        }
    });

    var main = $api.byId('main');
    var mainPos = $api.offset(main);
    api.openFrameGroup({
        name: 'index',
        rect: {
            x: 0,
            y: 0,
            w: 'auto',
            h: mainPos.h
        },
        scrollEnabled:false,
        index:0,
        preload:3,
        frames: [{
            name: 'square',
            url: 'square.html',
            allowEdit:true,
            bgColor: '#fff'
        }, {
            name: 'following',
            url: 'following.html',
            allowEdit:true,
            bgColor: '#fff'
        },{
            name: 'messages',
            url: 'conversation.html',
            bgColor: '#fff'
        },
            {
                name: 'mine',
                url: 'work-place.html',
                bgColor: '#fff'
            }]
    }, function(ret, err) {
        var index = ret.index;
        var id;
        switch (index) {
            case 0:
                id = "wall";
                break;
            case 1:
                id = "follow";
                break;
            case 2:
                id = "messages";
                break;
            case 3:
                id = "mine";
                break;

        }
        $('#footer').find('li').removeClass('active');

        $('#'+id).addClass('active');
    });
    api.setStatusBarStyle({
        style: 'dark',
        color:'#fff'
    });
    isOnLineStatus();
    // //载入聊天
    chatInit();
    //返回前台后重新连接
    api.addEventListener({
        name:'resume'
    }, function(ret, err){
        initRongAndListener();
    });
    //获取在web打开的参数
    api.addEventListener({
        name:'appintent'
    },function(ret,err){
        var appParam = ret.appParam;
        var url,name;
        //alert(JSON.stringify(ret));
        if(appParam.type==='product'){
            url = 'widget://html/share-back/share-product-back.html';
            name  = 'shareProduct';
        }else if(appParam.type==='blog'){
            url = 'widget://html/share-back/share-moment-back.html';
            name  = 'shareMoment';
        }
        api.openWin({
            name: name,
            url: url,
            bounces: false,
            allowEdit:true,
            rect: {
                x: 0,
                y: 0,
                w: 'auto',
                h: 'auto'
            },
            slidBackType:false,
            bgColor: '#fff',
            animation:{
                type:"movein",
                subType:"from_right",
            },
            pageParam: {
                prodId:appParam.id,
                userId:appParam.userId,
                useServer:appParam.server,
            }
        });
    });
    //监听登录
    api.addEventListener({
        name:'loginInit'
    }, function(ret, err){
        chatInit();
    });

    //监听好友请求
    // api.addEventListener({
    //     name: 'showFriendRequestTips'
    // }, function(ret, err) {
    //     if(ret.value.active === 'hide'){
    //         $('#contactBtn').removeClass('unread');
    //     }else {
    //         $('#contactBtn').addClass('unread');
    //     }
    // });
    //console.log(localStorage.getItem('chatID'));
    //监听未读消息数量
    api.addEventListener({
        name: 'showUnreadTips'
    }, function(ret, err) {
        if(ret.value.count !== 0){
            //console.log(JSON.stringify(ret.value.count));
            $('.new-info').show();
        }else {
            $('.new-info').hide();
        }
    });
    // 监听返回键
    var pressKey = 0;
    api.addEventListener({
        name: 'keyback'
    }, function (ret, err) {
        var open = localStorage.getItem('openFrame');
        if(open ==='' || !open){
            if(pressKey < 1){
                api.toast({
                    msg : $.i18n.prop('pressAgainCloseApp'), //'Press again to close tweebaa App'
                    duration : 2000,
                    location : 'bottom'
                });
                pressKey++;
            }else{
                api.closeWidget({
                    silent:true
                });
            }
            setTimeout(function () {
                pressKey = 0;
            },3000)
        }else{
            var jsfun = 'back();';
            api.execScript({
                frameName: open,
                script: jsfun
            });
        }
    });


};

function chatInit(){
    if(localStorage.getItem('access_token')){
        if (localStorage.getItem('chatToken')) {
            initRongAndListener('index');
        } else {
            getToken();
        }
    }
}
//footer 菜单
var clicked = 1;
$('#footer').find('li').each(function (index) {
    var id = $(this).attr('id');
        $(this).on('touchstart',function () {
        var token = localStorage.getItem('access_token');
        if(token || id ==='square'){
            api.setFrameGroupIndex({
                name: 'index',
                index: index,
            });
            $('#footer').find('li').removeClass('active');
            $('#'+id).addClass('active');
            if(clicked > 0 && id ==='wall'){
                api.sendEvent({
                    name: 'reloadSquare'
                });
            }
            // if(id ==='mine'){
            //     api.sendEvent({
            //         name: 'workplaceReload'
            //     });
            // }
        }else{
            openLogin();
        }
        //计算重复点击Square
            refreshSquare(id)
    });
});

function refreshSquare(id) {
    if(id==='wall'){
        clicked++
    }else{
        clicked = 0;
    }

}