
var path  = '../../res/i18n/';
var navLanguage = navigator.language;
var lan;
if(localStorage.getItem('languageSet')){
    lan= localStorage.getItem('languageSet');
}else{
    if(navLanguage==='zh' || navLanguage ==='zh-CN' || navLanguage ==='zh_CN' || navLanguage ==='zh-cn' || navLanguage ==='CN' || navLanguage ==='cn' ){
        lan= 'zh';

    }else{
        lan= 'en';
    }
}
jQuery.i18n.properties({
    name: 'index',
    path: path,
    mode: 'map',
    //async: true,
    language: lan,
    callback: function () {
        //console.log("i18n赋值中...");
        //初始化页面元素
        $('[data-i18n-placeholder]').each(function () {
            $(this).attr('placeholder', $.i18n.prop($(this).data('i18n-placeholder')));
        });
        $('[data-i18n-text]').each(function () {
            $(this).text($.i18n.prop($(this).data('i18n-text')));
        });
        // console.log("i18n写入完毕");
    }
});
