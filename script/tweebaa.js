/*
 * Tweebaa JavaScript Library
 * Copyright (c) 2018 tweebaa.com
 */

//Base 64 encode function:
var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }
};

function postApi(url, method, paramStr, token, contentTypeStr,node) {
    var defer = $.Deferred();
    var dataParam;
    var param = JSON.stringify(paramStr);
    var head;
    if (token === null) {
        head = {"Authorization": "Bearer "};
    } else if (token === 'IM') {
        head = {"Authorization": "Basic dGViYXdleTY5YmM6YnV0eW5vb2M5ODd2MG9jY2Q="};
    } else if (node === 'node') {
        head = {"apikey": token};
    } else {
        head = {"Authorization": "Bearer " + token};
    }
    if (contentTypeStr === null) {
        contentTypeStr = "application/x-www-form-urlencoded; charset=UTF-8";
        dataParam = paramStr;
    } else {
        contentTypeStr = "application/json; charset=utf-8";
        dataParam = param;
    }
    $.ajax({
        contentType: contentTypeStr,
        method: method,
        url: url,
        async: true,
        data: dataParam,
        dataType: 'json',
        headers: head
    }).done(function (data) {
        defer.resolve(data);
    }).fail(function (err) {
        defer.resolve(err);
    });
    return defer;
}

//axois 请求
function getAxois(url,method,perms) {
    var token = localStorage.getItem('access_token') || '';
    return  axios({
        method:method,
        url:url,
        data:perms,
        headers:{
            "Authorization":"Bearer " + token
        }
    })
}

function checkTokenValid(token) {
    var hasToken = false;
    if (token) {
        hasToken = true;
    }
    return hasToken;
}
//获取当前用户使用API
function getBaseUrl(countryCode) {
    // var token = localStorage.getItem('access_token');
    // var server;
    // if(token){
    //     server = countryCode || localStorage.getItem('registerCountryCode');
    // }else{
    //     server = localStorage.getItem('server') ==='cn' ? 'CN': 'COM';
    // }
    return "https://api.tweebaa.cn/";
    // if (server === 'CN') {
    //     return "https://api.tweebaa.cn/"
    // } else  {
    //     return "http://api.tycoonplace.com/"    //https://api.tycoonplace.com/ 生产环境    http://api-dev.tycoonplace.com/ 开发环境
    // }
}

//获取当前用空间地址
function getTycoonUrl() {
    var server = localStorage.getItem('server');
    // console.log(server);
    if (server === 'com' || !server) {
        return ".tycoonplace.com/"
    } else if (server === 'cn') {
        return ".leivaireacademy.com/"
    }
}
//获取当前用户使用API Sever 标识
function getApi() {
    return 'cn';
    // var api = getBaseUrl();
    // if (api === 'https://api.tweebaa.cn/') {
    //     return 'cn';
    // } else {
    //     return 'com';
    // }
}
// //获取nodeJs API
// function getNodeApiUrl() {
//     return "https://tweebaa.cn/api/v1/";
//     // var server = localStorage.getItem('server');
//     // if (server === 'com' || !server) {
//     //     return "https://tweebaa.com/api/v1/"
//     // } else if (server === 'cn') {
//     //     return "https://tweebaa.cn/api/v1/"
//     // }
// }
//获取NodeJs uas Api
function getUASApi(CountryCode) {
    return 'https://tweebaa.cn/api/v1/uas/';
    // if(CountryCode == 'CN'){
    //     return 'https://tweebaa.cn/api/v1/uas/'
    // }else{
    //     return 'https://tweebaa.com/api/v1/uas/'
    // }
}
function success(meg) {
    //弹出成功提示
    $('.success').text(meg).addClass('show-down');
    setTimeout(function () {
        $('.success').text('').removeClass('show-down');
    }, 2000);
}

//过滤字符串
function filterStr(str) {
    var pattern = new RegExp("[\u4E00-\u9FA5\uF900-\uFA2D`~!@#$^&*()=|{}':;', \\[\\].<>/?~！@#￥……&*（）——|{}【】‘；： ”“'。，、？%+]");
    var specialStr = "";
    for (var i = 0; i < str.length; i++) {
        specialStr += str.substr(i, 1).replace(pattern, '');
    }
    var replaceStr = specialStr.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, "");
    //过滤表情
    return replaceStr;
}

//    检查用户名
function checkUsername(userName) {
    if (!userName) {
        return false;
    }
    var nameUrlStr = getBaseUrl() + "api/Account/CheckUsernameAvailability";
    var nameurl = encodeURI(nameUrlStr);
    var method = "GET";
    var paramStr = {
        "username": userName
    };
    var status = $.Deferred();
    $.when(postApi(nameurl, method, paramStr, null, null)).done(function (data) {
        // console.log(JSON.stringify(data));
        if (data.Available === false) {
            messages($.i18n.prop('usernameUsed'), 'error', 'bottom', 2); //'Username has been used'
            status.resolve(false);
        } else if (data.Available === true) {
            status.resolve(true);
        } else {
            messages($.i18n.prop('systemError'), 'error', 'bottom', 2);
        }
    });
    return status;
}

//检查商店名
function checkStorename(storeName) {
    if (!storeName) {
        return false;
    }
    var storenameUrlStr = getBaseUrl() + "api/Account/CheckStorenameAvailability";
    var storenameurl = encodeURI(storenameUrlStr);
    var method = "GET";
    var paramStr = {
        "storename": storeName
    };
    var status = $.Deferred();
    $.when(postApi(storenameurl, method, paramStr, null, null)).done(function (data) {
        // console.log(JSON.stringify(data));
        if (data.Available === false) {
            messages($.i18n.prop('tycoonnameUsed'), 'error', 'bottom', 2); //'tycoon place name has been used'
            status.resolve(false);
        } else if (data.Available === true) {
            status.resolve(true);
        } else {
            messages($.i18n.prop('systemError'), 'error', 'bottom', 2);
        }
    });
    return status;
}


function messages(msg, type, location, time, square, close) {
    $('.msg').remove();
    var t = time * 1000 || 2000;
    var s = square || '';
    var c = close || '';
    var template = '<div class="msg ' + location + ' ' + type + ' ' + s + ' ' + c + '">' + msg + '</div>';
    $('body').append(template);
    setTimeout(function () {
        $('.msg').remove();
    }, t);
}

$('body').on('click', '.msg.close', function () {
    $(this).remove();
});

//loading
function loading(visible) {
    $('.loading-box').remove();
    if (visible === 'show') {
        var loading = '<div class="ui-loading-block  loading-box show" style="z-index: 9999999999999;">\n' +
            '                <div class="ui-loading-cnt">\n' +
            '                    <i class="ui-loading-bright"></i>\n' +
            '                    <p>Loading...</p>\n' +
            '                </div>\n' +
            '            </div>';
        $('body').append(loading);
        setTimeout(function () {
            $('.loading-box').remove();
        }, 530000)
    } else if (visible === 'hide') {
        $('.loading-box').remove();
    }

}

//弹窗
function popupWin(id) {
    var win = $('.popup-win' + '#' + id);
    win.css('display', 'flex');
    win.on("click", function (e) {
        $('.popup-win').fadeOut();
    });
}


//input 提示 及光标位置
$('.form-ctrl,input').on('focus', function () {
    $(this).attr('placeholder', '').prev('label').removeClass('invisible');
    $(this).selectionEnd = $(this).val().length;
});

//对话框
function confirmBox(title, btn1, btn2, type) {
    // var template = '<div class="dialog-box text-center ui-row ' + type + '" >' +
    //     '<div class="ui-col ui-col-100 small mb-4"><p>' + title + '</p></div>' +
    //     '<div class="ui-col ui-col-50 p-3"><button class="ui-btn btn-block ui-btn-primary" id="btn-1">' + btn1 + '</button></div>' +
    //     '<div class="ui-col ui-col-50 p-3"><button class="ui-btn btn-block ui-btn-primary" id="btn-2">' + btn2 + '</button></div>' +
    //     '</div>';
    var template = '<div class="ui-dialog dialog-box show ">\n' +
        '    <div class="ui-dialog-cnt">\n' +
        '        <div class="ui-dialog-bd">\n' +
        '            <p>'+ title +'</p>\n' +
        '        </div>\n' +
        '        <div class="ui-dialog-ft">\n' +
        '            <button type="button small" data-role="button" id="btn-1">'+ btn1 +'</button>\n' +
        '            <button type="button small bk-light-blue" data-role="button" id="btn-2">'+ btn2 +'</button>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    $('body').append(template);
}

//上弹框
function preventDefaultHandler(e) {
    e.preventDefault();
}
function showBounceboxBottom(type) {
    var box = !type?$('.bounce-box-bottom:not(.shopping-cart)'):$('.bounce-box-bottom.'+ type);
    var h = box.height();
    if (box.is(':hidden')) {
        box.css('bottom', -h);
        box.show();
        box.animate({bottom: "0"});
        if(!type){
            $('ul.moment-ul,ul.button-list,.container').addClass('no-touch');
            document.addEventListener('touchmove', preventDefaultHandler, false);
        }

    }
}

function hideBounceboxBottom(type) {
    var box = !type?$('.bounce-box-bottom:not(.shopping-cart)'):$('.bounce-box-bottom.'+ type);
    var h = box.height();
    if (box.is(':visible')) {
        box.animate({bottom: -h});
        setTimeout(function () {
            box.hide();
        }, 300);
        if(!type){
            $('ul.moment-ul,ul.button-list,.container').removeClass('no-touch');
            document.removeEventListener('touchmove', preventDefaultHandler, false);
        }

    }
}

//左弹框
function showBounceboxLeft(Id, momnets) {
    var box = $('.bounce-box-left');
    box.attr('id', Id);
    var h = box.height();
    var w = box.width();
    box.css('margin-top', -(h / 2));
    if (box.is(':hidden')) {
        box.css('left', -w).show().animate({left: "0"});
    }
    if (momnets) {
        var dis = $(momnets).closest('.moment-item').find('.moment-content p').text();
        $('#convertInfo').attr('data-momentId', Id).find('textarea#fullDescription').val(dis);
    }
    $('ul.moment-ul,ul.button-list,.container').addClass('blur no-touch');
    document.addEventListener('touchmove', preventDefaultHandler, false);
}

function hideBounceboxLeft() {
    var box = $('.bounce-box-left');
    var w = box.width();
    if (box.is(':visible')) {
        box.animate({left: -w});
        setTimeout(function () {
            box.hide();
        }, 300);
    }
    $('ul.moment-ul,ul.button-list,.container').removeClass('blur no-touch');
    document.removeEventListener('touchmove', preventDefaultHandler, false);
}
//弹出分享菜单
function OpenShareBox(){
    var box = '<div class="ui-actionsheet">\n' +
        '    <div class="ui-actionsheet-cnt am-actionsheet-down bk-white">\n' +
        '        <h4 class="text-center">'+ $.i18n.prop('shareto')+'</h4>\n' +
        '        <div class="ui-row text-center mt-5 share-box">\n' +
        '        <div class="ui-col ui-col-25 mb-3" onclick="shareToWeChat(\'session\')">\n' +
        '            <i class="icon iconfont icon-wechat "></i>\n' +
        '            <p data-i18n-text="wechat">'+ $.i18n.prop('wechat')+'</p>\n' +
        '        </div>\n' +
        '        <div class="ui-col ui-col-25 mb-3" onclick="shareToWeChat(\'timeline\')">\n' +
        '            <i class="icon iconfont icon-dpstipiconfriends "></i>\n' +
        '            <p data-i18n-text="wechatMoment">'+ $.i18n.prop('wechatMoment')+'</p>\n' +
        '        </div>\n' +
        '        <div class="ui-col ui-col-25 mb-3" onclick="shareToFacebook()">\n' +
        '            <i class="icon iconfont icon-facebook "></i>\n' +
        '            <p data-i18n-text="facebook">'+ $.i18n.prop('facebook')+'</p>\n' +
        '        </div>\n' +
        '        <div class="ui-col ui-col-25 mb-3" onclick="shareToGoogle()">\n' +
        '            <i class="icon iconfont icon-google "></i>\n' +
        '            <p data-i18n-text="google">'+ $.i18n.prop('google')+'</p>\n' +
        '        </div>\n' +
        '        <div class="ui-col ui-col-25 mb-3" onclick="shareToTwitter()">\n' +
        '            <i class="icon iconfont icon-twitter "></i>\n' +
        '            <p data-i18n-text="twitter">'+ $.i18n.prop('twitter')+'</p>\n' +
        '        </div>\n' +
        '        <div class="ui-col ui-col-25 mb-3" onclick="shareToWeibo()">\n' +
        '            <i class="icon iconfont icon-weibo "></i>\n' +
        '            <p data-i18n-text="weibo">'+ $.i18n.prop('weibo')+'</p>\n' +
        '        </div>\n' +
        '        <div class="ui-col ui-col-25 mb-3" onclick="shareToclipboard()">\n' +
        '            <i class="icon iconfont icon-order "></i>\n' +
        '            <p data-i18n-text="clipboard">'+ $.i18n.prop('clipboard')+'</p>\n' +
        '        </div>' +
        '        <div class="ui-actionsheet-split-line"></div>\n' +
        '        <button id="cancel"  onclick="CloseShareBox()">'+ $.i18n.prop('cancel')+'</button>\n' +
        '    </div>\n' +
        '</div>';
    $('body').append(box);
    setTimeout(function () {
        $('.ui-actionsheet').addClass('show');
    });
}
function CloseShareBox() {
    $('.ui-actionsheet').remove();
}
//更改价格标签
function changePriceType(countryCode) {
    // var server = countryCode || localStorage.getItem('registerCountryCode');
    // if (server !== 'CN') {
    //     $('.price-TAGs').text('US$ ');
    //     $('.price-type').attr('placeholder', 'US$');
    // } else  {
        $('.price-TAGs').text('￥');
        $('.price-type').attr('placeholder', '￥RMB');
    //}
}

//检测是否联网
function isOnLineStatus() {
    var s = api.connectionType;
    s = s.toLowerCase();
    if ((s.indexOf('wifi') != -1) || (s.indexOf('3g') != -1) || (s.indexOf('4g') != -1) || (s.indexOf('2g') != -1)) {
    } else {
        var url;
        if ($('#path').val()) {
            url = 'no-internet.html';
        } else {
            url = 'widget://html/access/no-internet.html'
        }
        api.openFrame({
            name: 'nointernet',
            url: url,
            bounces: false,
            animation: {
                type: "none"
            }
        });
    }
}

function insertPlaceHoder(page) {
    var template;
    if (page === 'tweebaaProduct') {
        template = '<li class="media ui-col ui-col-50">\n' +
            '      <div class="media-box place-holder-load">\n' +
            '        <img class="img-fluid" src="../../image/no-image.png" alt="image">\n' +
            '        <div class="media-body text-center">\n' +
            '          <p class="ui-nowrap place-holder-text-1"></p>\n' +
            '          <h5 class="text-left small text-red mt-1 place-holder-text-2"></h5>\n' +
            '        </div>\n' +
            '      </div>\n' +
            '    </li>';
        for (i = 0; i < 4; i++) {
            $('#tweebaaProductList').append(template);
        }
    }
    else if (page === 'myProduct') {
        template = '<li class="media ui-col ui-col-50">\n' +
            '      <div class="media-box place-holder-load">\n' +
            '        <img class="img-fluid" src="../../image/no-image.png" alt="image">\n' +
            '        <div class="media-body text-center">\n' +
            '          <p class="ui-nowrap place-holder-text-1"></p>\n' +
            '          <h5 class="text-left x-small text-blue mt-3 place-holder-text-2"></h5>\n' +
            '          <h5 class="text-left small text-red mt-1 place-holder-text-2"></h5>\n' +
            '        </div><br>\n' +
            '      </div>\n' +
            '    </li>';
        for (i = 0; i < 4; i++) {
            $('#productsList').append(template);
        }
    }
}

//检测权限
function checkPermission() {
    var systemType = api.systemType;
    var permissionList = [];
    if (systemType === 'android') {
        //检测用户权限
        var resultList = api.hasPermission({
            list: ['storage', 'camera', 'microphone', 'photos']
        });
        //alert(JSON.stringify(resultList));
        for (i = 0; i < resultList.length; i++) {
            if (resultList[i].granted === false) {
                permissionList.push(resultList[i].name);
                //alert(JSON.stringify(permissionList));
            }
        }
        setTimeout(function () {
            //alert(JSON.stringify(permissionList));
            api.requestPermission({
                list: permissionList,
                code: 1
            }, function (ret, err) {
                for (i = 0; i < ret.list.length; i++) {
                    if (ret.list[i].granted === false) {
                        api.closeWidget({
                            silent: true
                        });
                    }
                }
            });
        }, 500)
    }
}

//隐藏没有使用的右侧按钮
if ($('header li:last-child').children().length === 0) {
    $('header li:last-child').css('opacity', 0);
}

//下载二维码
function downloadQRCode () {
    loading('show');
    var img = $('#qrcode img')[0];
    var canvas = document.createElement('canvas');
    canvas.width = 256;
    canvas.height = 256;
    canvas.getContext('2d').drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL('image/png');
    var sendUrl = dataURL.split(",")[1];
    var username = localStorage.getItem('userName');
    var paramStr = {
        "name": username + '_' + getApi() + '_qrCode.jpg',
        "baseString": sendUrl
    };
    var url = "https://api.tweebucks.com/tweebaa/uploadHead";
    var token = "IM";
    var method = "POST";
    var contentTypeStr = "application/json";
    $.when(postApi(url, method, paramStr, token, contentTypeStr)).done(function (data) {
        api.download({
            url: data.fileDownloadUri,
            report: true,
            cache: true,
            allowResume: true
        }, function (ret, err) {
            if (ret.state === 1) {
                var Url= ret.savePath;
                loading('hide');
                api.saveMediaToAlbum({
                    path: Url
                }, function(ret, err) {
                    if (ret && ret.status) {
                        api.toast({
                            msg: $.i18n.prop('QRcodeDownload'),
                            duration: 2000,
                            location: 'bottom'
                        });
                    }
                });

            }
        });
    });
}

//打开浏览器
function openBrowser(url,name) {
    localStorage.setItem('openFrame', 'browser');
    api.openFrame({
        name: 'browser',
        url: '../users/browser.html',
        pageParam: {
            url: url,
            name:name
        },
        showProgress:true,
        progress:{
            type:"page",
            title:"default",
            text:"default",
            color:"#73b3f8"
        }
    });
}
//打开购物车
function openShoppingCart(page) {
    //localStorage.setItem('openFrame', 'shoppingCart');
    api.openWin({
        name: 'shoppingCart',
        url: '../frame/shopping.html',
        pageParam: {
            fromPage: page
        }
    });
}
//打开扫码器扫码
function openScaner() {
    var module = api.require('easyQRCodeScan');
    var resultCallback = function(ret, err) {
        var msg = ret.msg;
        api.startPlay({
            path: 'widget://image/beep.mp3'
        });
        if(msg.indexOf("http://")!==-1 || msg.indexOf("https://")!==-1){
            openBrowser(msg,'');
        }else if(msg.indexOf("@")!==-1){
            searchFriend(msg);
            $('.choose-add-friend-method,.show-my-name').hide();
        }
    };

    module.qrScan(resultCallback);
}

//打开登录窗
function openLogin() {
    api.openWin({
        name: 'login',
        url: 'widget://html/access/login-new.html',
        bounces: true,
        allowEdit: true,
        rect: {
            x: 0,
            y: 0,
            w: 'auto',
            h: 'auto'
        },
        bgColor: '#fff',
        animation:{
            type:"movein",
            subType:"from_right",
        }
    });
}

//打开用户空间
function openUserTycoonPlace(Id,StoreName,AvatarUrl,FromPage) {
    api.openWin({
        name: 'tycoonplace',
        url: 'widget://html/users/tycoonplace.html',
        bounces: false,
        rect: {
            x: 0,
            y: 0,
            w: 'auto',
            h: 'auto'
        },
        slidBackType:false,
        bgColor: '#fff',
        animation:{
            type:"movein",
            subType:"from_right",
        },
        pageParam: {
            userId:Id,
            storeName:StoreName,
            AvatarUrl:AvatarUrl,
            FromPage:FromPage
        }
    });
}

//打开新win窗口
function openWindows(name,url,param) {
    api.openWin({
        name: name,
        url: url,
        bounces: false,
        rect: {
            x: 0,
            y: 0,
            w: 'auto',
            h: 'auto'
        },
        slidBackEnabled:false,
        allowEdit:true,
        bgColor: '#fff',
        animation:{
            type:"movein",
            subType:"from_right",
        },
        pageParam: param
    });
}

//打开新手提示
function openNewUserTips(content) {
    var template = '<div class="ui-dialog ui-dialog-operate  show" id="NewUserTips">\n' +
        '    <div class="ui-dialog-cnt">\n' +
        '        <div class="ui-dialog-hd pt-4">\n' +
        '           <div class="ui-img">\n' +
        '               <span style="background:url(../../image/redpangzi/1.png) center center no-repeat;background-size: contain"></span>\n' +
        '           </div>\n' +
        '        </div>\n' +
        '        <div class="ui-dialog-bd">\n' +
        '            <pre class="p-5 small text-dark-gray">'+ $.i18n.prop(content) +'</pre>\n' +
        '        </div>\n' +
        '        <i class="ui-dialog-close" data-role="button" onclick="closeNewUserTips()"></i>\n' +
        '    </div>\n' +
        '</div>';
    $('body').append(template);
}
function closeNewUserTips() {
    $('#NewUserTips').remove();
}

//post 相关
//修改图片大小并设置时间格式
function setPost() {
    $('.moment-ul').find('li').each(function () {
        var pic = $(this).find('.moment-pic-box');
        if (pic) {
            var len = pic.length;
            var w;
            switch (len) {
                case 1:
                    w = 40;
                    break;
                case 2:
                    w = 40;
                    break;
                default:
                    w = 33.3;
                    break;
            }

            pic.each(function () {
                $(this).css('width', (w + '%'));
                $(this).height($(this).width());
            });
        }
        var time = $(this).find('.post-time');
        time.text(moment.utc(time.attr('data-time')).local().format('YYYY-MM-DD HH:mm'));
        var extendBtn = $(this).find('.moment-content-extend');
        var testContent = $(this).find('.moment-content');
        var contentHeight = testContent.height();
        //console.log(contentHeight);
        if (contentHeight > 65) {
            testContent.addClass('overflow');
            extendBtn.removeClass('d-none');
        }
    });
}

function showExtend(that) {
    $(that).toggleClass('extend');
    if ($(that).hasClass("extend")) {
        $(that).html('<span class="text-blue  p-2">' + $.i18n.prop('collapse') + '</span>').prev().removeClass('overflow');
    } else {
        $(that).html('<span class="text-blue  p-2">' + $.i18n.prop('fullText') + '</span>').prev().addClass('overflow');
    }

}
//复制post文字
function setCopy() {
    $('.moment-ul').on('touchstart', '.moment-content', function (event) {
        var timeout = undefined;
        var that = $(this);
        var $this = this;
        that.addClass('opacity-5');
        timeout = setTimeout(fn, 800);
        function fn() {
            var text = that.text();
            var clipBoard = api.require('clipBoard');
            clipBoard.set({
                value: text
            }, function (ret, err) {
                if (ret) {
                    messages($.i18n.prop('copied'), 'success', 'top', 2);
                }
            });
        }
        $this.addEventListener('touchmove', function (event) {
            clearTimeout(timeout);
            that.removeClass('opacity-5');
        }, false);
        $this.addEventListener('touchend', function (event) {
            clearTimeout(timeout);
            that.removeClass('opacity-5');
        }, false);
    });
}
//打开图片是监听长按来下载图片
function addListenerForDownloadImg() {
    api.addEventListener({
        name:'longpress'
    }, function(ret, err){
        api.actionSheet({
            cancelTitle: $.i18n.prop('cancel'),
            buttons: [$.i18n.prop('saveImage')]
        }, function(ret, err) {
            var index = ret.buttonIndex;
            if(index === 1){
                //alert($('#downloadImage').val());
                loading('show');
                api.saveMediaToAlbum({
                    path: $('#downloadImage').val(),
                    groupName:'Tweebaa'
                }, function(ret, err) {
                    if (ret && ret.status) {
                        loading('hide');
                        api.toast({
                            msg: $.i18n.prop('imgSaved'),
                            duration: 2000,
                            location: 'bottom'
                        });
                    }
                });

            }else{
                return false;
            }
        });

    });
}